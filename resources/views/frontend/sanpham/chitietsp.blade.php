@extends('frontend.layout.master')

@section('title',$sanpham->Pro_Name)
@section('keywords',$sanpham->TuKhoaSeo)
@section('description',$sanpham->MoTaSeo)
@section('url',url('/san-pham/'.$sanpham->Slug))
@section('titleseo','Nhà thuốc chính hãng - '.$sanpham->TieuDeSeo)
@section('type','product')
@section('descriptionseo',$sanpham->MoTaSeo)
@section('image')
@section('plugins-css')
    <script src='{{ asset('frontend/js/lastviewproduct.js') }}' type='text/javascript'></script>
    <script src='{{ asset('frontend/js/jquery.elevatezoom.min.js') }}' type='text/javascript'></script>
@endsection

@section('content')
    <section id="breadcrumb-wrapper">
        <div class="breadcrumb-overlay"></div>
        <div class="breadcrumb-content">
            <div class="wrapper">
                <div class="inner text-center">
                    <div class="breadcrumb-big">
                        <h2>
                            {!! $sanpham->Pro_Name !!}
                        </h2>
                    </div>
                    <div class="breadcrumb-small">
                        <a href="{{ url('/') }}" title="Quay trở về trang chủ">Trang chủ</a>
                        {{-- <span aria-hidden="true">/</span>
                        <a href="{{ url('tin-tuc.html') }}" title="">Tin tức</a> --}}
                        <span aria-hidden="true">/</span>
                        <span>{!! $sanpham->Pro_Name !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            <!-- HRV LAST VIEW PRODUCT -->
            {{-- <script>
                (function saveAlgorithm() {
                    lastViewProducts.add('bo-2-hop-tinh-chat-lam-dep-collagen-adiva-28-lo-x-30ml');
                }());
            </script> --}}
            <!-- END: HRV LAST VIEW PRODUCT -->
            <section id="product-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        <div>
                            
                            <div class="grid product-single">
                                <div class="grid__item large--five-twelfths medium--one-whole small--one-whole">
                                    <div class="product-single__photos" >
                                        <div id="ProductPhoto" class="owl-carousel owl-enable" data-slide="1" data-pagination="false" data-nav="true">
                                            {{-- <div class="item" >
                                                <a>
                                                <img 
                                                    class="lazyload" src="{{ asset('img/lazyload.jpg') }}" data-src="https://product.hstatic.net/1000282280/product/suplo-013a-01_master.jpg" 
                                                    alt="" />
                                                </a>
                                            </div>
                                            <div class="item" >
                                                <a>
                                                <img 
                                                    class="lazyload" src="{{ asset('img/lazyload.jpg') }}" data-src="https://product.hstatic.net/1000282280/product/suplo-013a-02_master.jpg" 
                                                    alt="" />
                                                </a>
                                            </div> --}}
                                            @if ($sanpham->Image)
                                          
                                            <div class="item" >
                                                <a>
                                                <img 
                                                    class="lazyload" src="{{ asset('img/lazyload.jpg') }}" data-src="{{ asset('uploads/sanpham/'.$sanpham->Image) }}" 
                                                    alt="{!! $sanpham->Pro_Name !!}" />
                                                </a>
                                            </div>
                                            
                                            @endif
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="grid__item large--seven-twelfths medium--one-whole small--one-whole">
                                    <div class="product-content">
                                        <div class="pro-content-head clearfix">
                                            <h1 itemprop="name">{!! $sanpham->Pro_Name !!}</h1>
                                            @if(!empty($sanpham['ThuongHieu']))
                                            <div class="pro-brand">
                                                <span class="title">Thương hiệu:</span> <a href="javascript:;">{{ $sanpham->ThuongHieu }}</a>
                                            </div>
                                            <span>|</span>
                                            @endif
                                            <div class="pro-sku ProductSku">
                                                <span class="title">Mã SP:</span> <span class="sku-number">{{ $sanpham->Pro_id }}</span>
                                            </div><br>
                                            @if(!empty($sanpham['ThanhPhan']))
                                            <div class="pro-brand">
                                                <span class="title">Thành phần:</span> <a href="javascript:;">{{ $sanpham->ThanhPhan }}</a>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="pro-price clearfix">
                                            <span class="current-price ProductPrice">{!! number_format($sanpham->Gia,0,',','.') !!}₫/{{ $sanpham->DonVi }}</span>
                                        </div>
                                        
                                        <div class="pro-short-desc">
                                            {!! strip_tags(str_limit($sanpham->MoTa,200)) !!}
                                        </div>
                                        <form action="{{ url('them-gio-hang/'.$sanpham->Pro_id) }}" method="get" enctype="multipart/form-data" id="AddToCartForm" class="form-vertical">
                                            <div class="product-variants-wrapper">
                                                {{-- <select name="id" id="productSelect" class="product-single__variants hide">
                                                    <option  selected="selected"  data-sku="SUPLO-013A" value="1028117897">Đức - 990,000 VND</option>
                                                </select>
                                                <div id="product-select-watch" class="select-swatch">
                                                    <div id="variant-swatch-0" class="swatch swatch-product-single clearfix" data-option="option1" data-option-index="0">
                                                        <div class="header">Xuất xứ</div>
                                                        <div class="select-swap">
                                                            <div data-value="Đức" class="n-sd swatch-element duc " >
                                                                <input class="variant-0 input-product" id="swatch-0-duc" type="radio" name="option1" value="Đức" checked  />
                                                                <label for="swatch-0-duc" >
                                                                Đức
                                                                <img class="crossed-out" src="https://theme.hstatic.net/1000282280/1000362500/14/soldout.png?v=1486" />
                                                                <img class="img-check" src="https://theme.hstatic.net/1000282280/1000362500/14/select-pro.png?v=1486" />
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                                <div class="product-size-hotline">
                                                    <div class="product-hotline">
                                                        Hotline hỗ trợ bán hàng 24/7: <a href="tel:{!! $lienhe->Sdt !!}">{!! $lienhe->Sdt !!}</a>
                                                    </div>
                                                    {{-- <span>|</span>
                                                    <div class="social-network-actions text-left">
                                                        <div class="fb-like" data-href="https://nha-thuoc-suplo.myharavan.com/products/bo-2-hop-tinh-chat-lam-dep-collagen-adiva-28-lo-x-30ml" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                                    </div> --}}
                                                </div>
                                            </div>
                                            <div class="grid mg-left-5">
                                                <div class="grid__item large--one-quarter medium--one-quarter small--one-whole pd-left5">
                                                    <div class="product-quantity clearfix">
                                                        <div class="qty-addcart clearfix">
                                                            <span>Số lượng </span>
                                                            <input type="number" id="Quantity" name="quantity" value="1" min="1" class="quantity-selector">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid__item large--one-quarter medium--one-quarter small--one-half pd-left5">
                                                    <div class="product-actions clearfix">
                                                        <button type="submit" name="add" class="btnAddToCart">Thêm vào giỏ hàng</button>
                                                    </div>
                                                </div>
                                                <div class="grid__item large--one-quarter medium--one-quarter small--one-half pd-left5">
                                                    <div class="product-actions clearfix">
                                                        <button type="button" name="buy" class="btnBuyNow">
                                                            <a href="{{ url('mua-nhanh/'.$sanpham->Pro_id) }}"" style="color: #fff">Mua ngay</a>                                                         
                                                       </button>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </form>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="grid">
                                <div class="grid__item large--three-quarters medium--one-whole small--one-whole">
                                    <div class="product-description-wrapper">
                                        <div class="tab clearfix">
                                            <button class="pro-tablinks" onclick="openProTabs(event, 'protab1')" id="defaultOpenProTabs">Mô tả sản phẩm</button>
                                            {{-- <button class="pro-tablinks" onclick="openProTabs(event, 'protab2')">Hướng dẫn sử dụng</button> --}}
                                            <button class="pro-tablinks" onclick="openProTabs(event, 'proCom')">Bình luận</button>
                                        </div>
                                        <div id="protab1" class="pro-tabcontent">
                                            {!! $sanpham->MoTa !!}
                                        </div>
                                        {{-- <div id="protab2" class="pro-tabcontent">
                                            
                                        </div> --}}
                                        <div id="proCom" class="pro-tabcontent">
                                            <div class="fb-comments" data-href="{{ url('san-pham/'.$sanpham->Pro_id.'-'.$sanpham->Slug) }}.html" data-numposts="5"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="grid__item large--one-quarter medium--hide small--hide">
                                    <div class="product-buynow-sidebar-sticky">
                                        <div class="product-buynow-sidebar">
                                            <h3>
                                                {!! $sanpham->Pro_Name !!}
                                            </h3>
                                            <div class="pro-sku ProductSku">
                                                <span class="title">Mã SP:</span> <span class="sku-number">{{ $sanpham->MaSp }}</span>
                                            </div>
                                            <div class="product-buynow-img">
                                                <a href="javascript:;">
                                                <img class="lazyload" src="{{ asset('img/lazyload.jpg') }}" data-src="{{ asset('uploads/sanpham/'.$sanpham->Image) }}" alt="{!! $sanpham->Pro_Name !!}" />
                                                </a>
                                            </div>
                                            <div class="product-buynow-price text-center">
                                                <span class="current-price ProductPrice">{!! number_format($sanpham->Gia,0,',','.') !!}₫</span>
                                            </div>
                                            <div class="product-buynow-actions">
                                                <button type="button"  class="btnBuyNow"><span><a href="{{ url('mua-nhanh/'.$sanpham->Pro_id) }}"" style="color: #fff">Mua ngay</a></span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <section id="related-products">
                                <div class="home-section-head clearfix">
                                    <div class="section-title text-center">
                                        <h2>
                                            Sản phẩm liên quan
                                        </h2>
                                        <span class="section-title-border"></span>
                                    </div>
                                </div>
                                <div class="home-section-body">
                                    <div class="grid mg-left-15">
                                        <div id="owl-related-products-slider" class="owl-carousel owl-theme">

                                            @foreach ($sanphamlq as $items)
                                                <div class="item grid__item pd-left15">
                                                    <div class="product-item">
                                                        <div class="product-img">
                                                            <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">
                                                            <img id="1014835149" 
                                                                class="lazyload" src="{{ asset('img/lazyload.jpg') }}" data-src="{{ asset('uploads/sanpham/'.$items->Image) }}" 
                                                                alt="{{ $items->Pro_Name }}" />
                                                            </a>
                                                            <div class="tag-saleoff text-center">
                                                                -49%
                                                            </div>
                                                            <div class="product-actions text-center clearfix">
                                                                <div>
                                                                    
                                                                    <button type="button"  class="btnBuyNow"><span><a href="{{ url('mua-nhanh/'.$sanpham->Pro_id) }}"" style="color: #fff">Mua ngay</a></span></button>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="product-item-info text-center">
                                                            <div class="product-title">
                                                                <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">{{ $items->Pro_Name }}</a>
                                                            </div>
                                                            <div class="product-price clearfix">
                                                                <span class="current-price">{!! number_format($sanpham->Gia,0,',','.') !!}₫</span>
                                                                {{-- <span class="original-price"><s>660,000₫</s></span> --}}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            </section>
                           
                        </div>
                    </div>
                </div>
            </section>

        </main>
    </div>
@endsection

@section('plugins-js')
    <script src='{{ asset('frontend/js/jquery.flexslider.js') }}' type='text/javascript'></script>
    <link href='{{ asset('frontend/css/flexslider.css') }}' rel='stylesheet' type='text/css'  media='all'  />
    <script>
        function openProTabs(evt, cityName) {
            var i, pro_tabcontent, pro_tablinks;
            pro_tabcontent = document.getElementsByClassName("pro-tabcontent");
            for (i = 0; i < pro_tabcontent.length; i++) {
                pro_tabcontent[i].style.display = "none";
            }
            pro_tablinks = document.getElementsByClassName("pro-tablinks");
            for (i = 0; i < pro_tablinks.length; i++) {
                pro_tablinks[i].className = pro_tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
        
        document.getElementById("defaultOpenProTabs").click();
    </script>
@endsection
