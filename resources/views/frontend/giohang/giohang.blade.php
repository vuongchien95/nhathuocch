@extends('frontend.layout.master')

@section('title','Giỏ Hàng')
@section('keywords',$cauhinh->TuKhoa)
@section('description',$cauhinh->MoTa)
@section('url',url('/tin-tuc.html'))
@section('titleseo',$cauhinh->TuKhoa)
@section('type','news')
@section('descriptionseo',$cauhinh->TuKhoa)
@section('image')

@section('content')
    <section id="breadcrumb-wrapper">
        <div class="breadcrumb-overlay"></div>
        <div class="breadcrumb-content">
            <div class="wrapper">
                <div class="inner text-center">
                    <div class="breadcrumb-big">
                        <h2>
                            Giỏ hàng của bạn - Nhà thuốc Chính Hãng
                        </h2>
                    </div>
                    <div class="breadcrumb-small">
                        <a href="{{ url('/') }}" title="Quay trở về trang chủ">Trang chủ</a>
                        <span aria-hidden="true">/</span>
                        <span>Giỏ hàng của bạn - Nhà thuốc Chính Hãng</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            <div id="page-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        @if (Cart::count() > 0)
                        <h1>Giỏ hàng (<span id="sl_sp">{{ Cart::count() }}</span> sản phẩm)</h1>
                        <form action="{{ route('thanh-toan') }}" method="get" novalidate class="cart table-wrap medium--hide small--hide">
                            <table class="cart-table full table--responsive" id="load_grid">
                                <thead class="cart__row cart__header-labels">
                                    <th colspan="2" class="text-center">Thông tin chi tiết sản phẩm</th>
                                    <th class="text-center">Đơn giá</th>
                                    <th class="text-center">Số lượng</th>
                                    <th class="text-right">Tổng giá</th>
                                </thead>
                                
                                @foreach ($cart as $items)
                                <tbody>
                                    <tr class="cart__row table__section">
                                        <td data-label="Sản phẩm">
                                            <a href="{{ url('san-pham/'.$items->options['slug']) }}" class="cart__image">
                                            <img src="{{ asset('uploads/sanpham/'.$items->options['image']) }}" alt="{{ $items->name }}" style="height: 187px; width: 187px;">
                                            </a>
                                        </td>
                                        <td>
                                            <a href="{{ url('san-pham/'.$items->options['slug']) }}" class="h4">
                                            {{ $items->name }}
                                            </a>
                                            <a href="javascript:;" class="cart__remove xoaSanPham_{{ $items->id }}" onclick="return xoaCart({{ $items->id }})" data-id="{{ $items->rowId }}">
                                                <small style="color: red">Xóa</small>
                                            </a>
                                        </td>
                                        <td data-label="Đơn giá" class="text-center">
                                            <span class="h3">
                                            {{ number_format($items->price,0,',','.') }}₫
                                            </span>
                                        </td>
                                        <td data-label="Số lượng" class="text-center">
                                            <input type="number" class="qtyItem{{ $items->id }}" onchange="qtySp({{ $items->id }})" data-id="{{ $items->rowId }}" value="{{ $items->qty }}" min="0">
                                           
                                        </td>
                                        <td data-label="Tổng giá" class="text-right">
                                            <span class="h3">
                                            {!! number_format(($items->qty) * ($items->price),0,',','.') !!}₫
                                            </span>
                                        </td>
                                    </tr>
                                    
                                </tbody>
                                @endforeach
                                

                            </table>
                            <div class="grid cart__row">
                                
                                <div class="grid__item two-thirds small--one-whole">                                   
                                </div>
                                <div class="grid__item text-right one-third small--one-whole">
                                    <p>
                                        <span class="cart__subtotal-title">Tổng tiền</span>
                                        <span class="h3 cart__subtotal" id="tong_price">{{ $total }}₫</span>
                                    </p>
                                    <button type="button" name="update" class="btn update-cart"><a href="{{ url('/') }}" style="color: #fff">Tiếp tục mua hàng</a></button>
                                    <button type="submit" name="checkout" class="btn">Thanh toán</button>
                                </div>
                            </div>
                        </form>

                        {{-- mobile --}}
                        <form action="{{ route('thanh-toan') }}" method="get" novalidate class="cart table-wrap large--hide">
                            @foreach ($cart as $items)
                            <table class="cart-table full">
                                <div class="grid cart-item mg-left-0">
                                    <div data-label="Sản phẩm" class="grid__item medium--one-third small--one-third pd-left0">
                                        <a href="{{ url('san-pham/'.$items->options['slug']) }}" class="cart__image">
                                        <img src="{{ asset('uploads/sanpham/'.$items->options['image']) }}" alt="{{ $items->name }}">
                                        </a>
                                    </div>
                                    <div class="grid__item medium--two-thirds small--two-thirds pd-left15">
                                        <a href="{{ url('san-pham/'.$items->options['slug']) }}" class="h4">
                                        {{ $items->name }}
                                        </a>
                                        {{-- <small>Đức</small> --}}
                                        <div data-label="Số lượng">
                                            <input type="number" class="qtyItem{{ $items->id }}" onchange="qtySp({{ $items->id }})" data-id="{{ $items->rowId }}" value="{{ $items->qty }}" min="0">
                                        </div>
                                        <div data-label="Đơn giá" class="price">
                                            <span class="h3">
                                            {!! number_format(($items->qty) * ($items->price),0,',','.') !!}₫
                                            </span>
                                        </div>
                                        <a href="{{ route('gio-hang') }}" class="cart__remove xoaSanPham_{{ $items->id }}" onclick="return xoaCart({{ $items->id }})" data-id="{{ $items->rowId }}">
                                                <small style="color: red">Xóa</small>
                                        </a>
                                    </div>
                                </div>
                            </table>
                            @endforeach
                            <div class="grid cart__row">
                                <div class="grid__item text-right one-third small--one-whole">
                                    <p>
                                        <span class="cart__subtotal-title">Tổng tiền</span>
                                        <span class="h3 cart__subtotal">{{ $total }}₫</span>
                                    </p>
                                    {{-- <button type="button" name="update" class="btn update-cart">
                                            <a href="{{ url('cap-nhat-sl/'.$items->rowId.'/'.$items->qty) }}">Cập nhật</a>
                                    </button> --}}
                                    <button type="button" name="update" class="btn update-cart"><a href="{{ url('/') }}" style="color: #fff">Tiếp tục mua hàng</a></button>
                                    <button type="submit" name="checkout" class="btn">Thanh toán</button>
                                </div>
                            </div>
                        </form>
                        @else
                            <h1 class="text-center">Giỏ hàng trống</h1>
                        @endif
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection

