<footer id="footer">
    <div class="footer-content">
        <div class="wrapper">
            <div class="inner">
                <div class="grid-uniform">
                    <div class="grid__item large--one-quarter medium--one-half small--one-whole wow fadeInLeft">
                        <div class="ft-contact">
                            <h3 class="ft-title">
                                Thông tin liên hệ
                            </h3>
                            <div class="ft-contact-desc">
                                Nhà thuốc chính hãng cam kết nỗ lực hết mình nhằm cung cấp sản phẩm và dịch vụ đúng với những giá trị mà khách hàng mong đợi.
                            </div>
                            <div class="ft-contact-address">
                                <span class="ft-contact-icon"><i class="fas fa-map-marker-alt"></i></span>
                                <div class="ft-contact-detail">
                                    Địa chỉ: {!! $lienhe->DiaChi !!}
                                </div>
                            </div>
                            <div class="ft-contact-tel">
                                <span class="ft-contact-icon"><i class="fas fa-phone"></i></span>
                                <div class="ft-contact-detail">
                                    Số điện thoại: <a href="tel:{!! $lienhe->Sdt !!}">{!! $lienhe->Sdt !!}</a>
                                </div>
                            </div>
                            <div class="ft-contact-email">
                                <span class="ft-contact-icon"><i class="fas fa-envelope"></i></span>
                                <div class="ft-contact-detail">
                                    Email:  <a href="mailto:{!! $lienhe->Email !!}">{!! $lienhe->Email !!}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid__item large--one-quarter medium--one-half small--one-whole wow fadeInUp">
                        <div class="ft-nav">
                            <h3 class="ft-title">
                                Liên kết nhanh website
                            </h3>
                            <ul class="no-bullets">
                                <li>
                                    <i class="fas fa-caret-right"></i> 
                                    <a href="{{ url('/') }}">Trang chủ</a>
                                </li>
                                <li>
                                    <i class="fas fa-caret-right"></i> 
                                    <a href="{{ url('gioi-thieu.html') }}">Giới thiệu</a>
                                </li>
                                {{-- <li>
                                    <i class="fas fa-caret-right"></i> 
                                    <a href="collections\all.html">Sản phẩm</a>
                                </li> --}}
                                <li>
                                    <i class="fas fa-caret-right"></i> 
                                    <a href="{{ url('tin-tuc.html') }}">Tất cả tin tức</a>
                                </li>
                                <li>
                                    <i class="fas fa-caret-right"></i> 
                                    <a href="{{ url('lien-he.html') }}">Liên hệ</a>
                                </li>
                            </ul>
                            <div style="margin-top: 15px"></div>
                            <img src="{{ asset('img/bocongthuong.png') }}" alt="Nhà thuốc chính hãng">
                        </div>
                    </div>
                    <div class="grid__item large--one-quarter medium--one-half small--one-whole wow fadeInUp">
                        <div class="ft-nav">
                            <h3 class="ft-title">
                                Nhóm sản phẩm nổi bật
                            </h3>
                            <ul class="no-bullets">
                                @foreach ($danhmuccha as $cate)
                                <li>
                                    <i class="fas fa-caret-right"></i> 
                                    <a href="{{ url($cate->id.'-'.$cate->Slug) }}.html">{{ $cate->Name }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="grid__item large--one-quarter medium--one-half small--one-whole wow fadeInRight">
                        <div class="ft-subscribe">
                            <h3 class="ft-title">
                                Đăng ký nhận tin
                            </h3>
                            <div class="ft-subscribe-desc">
                                Mỗi tháng chúng tôi đều có những đợt giảm giá dịch vụ và sản phẩm nhằm chi ân khách hàng. Để có thể cập nhật kịp thời những đợt giảm giá này, vui lòng nhập địa chỉ email của bạn vào ô dưới đây.
                            </div>
                            <div class="ft-sub-wrapper">
                                <form accept-charset='UTF-8' action='{{ url('send-mail') }}' class='contact-form' method='post'>
                                    {{ csrf_field() }}
                                    <input name='form_type' type='hidden' value='customer'>
                                    <input name='utf8' type='hidden' value='✓'>
                                    <input type="email" value="" placeholder="Nhập địa chỉ email của bạn..." name="contact[email]" id="Email" required="">                  
                                    <button type="submit" name="subscribe" id="subscribe" value="GỬI"><i class="fab fa-telegram-plane"></i></button>                  
                                </form>
                            </div>
                        </div>
                        <div class="ft-social">
                            <a href="https://www.facebook.com/nhathuocchinhhang/"><i class="fab fa-facebook-f"></i></a>
                            <a href="https://twitter.com/"><i class="fab fa-twitter"></i></a>
                            <a href="https://vn.linkedin.com/"><i class="fab fa-linkedin-in"></i></a>
                            <a href="https://plus.google.com/"><i class="fab fa-google-plus-g"></i></a>
                            <a href="https://www.youtube.com/"><i class="fab fa-youtube"></i></a>
                            <a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyrights">
        <div class="wrapper">
            <div class="inner">
                <div class="grid">
                    {{-- <div class="grid__item large--one-half medium--one-whole small--one-whole float-right">
                        <div class="ft-copyrights-link text-right">
                            <ul class="no-bullets">
                                <li class="wow fadeInRight" data-wow-delay="0.2s" data-wow-duration="0.75s"><a href="index.html">Điều khoản sử dụng</a></li>
                                <li class="wow fadeInRight" data-wow-delay="0.4s" data-wow-duration="0.75s"><a href="index.html">Chính sách bảo mật</a></li>
                                <li class="wow fadeInRight" data-wow-delay="0.6s" data-wow-duration="0.75s"><a href="index.html">Câu hỏi thường gặp</a></li>
                            </ul>
                        </div>
                    </div> --}}
                    <div class="grid__item large--one-half medium--one-whole small--one-whole">
                        <div class="ft-copyrights-content wow fadeInLeft" data-wow-duration="0.75s" data-wow-delay="0.2s">
                            Copyrights © 2018 by <a target="_blank" href="{{ url('/') }}">Nhà Thuốc Chính Hãng</a>. <a target="_blank" href="https://www.facebook.com/vuongchien.2412">Powered by VuongChien</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<div id="fixed-social-network" class="medium--hide small--hide">
    <a href="https://www.facebook.com/nhathuocchinhhang/" class="fb-icon"><i class="fab fa-facebook-f"></i> Facebook</a>
    <a href="https://www.instagram.com/" class="ins-icon"><i class="fab fa-instagram"></i> Instagram</a>
    <a href="https://www.youtube.com/" class="yt-icon"><i class="fab fa-youtube"></i> Youtube</a>
    <a href="https://twitter.com/" class="tw-icon"><i class="fab fa-twitter"></i> Twitter</a>
    <a href="https://plus.google.com/" class="gg-icon"><i class="fab fa-google-plus-g"></i> Google+</a>
    <a href="https://vn.linkedin.com/" class="linkedin-icon"><i class="fab fa-linkedin-in"></i> Linkedin</a>
    <a href="javascript:void(0)" id="back-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i> Top</a>
</div>

<div id="modalAddComplete"></div>
<button type="button" id="modalAddCompleteBtn" style="display: none;"></button>

<!-- /snippets/ajax-cart-template.liquid -->




<script>
    new WOW().init();
</script>
<script>
    initCartHeader();
    
    function initCartHeader() {
        var box1 = document.querySelector('.desktop-cart-wrapper .quickview-cart');
        var box2 = document.querySelector('.desktop-cart-wrapper1 .quickview-cart');
    
        function show1() {
            //console.log('showing');
            $(".cart-overlay1").addClass('open');   
            box1.style.display = 'block';   
        }
    
        function hide1() {
            //console.log('hiding');
            $(".cart-overlay1").removeClass('open');    
            box1.style.display = 'none';    
        }
    
        $(".desktop-cart-wrapper .btnCloseQVCart").click(function(){
            hide1();
        });
    
        var outside1 = function(event) {
            if (!box1.contains(event.target)) {
                hide1();
                //console.log('removing outside listener on document')
                this.removeEventListener(event.type, outside1);
            }
        }
    
        document.querySelector('.desktop-cart-wrapper > a').addEventListener('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
            show1();
            document.addEventListener('click', outside1);
        });
    
        function show2() {
            //console.log('showing');
            $(".cart-overlay1").addClass('open');   
            box2.style.display = 'block';   
        }
    
        function hide2() {
            //console.log('hiding');
            $(".cart-overlay1").removeClass('open');    
            box2.style.display = 'none';    
        }
    
        $(".desktop-cart-wrapper1 .btnCloseQVCart").click(function(){
            hide2();
        });
    
        var outside2 = function(event) {
            if (!box2.contains(event.target)) {
                hide2();
                //console.log('removing outside listener on document')
                this.removeEventListener(event.type, outside2);
            }
        }
    
        document.querySelector('.desktop-cart-wrapper1 > a').addEventListener('click', function(event) {
            event.preventDefault();
            event.stopPropagation();
            show2();
            document.addEventListener('click', outside2);
        });
    
    }
</script>
<script>
    function openHmpTab1(evt, cityName) {
        var i, hmp_tabcontent1, hmp_tablinks1;
        hmp_tabcontent1 = document.getElementsByClassName("hmp-tabcontent1");
        for (i = 0; i < hmp_tabcontent1.length; i++) {
            hmp_tabcontent1[i].style.display = "none";
        }
        hmp_tablinks1 = document.getElementsByClassName("hmp-tablinks1");
        for (i = 0; i < hmp_tablinks1.length; i++) {
            hmp_tablinks1[i].className = hmp_tablinks1[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
    
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpenHmpTab1").click();
</script>

<!-- Owl carousel init -->
<script>
    $(document).ready(function() {
        $("#owl-related-products-slider").owlCarousel({
            items: 5,
            itemsDesktop: [1000, 5],
            itemsDesktopSmall: [900, 4],
            itemsTablet: [768, 3],
            itemsMobile: [480, 2],
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
        $("#owl-home-featured-products-slider").owlCarousel({
            items: 1,
            itemsDesktop: [1000, 1],
            itemsDesktopSmall: [900, 1],
            itemsTablet: [768, 3],
            itemsMobile: [480, 2],
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            autoPlay: 5000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
        $("#owl-isb-testi-slider").owlCarousel({
            items: 1,
            itemsDesktop: [1000, 1],
            itemsDesktopSmall: [900, 1],
            itemsTablet: [768, 2],
            itemsMobile: [480, 1],
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            autoPlay: 5000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    
        $("#owl-home-articles-slider").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [768, 2],
            itemsMobile: [480, 1],
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    
    
        $("#owl-brands-slider").owlCarousel({
            items: 6,
            itemsDesktop: [1000, 6],
            itemsDesktopSmall: [900, 5],
            itemsTablet: [768, 4],
            itemsMobile: [480, 2],
            navigation: false,
            pagination: false,
            slideSpeed: 1000,
            autoPlay: 3000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    
        $("#owl-customize-variants-products-slider").owlCarousel({
            items: 5,
            itemsDesktop: [1000, 5],
            itemsDesktopSmall: [900, 5],
            itemsTablet: [768, 4],
            itemsMobile: [480, 3],
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            autoPlay: 3000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    
        $("#owl-rpa-slider").owlCarousel({
            items: 5,
            itemsDesktop: [1000, 5],
            itemsDesktopSmall: [900, 5],
            itemsTablet: [768, 4],
            itemsMobile: [480, 3],
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    
        $("#owl-blog-single-slider1").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    
        $("#owl-blog-single-slider2").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    
        $("#owl-blog-single-slider3").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    
        $("#owl-blog-single-slider4").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    
        $("#owl-blog-single-slider5").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    
        $("#owl-blog-single-slider6").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    
        $("#owl-blog-single-slider7").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    
        $("#owl-blog-single-slider8").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    
        $("#owl-blog-single-slider9").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    
        $("#owl-blog-single-slider10").owlCarousel({
            items: 2,
            itemsDesktop: [1000, 2],
            itemsDesktopSmall: [900, 2],
            itemsTablet: [600, 1],
            itemsMobile: false,
            navigation: true,
            pagination: false,
            slideSpeed: 1000,
            paginationSpeed: 1000,
            navigationText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
        });
    });
</script>
<!-- Back to top -->
<script>
    jQuery(document).ready(function() {
        var offset = 220;
        var duration = 500;
        jQuery(window).scroll(function() {
            if (jQuery(this).scrollTop() > offset) {
                jQuery('#back-to-top').fadeIn(duration);
            } else {
                jQuery('#back-to-top').fadeOut(duration);
            }
        });
    
        jQuery('#back-to-top').click(function(event) {
            event.preventDefault();
            jQuery('html, body').animate({
                scrollTop: 0
            }, duration);
            return false;
        });
    
        window.onscroll = changePos;
    
        function changePos() {
            var header = $("#header");
            var headerheight = $("#header").height();
            if (window.pageYOffset > headerheight) {        
                header.addClass('scrolldown');
            } else {
                header.removeClass('scrolldown');
            }
        }
    });
</script>
<!-- Validate quantity form & trigger zoom -->
<script>
    $("document").ready(function() {
        $(function() {
            $(".js-qty__num").keypress(function(event) {
                if (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57)) {
                    return false;
                }
            });
        });
        if($(window).width()> 768){
            setTimeout(function() {
                $("#ProductThumbs .thumbnail-item:first-child a").trigger('click');
            },3000);
        };
    });
</script>
<!-- popup loaded -->
<script>
    $(document).ready(function(){
        var date = new Date();
        var minutes = 60;
        date.setTime(date.getTime() + (minutes * 60 * 1000));
    
        if (getCookie('popupNewLetterStatus') != 'closed') {
            $('#popup-btn').trigger('click');
            setCookie('popupNewLetterStatus', 'closed', date);
        };
    })
</script>




