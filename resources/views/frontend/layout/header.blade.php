@php
    $menu = DB::table('danhmuc')->where('DanhMuc_Cha', 0)->get();
@endphp
<div id="NavDrawer" class="drawer drawer--right">
    <div class="drawer__header">
        <div class="drawer__close js-drawer-close">
            <button type="button" class="icon-fallback-text">
            <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <!-- begin mobile-nav -->
    <ul class="mobile-nav">
        <li class="mobile-nav__item mobile-nav__search">
            <form action="{{ url('tim-kiem.html') }}" method="get" class="input-group search-bar" role="search">
                <input type="hidden" name="type" value="product">
                <input type="search" id="main-search-form-input" name="search" value="" placeholder="Tìm sản phẩm..." class="input-group-field" aria-label="Tìm sản phẩm...">
                <span class="input-group-btn">
                <button type="submit" class="btn icon-fallback-text">
                <i class="fa fa-search" aria-hidden="true"></i>
                </button>
                </span>
                
            </form>
        </li>
        <li class="mobile-nav__item mobile-nav__item--active">
            <a href="{{ url('/') }}" class="mobile-nav__link">Trang chủ</a>
        </li>
        <li class="mobile-nav__item">
            <a href="{{ url('gioi-thieu.html') }}" class="mobile-nav__link">Giới thiệu</a>
        </li>

        <li class="mobile-nav__item" aria-haspopup="true">
            <div class="mobile-nav__has-sublist">
                <a href="javascript:void" class="mobile-nav__link">Sản phẩm</a>
                <div class="mobile-nav__toggle">
                    <button type="button" class="icon-fallback-text mobile-nav__toggle-open">
                    <span class="icon icon-plus" aria-hidden="true"></span>
                    <span class="fallback-text">See More</span>
                    </button>
                    <button type="button" class="icon-fallback-text mobile-nav__toggle-close">
                    <span class="icon icon-minus" aria-hidden="true"></span>
                    <span class="fallback-text">"Đóng"</span>
                    </button>
                </div>
            </div>
            <ul class="mobile-nav__sublist">
                @foreach ($menu as $items)
                    @php
                        $subMenu_mb = DB::table('danhmuc')->where('DanhMuc_Cha',$items->id )->get();
                    @endphp
                    <li class="mobile-nav__item" aria-haspopup="true">
                        <div class="mobile-nav__has-sublist">
                            <a href="{{ url($items->id.'-'.$items->Slug) }}.html" class="mobile-nav__link">{{ $items->Name }}</a>
                            @if(count($subMenu_mb) > 0)
                                <div class="mobile-nav__toggle">
                                    <button type="button" class="icon-fallback-text mobile-nav__toggle-open">
                                    <span class="icon icon-plus" aria-hidden="true"></span>
                                    <span class="fallback-text">See More</span>
                                    </button>
                                    <button type="button" class="icon-fallback-text mobile-nav__toggle-close">
                                    <span class="icon icon-minus" aria-hidden="true"></span>
                                    <span class="fallback-text">"Đóng"</span>
                                    </button>
                                </div>
                            @endif
                        </div>
                        @if(count($subMenu_mb) > 0)
                            <ul class="mobile-nav__sublist">
                                @foreach ($subMenu_mb as $items_mb)
                                    <li class="mobile-nav__item ">
                                        <a href="{{ url($items_mb->id.'-'.$items_mb->Slug) }}.html" class="mobile-nav__link">{{ $items_mb->Name }}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                @endforeach
            </ul>
        </li>

        <li class="mobile-nav__item" aria-haspopup="true">
            <div class="mobile-nav__has-sublist">
                <a href="{{ url('tin-tuc.html') }}" class="mobile-nav__link">Tin tức</a>
            </div>
        </li>

        <li class="mobile-nav__item">
            <a href="{{ url('lien-he.html') }}" class="mobile-nav__link">Liên hệ</a>
        </li>
        @if(Auth::check())
        <li class="mobile-nav__item">
            <a href="javascript:void" id="customer_login_link">{{Auth::user()->name}}</a>
        </li>
        <li class="mobile-nav__item">
            <a href="{{ url('dang-xuat.html') }}">Đăng xuất</a>
        </li>
        @else
        <li class="mobile-nav__item">
            <a href="{{ url('dang-nhap.html') }}" id="customer_login_link">Đăng nhập</a>
        </li>
        <li class="mobile-nav__item">
            <a href="{{ url('dang-ky.html') }}" id="customer_register_link">Đăng kí</a>
        </li>
        @endif
    </ul>
    <!-- //mobile-nav -->
</div>
<div class="cart-overlay"></div>
<div id="CartDrawer" class="drawer drawer--left">
    <div class="drawer__header">
        <div class="drawer__title h3">Giỏ hàng</div>
        <div class="drawer__close js-drawer-close">
            <button type="button" class="icon-fallback-text">
            <span class="icon icon-x" aria-hidden="true"></span>
            <span class="fallback-text">"Đóng"</span>
            </button>
        </div>
    </div>
    <div id="CartContainer"></div>
</div>
<header id="header">
    <div class="desktop-header medium--hide small--hide">
        <div class="desktop-header-top">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">
                        <div class="grid__item large--one-third">
                            <div class="hdt-left-contact wow fadeInLeft" data-wow-duration="0.75s" data-wow-delay="0.2s">
                                <a href="tel:{!! $lienhe->Sdt !!}"><i class="fas fa-phone"></i> (+84) {!! $lienhe->Sdt !!}</a>
                                <a href="mailto:{!! $lienhe->Email !!}"><i class="fas fa-envelope"></i> {!! $lienhe->Email !!}</a>
                            </div>
                        </div>
                        <div class="grid__item large--two-thirds text-right">
                            {{-- <div class="hdt-right-search">
                                <div class="search-form-wrapper">
                                    <form action="{{ url('tim-kiem.html') }}" method="get" role="search" class="searchform-categoris ultimate-search">
                                        <div class="wpo-search">
                                            <div class="wpo-search-inner">
                                               
                                                <div class="input-group">
                                                    <input type="hidden" name="type" value="product">
                                                    <input id="searchtext" name="search" maxlength="40" class="form-control input-search" type="text" size="20" placeholder="Tìm kiếm sản phẩm...">
                                                    <span class="input-group-btn">
                                                    <button type="submit"><i class="fas fa-search"></i></button>
                                                    </span>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </form>

                                    <div class="smart-search-wrapper search-wrapper" id="showItems">
                                    </div>
                                </div>
                            </div> --}}

                            @if (Auth::check())
                            <div class="hdt-account">
                                <a href="javascript:void" class="wow fadeInRight" data-wow-duration="0.75s" data-wow-delay="0.8s">{{Auth::user()->name}}</a>
                                <a class="wow fadeInRight" data-wow-duration="0.75s" data-wow-delay="1s" href="{{ url('dang-xuat') }}.html" onclick="return confirm('Bạn có muốn đăng xuất?')" title="Đăng Xuất">Đăng xuất</a>
                            </div>
                            @else
                            <div class="hdt-account">
                                <a href="{{ url('dang-nhap') }}.html" class="wow fadeInRight" data-wow-duration="0.75s" data-wow-delay="0.8s">Đăng nhập</a>
                                <a href="{{ url('dang-ky') }}.html" class="wow fadeInRight" data-wow-duration="0.75s" data-wow-delay="1s">Đăng kí</a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="desktop-header-center">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">
                        <div class="grid__item large--three-twelfths">
                            <div class="hd-logo wow fadeInUp">
                                <h1>
                                    <a href="{{ url('/') }}">
                                    Nhà Thuốc Chính Hãng<img src="{{ asset('uploads/images/logo/'.$logo->Image) }}" alt="Nhà Thuốc Chính Hãng">
                                    </a>
                                </h1>
                            </div>
                        </div>

                        <div class="grid__item large--nine-twelfths text-right">
                            <div class="desktop-header-navbar">
                                <ul class="no-bullets">
                                    <li class="active 
                                        wow fadeInRight 
                                        " data-wow-delay="0.2s" data-wow-duration="0.75s">
                                        <a href="{{ url('/') }}">Trang chủ </a>
                                    </li>
                                    <li class=" 
                                        wow fadeInRight 
                                        " data-wow-delay="0.4s" data-wow-duration="0.75s">
                                        <a href="{{ url('gioi-thieu.html') }}">Giới thiệu </a>
                                    </li>

                                    <li class=" 
                                        wow fadeInRight 
                                        dropdown" data-wow-delay="0.6s" data-wow-duration="0.75s">
                                        <a href="javascript:void">Sản phẩm <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                        
                                        <ul class="no-bullets">
                                            @foreach ($menu as $items)
                                                @php
                                                    $subMenu = DB::table('danhmuc')->where('DanhMuc_Cha',$items->id )->get();
                                                @endphp
                                                <li class='@if(count($subMenu) > 0 ) wow fadeInRight dropdown @endif' data-wow-delay="0.5s" data-wow-duration="0.65s">
                                                    <a href="{{ url($items->id.'-'.$items->Slug) }}.html">
                                                
                                                    {{ $items->Name }}
                                                
                                                        @if(count($subMenu) > 0 )
                                                            <i class="fas fa-caret-right"></i> 
                                                        @endif
                                                    </a>
                                                    @if(count($subMenu) > 0 )
                                                    <ul class="no-bullets">
                                                        @foreach ($subMenu  as $sub_items)
                                                            <li class="">
                                                                <i class="fas fa-caret-right"></i> 
                                                                <a href="{{ url($sub_items->id.'-'.$sub_items->Slug) }}.html">{{ $sub_items->Name }} </a>
                                                            </li>
                                                        @endforeach
                                                        
                                                    </ul>
                                                    @endif
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>

                                    <li class=" 
                                        wow fadeInRight 
                                        dropdown" data-wow-delay="0.8s" data-wow-duration="0.75s">
                                        <a href="{{ url('tin-tuc.html') }}">Tin tức</a>
                                    </li>
                                    <li class=" 
                                        wow fadeInRight 
                                        " data-wow-delay="1.2s" data-wow-duration="0.75s">
                                        <a href="{{ url('lien-he.html') }}">Liên hệ </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="desktop-cart-wrapper wow fadeInRight">
                                <a href="javascript:void(0)" class="hd-cart" id="sl_sp_hd">
                                    <i class="fas fa-shopping-bag"></i>
                                    <span class="hd-cart-count" >{{ Cart::count() }}</span>
                                </a>
                                
                                @if(Cart::count() > 0)
                                <div class="quickview-cart" id="view-cart">
                                    <h3>
                                        Giỏ hàng của tôi
                                        
                                        <span class="btnCloseQVCart"><svg class="svg-inline--fa fa-times fa-w-12" aria-hidden="true" data-prefix="fa" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" data-fa-i2svg=""><path fill="currentColor" d="M323.1 441l53.9-53.9c9.4-9.4 9.4-24.5 0-33.9L279.8 256l97.2-97.2c9.4-9.4 9.4-24.5 0-33.9L323.1 71c-9.4-9.4-24.5-9.4-33.9 0L192 168.2 94.8 71c-9.4-9.4-24.5-9.4-33.9 0L7 124.9c-9.4 9.4-9.4 24.5 0 33.9l97.2 97.2L7 353.2c-9.4 9.4-9.4 24.5 0 33.9L60.9 441c9.4 9.4 24.5 9.4 33.9 0l97.2-97.2 97.2 97.2c9.3 9.3 24.5 9.3 33.9 0z"></path></svg><!-- <i class="fa fa-times" aria-hidden="true"></i> --></span>
                                    </h3>
                                    <ul class="no-bullets" id="cart_hader">
                                        @foreach ($cart as $items)
                                            <li class="cart-item">
                                                <a href="javascript:void"  class="cart__remove xoaSanPham_{{ $items->id }}" onclick="return xoaCart({{ $items->id }})" data-id="{{ $items->rowId }}">
                                                    <svg class="svg-inline--fa fa-times fa-w-12" aria-hidden="true" data-prefix="fa" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" data-fa-i2svg=""><path fill="currentColor" d="M323.1 441l53.9-53.9c9.4-9.4 9.4-24.5 0-33.9L279.8 256l97.2-97.2c9.4-9.4 9.4-24.5 0-33.9L323.1 71c-9.4-9.4-24.5-9.4-33.9 0L192 168.2 94.8 71c-9.4-9.4-24.5-9.4-33.9 0L7 124.9c-9.4 9.4-9.4 24.5 0 33.9l97.2 97.2L7 353.2c-9.4 9.4-9.4 24.5 0 33.9L60.9 441c9.4 9.4 24.5 9.4 33.9 0l97.2-97.2 97.2 97.2c9.3 9.3 24.5 9.3 33.9 0z"></path></svg>
                                                </a>
                                                <div class="grid mg-left-15">
                                                    <div class="grid__item large--four-twelfths medium--four-twelfths small--four-twelfths pd-left15">
                                                        <div class="cart-item-img text-center">
                                                            <a href="{{ url('san-pham/'.$items->id.'-'.$items->options['slug']) }}.html">
                                                                
                                                                <img src="{{ asset('uploads/sanpham/'.$items->options['image']) }}" alt="{{ $items->name }}">
                                                                
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="grid__item large--eight-twelfths medium--eight-twelfths small--eight-twelfths pd-left15">
                                                        <div class="cart-item-info text-left">
                                                            <a href="{{ url('san-pham/'.$items->id.'-'.$items->options['slug']) }}.html">{{ $items->name }}</a> 
                                                            
                                                        </div>
                                                        <div class="cart-item-price-quantity text-left">
                                                            <span class="quantity">Số lượng: {{ $items->qty }}</span>
                                                            <span class="current-price">Giá/sp: {{ number_format($items->price,0,',','.') }}₫</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                        
                                    </ul>
                                    
                                    <div class="qv-cart-total">
                                        Tạm tính: <span id="total_price_hd">{{ $total }}₫</span>
                                    </div>
                                    <div class="quickview-cartactions clearfix">
                                        <a href="{{ route('gio-hang') }}">Xem giỏ hàng</a>
                                        <a href="{{ route('thanh-toan') }}">Thanh toán</a>
                                    </div>
                                    
                                </div>
                                @else
                                <div class="quickview-cart">
                                    <h3>
                                        Giỏ hàng trống
                                        <span class="btnCloseQVCart"><i class="fa fa-times" aria-hidden="true"></i></span>
                                    </h3>
                                    <ul class="no-bullets">
                                        <li>Bạn chưa có sản phẩm nào trong giỏ hàng!</li>
                                    </ul>
                                </div>
                                @endif
                            </div>
                        </div>

                        <div class="grid__item large--nine-twelfths text-right">
                            <div class="search-form-wrapper">
                                <form action="{{ url('tim-kiem.html') }}" method="get" role="search" class="searchform-categoris ultimate-search">
                                    <div class="wpo-search">
                                        <div class="wpo-search-inner">
                                           
                                            <div class="input-group">
                                                <input type="hidden" name="type" value="product">
                                                <input id="searchtext" name="search" maxlength="40" class="form-control input-search" type="text" size="20" placeholder="Tìm kiếm sản phẩm...">
                                                <span class="input-group-btn">
                                                <button type="submit" class="btn-search"><i class="fas fa-search"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </form>

                                <div class="smart-search-wrapper search-wrapper" id="showItems">
                                </div>
                            </div>
                        </div>
                        

                    </div>
                </div>
            </div>

        </div>

    </div>
    <div class="mobile-header large--hide">
        <div class="wrapper">
            <div class="inner">
                <div class="grid">
                    <div class="grid__item medium--one-third small--one-third">
                        <div class="hd-logo text-left">
                            <a href="{{ url('/') }}">
                            <img src="{{ asset('uploads/images/logo/'.$logo->Image) }}" alt="Nhà Thuốc Chính Hãng">
                            </a>
                        </div>
                    </div>
                    <div class="grid__item large--two-twelfths push--large--eight-twelfths medium--two-thirds small--two-thirds clearfix text-right">

                        <div class="desktop-cart-wrapper1">
                            <a href="javascript:void(0)" class="hd-cart" id="sl_sp_hd_mb">
                            <i class="fas fa-shopping-bag"></i><span class="hd-cart-count">{{ Cart::count() }}</span>
                            </a>
                            @if(Cart::count() >0)
                            <div class="quickview-cart">
                                <h3>
                                    Giỏ hàng của tôi
                                    <span class="btnCloseQVCart">
                                        <svg class="svg-inline--fa fa-times fa-w-12" aria-hidden="true" data-prefix="fa" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" data-fa-i2svg="">
                                            <path fill="currentColor" d="M323.1 441l53.9-53.9c9.4-9.4 9.4-24.5 0-33.9L279.8 256l97.2-97.2c9.4-9.4 9.4-24.5 0-33.9L323.1 71c-9.4-9.4-24.5-9.4-33.9 0L192 168.2 94.8 71c-9.4-9.4-24.5-9.4-33.9 0L7 124.9c-9.4 9.4-9.4 24.5 0 33.9l97.2 97.2L7 353.2c-9.4 9.4-9.4 24.5 0 33.9L60.9 441c9.4 9.4 24.5 9.4 33.9 0l97.2-97.2 97.2 97.2c9.3 9.3 24.5 9.3 33.9 0z"></path>
                                        </svg>
                                        <!-- <i class="fa fa-times" aria-hidden="true"></i> -->
                                    </span>
                                </h3>
                                <ul class="no-bullets" id="view-cart-mb">
                                    @foreach ($cart as $items)
                                    <li class="cart-item">
                                        <a href="javascript:void"  class="cart__remove xoaSanPham_{{ $items->id }}" onclick="return xoaCart({{ $items->id }})" data-id="{{ $items->rowId }}">
                                                <svg class="svg-inline--fa fa-times fa-w-12" aria-hidden="true" data-prefix="fa" data-icon="times" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512" data-fa-i2svg="">
                                                <path fill="currentColor" d="M323.1 441l53.9-53.9c9.4-9.4 9.4-24.5 0-33.9L279.8 256l97.2-97.2c9.4-9.4 9.4-24.5 0-33.9L323.1 71c-9.4-9.4-24.5-9.4-33.9 0L192 168.2 94.8 71c-9.4-9.4-24.5-9.4-33.9 0L7 124.9c-9.4 9.4-9.4 24.5 0 33.9l97.2 97.2L7 353.2c-9.4 9.4-9.4 24.5 0 33.9L60.9 441c9.4 9.4 24.5 9.4 33.9 0l97.2-97.2 97.2 97.2c9.3 9.3 24.5 9.3 33.9 0z"></path>
                                            </svg>
                                            <!-- <i class="fa fa-times" aria-hidden="true"></i> -->
                                        </a>
                                        <div class="grid mg-left-15">
                                            <div class="grid__item large--four-twelfths medium--four-twelfths small--four-twelfths pd-left15">
                                                <div class="cart-item-img text-center">
                                                    <a href="{{ url('san-pham/'.$items->id.'-'.$items->options['slug']) }}.html">
                                                    <img src="{{ asset('uploads/sanpham/'.$items->options['image']) }}" alt="{{ $items->name }}">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="grid__item large--eight-twelfths medium--eight-twelfths small--eight-twelfths pd-left15">
                                                <div class="cart-item-info text-left">
                                                    <a href="{{ url('san-pham/'.$items->id.'-'.$items->options['slug']) }}.html">{{ $items->name }}</a> 
                                                    {{-- <small>Đức</small> --}}
                                                </div>
                                                <div class="cart-item-price-quantity text-left">
                                                    <span class="quantity">Số lượng: {{ $items->qty }}</span>
                                                    <span class="current-price">Giá/sp: {{ number_format($items->price,0,',','.') }}₫</span>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                                <div class="qv-cart-total">
                                    Tạm tính: <span id="total_price_hd_mb">{{ $total }}₫</span>
                                </div>
                                <div class="quickview-cartactions clearfix">
                                    <a href="{{ route('gio-hang') }}">Xem giỏ hàng</a>
                                    <a href="{{ route('thanh-toan') }}">Thanh toán</a>
                                </div>
                            </div>
                            @else
                            <div class="quickview-cart">
                                <h3>
                                    Giỏ hàng trống
                                    <span class="btnCloseQVCart"><i class="fa fa-times" aria-hidden="true"></i></span>
                                </h3>
                                <ul class="no-bullets">
                                    <li>Bạn chưa có sản phẩm nào trong giỏ hàng!</li>
                                </ul>
                            </div>
                            @endif
                        </div>
                        <div class="hd-btnMenu">
                            <a href="javascript:void(0)" class="icon-fallback-text site-nav__link js-drawer-open-right" aria-controls="NavDrawer" aria-expanded="false">
                            <i class="fas fa-bars"></i>
                            </a>
                        </div>
                    </div>
                    
                    <div class="grid__item" style="margin-top: 10px">
                        <form action="{{ url('tim-kiem.html') }}" method="get" class="input-group search-bar" role="search">
                            <input type="hidden" name="type" value="product">
                            <input type="search" id="main-search-form-input" name="search" value="" placeholder="Tìm sản phẩm..." class="input-group-field" aria-label="Tìm sản phẩm...">
                            <span class="input-group-btn">
                            <button type="submit" class="btn icon-fallback-text">
                            <i class="fa fa-search" aria-hidden="true"></i>
                            </button>
                            </span>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<style>

    .search-form-wrapper{
        position: relative;
       
        width: 100%;
    }
    #searchtext
    {
        float: right;
        width: 39.7%;
    }
    .btn-search{
        height: 40px;
        line-height: 30px;
        width: 31px;
        text-align: center;
        background: #ffffff;
        color: #f66d52;
        font-size: 14px;
        padding: 0;
        margin: 0;
        outline: 0;
        border: 1px solid #e9e9e9;
        border-left: none;
        border-radius: 0;
    }
    .smart-search-wrapper>a.thumbs{   
    width: 32px;
    display: inline-block;
    padding: 5px 0px;
    /*height: 300px;
    overflow: scroll;*/
    }



    .smart-search-wrapper>a.thumbs img {
    position: absolute;
    top: 0px;
    width: 32px;
    height: 35px;
    left: 0px;
    }
    .smart-search-wrapper{
    position: absolute;
    /*display: none;*/
    left: 0 !important;
    right: 0 !important;
    top: 100% !important;
    background: #fff;
    /*border: 1px solid rgb(215, 215, 215);*/
    border-top: none;
    max-height: 300px;
    overflow-y: scroll;
        margin-left: 58.2%;
    z-index: 99;

    }

    .smart-search-wrapper>a {
    width: calc(100% - 32px);
    float: left;
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: pre;
    color: #686767;
    text-decoration: none;
    line-height: 29px;
    font-size: 13px;
    font-family: sans-serif;
    padding: 5px 160px 5px 5px;
    position: relative;
    height: 35px;
    }

    .smart-search-wrapper>a.select, .smart-search-wrapper>a:hover {
    background: -webkit-linear-gradient(left, #fff,#EAEAEA); /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(left, #fff,#EAEAEA); /* For Opera 11.1 to 12.0 */
    background: -moz-linear-gradient(left, #fff,#EAEAEA ); /* For Firefox 3.6 to 15 */
    background: linear-gradient(left, #fff,#EAEAEA ); /* Standard syntax (must be last) */
    color: #000;
    }

    .smart-search-wrapper>a>span.price-search {
    position: absolute;
    right: 5px;
    top:0px;
    }

</style>
<script type="text/javascript">
    $(document).ready(function(){
        $('#searchtext').keyup(function(){
            var key = $('#searchtext').val() ? $('#searchtext').val() : null;
            $.get('/search/'+key,function(data){
                $('#showItems').html(data.html);
            });
        });
    });
</script>