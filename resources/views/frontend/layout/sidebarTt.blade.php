<div class="grid__item large--three-twelfths medium--one-whole small--one-whole">
    <div class="blog-sidebar">
        {{-- <div class="list-categories">
            <div class="blog-sb-title clearfix">
                <h3>
                    Danh mục tin tức
                </h3>
            </div>
            <ul class="no-bullets">
                <li class="active">
                    <a href="/blogs/news">
                    <span class="isb-cate-icon"><i class="fas fa-caret-right"></i></span>
                    <span class="isb-cate-text">Tin tức</span>
                    </a>
                </li>
                <li class="">
                    <a href="/blogs/goc-suc-khoe">
                    <span class="isb-cate-icon"><i class="fas fa-caret-right"></i></span>
                    <span class="isb-cate-text">Góc sức khỏe</span>
                    </a>
                </li>
                <li class="">
                    <a href="/blogs/tuyen-dung">
                    <span class="isb-cate-icon"><i class="fas fa-caret-right"></i></span>
                    <span class="isb-cate-text">Tuyển dụng</span>
                    </a>
                </li>
                <li class="">
                    <a href="/blogs/su-kien">
                    <span class="isb-cate-icon"><i class="fas fa-caret-right"></i></span>
                    <span class="isb-cate-text">Sự kiện</span>
                    </a>
                </li>
            </ul>
        </div> --}}
        <style type="text/css" media="screen">
            .blog-item-list{
                padding: 20px 0px 20px 0px;
                margin: 0px 0px !important;
                position: relative;
                border-bottom: dashed 1px #ebebeb;
            }
            .blog-item-thumbnail{
                    display: inline-block;
                    float: left;
                    width: 100%;
                    width: 90px;
                    margin-right: 15px;
            }
        
            .time_post{
                float: right;
                padding-right: 15px;
            }
            
        </style>
        <div class="blog-sb-banner">
            <a href="http://member.civi.vn/cpc/?sid=31536&bid=10052603" target="_blank"><img  src="http://member.civi.vn/imp/b/?sid=31536&bid=10052603" /></a>
        </div>
        

        <div class="all-tags">
            <div class="blog-sb-title clearfix">
                <h3>
                    Tin tức nổi bật 
                </h3>
            </div>
            @foreach ($tintuckhac as $items)
                <article class="blog-item blog-item-list">
                    <div class="blog-item-thumbnail img1">
                      <a href="{{ url('tin-tuc/'.$items->Slug) }}">
                        <picture>
                          <img src="{{ asset('uploads/tintuc/'.$items->Image) }}" style="max-width:100%;" class="img-responsive" alt="{!! $items->TieuDe !!}">
                        </picture>
                      </a>
                    </div>
                    <div class="ct_list_item">
                      <div class="blog-item-name"><a href="{{ url('tin-tuc/'.$items->Slug) }}">{!! $items->TieuDe !!}</a></div>
                      <span class="time_post"><i class="fas fa-calendar-alt"></i>&nbsp; {!! $items->created_at !!}</span>
                    </div>
                </article>
            @endforeach
        </div>
        <div class="blog-sb-banner">
            <a href="http://member.civi.vn/cpc/?sid=31536&bid=10049704" target="_blank"><img  src="http://member.civi.vn/imp/b/?sid=31536&bid=10049704" /></a>
        </div>
        
        {!! $video->LinkFb !!}
    </div>
</div>