{{-- @foreach ($banner as $items)
    
@endforeach --}}
<div id="hero-slider">
    @foreach ($banner as $items)
        <div class="hero-slide" style="background-image: url({{ asset('uploads/images/banner/'.$items->Image) }}); height: 500px;">
            <div class="hero-slide-overlay">
            </div>
            <div class="hero-slide-content">
                <div class="wrapper">
                    <div class="hero-content text-center">
                        <div class="slide-message" data-animation="fadeInDown" data-delay="0.5s">
                            <h4>    
                                Chuẩn mực về chất lượng sản phẩm
                            </h4>
                            <p class="medium--hide small--hide">
                                Bằng tất cả tâm huyết, năng lực vượt trội và quy mô không ngừng phát triển, Suplo cam kết nỗ lực hết mình nhằm cung cấp sản phẩm và dịch vụ đúng với những giá trị mà khách hàng mong đợi.
                            </p>
                        </div>
                        {{-- <div class="slide-button">
                            <a href="index.html" data-animation="fadeInUp" data-delay="1s">Xem thêm</a>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    
</div>
<script>
    $(document).ready(function() {
        $('#hero-slider').on('init', function(e, slick) {
            var $firstAnimatingElements = $('div.hero-slide:first-child').find('[data-animation]');
            doAnimations($firstAnimatingElements);
        });
        $('#hero-slider').on('beforeChange', function(e, slick, currentSlide, nextSlide) {
            var $animatingElements = $('div.hero-slide[data-slick-index="' + nextSlide + '"]').find('[data-animation]');
            doAnimations($animatingElements);
        });
        $('#hero-slider').slick({
            autoplay: true,
            autoplaySpeed: 10000,
            dots: true,
            fade: true
        });
    
        function doAnimations(elements) {
            var animationEndEvents = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            elements.each(function() {
                var $this = $(this);
                var $animationDelay = $this.data('delay');
                var $animationType = 'animated ' + $this.data('animation');
                $this.css({
                    'animation-delay': $animationDelay,
                    '-webkit-animation-delay': $animationDelay
                });
                $this.addClass($animationType).one(animationEndEvents, function() {
                    $this.removeClass($animationType);
                });
            });
        }
    });
</script>