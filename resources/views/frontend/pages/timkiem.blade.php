@extends('frontend.layout.master')

@section('title','Tìm kiếm - Nhà thuốc chính hãng')
@section('keywords',$cauhinh->TuKhoa)
@section('description',$cauhinh->MoTa)
@section('url',url('/tin-tuc.html'))
@section('titleseo',$cauhinh->TuKhoa)
@section('type','news')
@section('descriptionseo',$cauhinh->TuKhoa)
@section('image')

@section('content')
    <section id="breadcrumb-wrapper">
        <div class="breadcrumb-overlay"></div>
        <div class="breadcrumb-content">
            <div class="wrapper">
                <div class="inner text-center">
                    <div class="breadcrumb-big">
                        <h2>
                            Tìm kiếm
                        </h2>
                    </div>
                    <div class="breadcrumb-small">
                        <a href="{{ url('/') }}" title="Quay trở về trang chủ">Trang chủ</a>
                        <span aria-hidden="true">/</span>
                        <span>Tìm kiếm</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            <section id="other-sections-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        
                        <div  id="collection-wrapper" class="grid">
                            <div class="grid__item collection-content-wrapper">
                                <h1 class="search-h1 text-center">Kết quả tìm kiếm:<br/><span class="check_query_text">Nhóm sản phẩm: {{ count($sanPham) }} / </span> Từ khóa: {{ $search }}</h1>
                                <div style="max-width:500px;margin: 0px auto;">
                                    <div class="search-form-wrapper1">
                                        <form action="{{ url('tim-kiem.html') }}" method="get" role="search" class="searchform-categoris ultimate-search">
                                            <div class="wpo-search">
                                                <div class="wpo-search-inner">
                                                    <div class="input-group">
                                                        <input type="hidden" name="type" value="product" />
                                                        <input id="searchtext1" name="search" id="s" maxlength="40" class="form-control input-search" type="text" size="20" placeholder="Tìm kiếm sản phẩm...">
                                                        <span class="input-group-btn">
                                                        <button type="submit" id="searchsubmit1"><i class="fa fa-search" aria-hidden="true"></i></button>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    
                                </div>
                                <hr class="hr--clear">
                                @if(Count($sanPham) >0 )
                                <div class="grid-uniform mg-left-15 product-loop">
                                    @foreach ($sanPham as $items)
                                    <div class="grid__item product--loop product--grid-item large--one-fifth medium--one-third small--one-half pd-left15 search-item">
                                        <div class="product-item">
                                            <div class="product-img">
                                                <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">
                                                <img id="1015092994" 
                                                    src="{{ asset('uploads/sanpham/'.$items->Image) }}"
                                                    alt="{{ $items->Pro_Name }}" />
                                                </a>
                                                <div class="product-actions text-center clearfix">
                                                    <div>
                                                        
                                                        <button type="button" class="btnBuyNow medium--hide small--hide">
                                                            <span><a href="{{ url('mua-nhanh/'.$items->Pro_id) }}" style="color: #fff"> Mua ngay</span>
                                                        </button>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-item-info text-center">
                                                <div class="product-title">
                                                    <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">{{ $items->Pro_Name }}</a>
                                                </div>
                                                <div class="product-price clearfix">
                                                    <span class="current-price">{{ number_format($items->Gia,0,',','.') }}₫</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    
                                </div>
                                @else
                                <div> <p class="search-h1 text-center">Có thể bạn quan tâm</p></div>
                                <div class="grid-uniform mg-left-15 product-loop">

                                    @foreach ($viewsp as $items)
                                    <div class="grid__item product--loop product--grid-item large--one-fifth medium--one-third small--one-half pd-left15 search-item">
                                        <div class="product-item">
                                            <div class="product-img">
                                                <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">
                                                <img id="1015092994" 
                                                    src="{{ asset('uploads/sanpham/'.$items->Image) }}"
                                                    alt="{{ $items->Pro_Name }}" />
                                                </a>
                                                <div class="product-actions text-center clearfix">
                                                    <div>
                                                        
                                                        <button type="button" class="btnBuyNow medium--hide small--hide">
                                                            <span><a href="{{ url('mua-nhanh/'.$items->Pro_id) }}" style="color: #fff"> Mua ngay</span>
                                                        </button>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-item-info text-center">
                                                <div class="product-title">
                                                    <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">{{ $items->Pro_Name }}</a>
                                                </div>
                                                <div class="product-price clearfix">
                                                    <span class="current-price">{{ number_format($items->Gia,0,',','.') }}₫</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    
                                </div>
                                @endif
                                <div class="pagination not-filter">
                                    {!! $sanPham->links() !!}    
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </main>
    </div>
@endsection