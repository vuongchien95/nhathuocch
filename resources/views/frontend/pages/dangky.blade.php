@extends('frontend.layout.master')

@section('title','Đăng ký - Nhà Thuốc Chính Hãng')
@section('keywords',$cauhinh->TuKhoa)
@section('description',$cauhinh->MoTa)
@section('url',url('/gioi-thieu.html'))
@section('titleseo',$cauhinh->TuKhoa)
@section('type','giới thiệu')
@section('descriptionseo',$cauhinh->TuKhoa)
@section('image')

@section('content')

<section id="breadcrumb-wrapper">
    <div class="breadcrumb-overlay"></div>
    <div class="breadcrumb-content">
        <div class="wrapper">
            <div class="inner text-center">
                <div class="breadcrumb-big">
                    <h2>
                        Tạo tài khoản
                    </h2>
                </div>
                <div class="breadcrumb-small">
                    <a href="{{ url('/') }}" title="Quay trở về trang chủ">Trang chủ</a>
                    <span aria-hidden="true">/</span>
                    <span>Tạo tài khoản</span>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="PageContainer" class="is-moved-by-drawer">
    <main class="main-content" role="main">
        <section id="page-wrapper">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">
                        <div class="grid__item large--one-third push--large--one-third text-center">
                            <h1>Tạo tài khoản</h1>
                            <div class="form-vertical">
                                <div class="row">
                                    <div class="col-md-4"> </div>
                                    @if(count($errors) > 0)
                                    <div class="col-md-4">
                                      <ul style="list-style: none">
                                        @foreach($errors->all() as $err)
                                        <li style="color: red"><i class="ace-icon fa fa-exclamation-triangle"></i> {{ $err }} </li>
                                        @endforeach
                                      </ul>
                                    </div>
                                    @endif
                                    <div class="col-md-4"> </div>
                                </div>
                                <form accept-charset='UTF-8' action='{{ url('dang-ky.html') }}' id='create_customer' method='post'>
                                    {{ csrf_field() }}
                                    <input name='form_type' type='hidden' value='create_customer'>
                                    <input name='utf8' type='hidden' value='✓'>

                                    <label for="FirstName" class="hidden-label">Họ & Tên</label>
                                    <input type="text" class="input-full" placeholder="Họ &amp; tên" value="{{ old('name') }}" name="name" id="name" required="">

                                    <label for="LastName" class="hidden-label">Số điện thoại</label>
                                    <input type="text" class="input-full" pattern="^[0-9]*$" placeholder="Số điện thoại" value="{{ old('phone') }}" name="phone" id="phone" required="">

                                    <label for="Email" class="hidden-label">Email</label>
                                    <input type="email" class="input-full" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Email" value="{{ old('email') }}" name="email" id="email" required="">

                                    <label for="CreatePassword" class="hidden-label">Mật khẩu</label>
                                    <input type="password" class="input-full" placeholder="Mật khẩu" value="" name="password" required="">

                                    <select class="input-full" name="tinhthanh" id="tinhThanhChange" required>
                                        <option value=''>--- Chọn tỉnh thành ---</option>
                                        @foreach ($tinhThanh as $items)
                                          <option  value="{{ $items->id }}">{{ $items->Name }}</option>
                                        @endforeach
                                    </select>

                                    <select class="input-full" name="quanhuyen" id="quanHuyenChange" required>
                                        <option value="">--- Chọn quận huyện ---</option>
                                    </select>
                                    <select class="input-full" name="xaphuong" id="xaPhuongChange" required>
                                        <option value="">--- Chọn xã phường ---</option>
                                    </select>

                                    <input name="address" type="text" value="{{ old('address') }}" class="input-full" id="" placeholder="Địa chỉ" required />

                                    <p>
                                        <input type="submit" value="Đăng ký" class="btn btn--full">
                                    </p>
                                    <a href="{{ url('/') }}">Trở về</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>
@endsection