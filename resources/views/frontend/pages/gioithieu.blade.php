@extends('frontend.layout.master')

@section('title','Thông tin - Nhà Thuốc Chính Hãng')
@section('keywords',$cauhinh->TuKhoa)
@section('description',$cauhinh->MoTa)
@section('url',url('/gioi-thieu.html'))
@section('titleseo',$cauhinh->TuKhoa)
@section('type','giới thiệu')
@section('descriptionseo',$cauhinh->TuKhoa)
@section('image')

@section('content')
    <section id="breadcrumb-wrapper">
        <div class="breadcrumb-overlay"></div>
        <div class="breadcrumb-content">
            <div class="wrapper">
                <div class="inner text-center">
                    <div class="breadcrumb-big">
                        <h2>
                            Giới thiệu
                        </h2>
                    </div>
                    <div class="breadcrumb-small">
                        <a href="{{ url('/') }}" title="Quay trở về trang chủ">Trang chủ</a>
                        <span aria-hidden="true">/</span>
                        <span>Giới thiệu</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            <section id="page-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        <div class="grid">
                            <div class="grid__item large--one-whole">
                                <h1>Giới thiệu</h1>
                                <div class="rte">
                                    {!! $gioithieu->GioiThieu !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
@endsection