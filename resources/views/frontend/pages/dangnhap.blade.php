@extends('frontend.layout.master')

@section('title','Đăng nhập - Nhà thuốc chính hãng')
@section('keywords',$cauhinh->TuKhoa)
@section('description',$cauhinh->MoTa)
@section('url',url('/tin-tuc.html'))
@section('titleseo',$cauhinh->TuKhoa)
@section('type','news')
@section('descriptionseo',$cauhinh->TuKhoa)

@section('content')






{{--   <div class="row">
    <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 col-lg-offset-4 col-md-offset-3 col-sm-offset-3 ">
      <div class="page-login">
        <div id="login">
          <form accept-charset="UTF-8" action="{{ url('post-dang-nhap.html') }}" id="" method="post">
            {{ csrf_field() }}
            <div class="form-signup clearfix">
              <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                  <fieldset class="form-group">
                    <input type="text" class="form-control form-control-lg"  value="{{ old('name') }}" name="name" id="name" placeholder="Nhập tên tài khoản" required="">
                  </fieldset>
                  <fieldset class="form-group">
                    <input type="password" class="form-control form-control-lg" value="" name="password" id="password" placeholder="Mật khẩu" required="">
                  </fieldset>
                  @if (!Auth::check())
                    <span class="recv-text"><a id="rcv-pass">Quên mật khẩu?</a></span>
                  @endif
                </div>
              </div>
              <div class="col-xs-12 text-xs-left margin-bottom-15" style="margin-top:20px; padding: 0">
                <button type="submit" value="Đăng nhập" class="btn button-50 btn-50-blue width_100">Đăng nhập</button>
              </div>
              <div class="fot_sigup hidden">
                <span class="tit margin-top-15"><span>Hoặc đăng nhập</span></span>
              </div>
              <div class="social_login"></div>
              <span class="have_ac">Chưa có tài khoản đăng ký <a href="{{ url('dang-ky.html') }}">tại đây</a></span>
            </div>
          </form>
        </div>
        <div id="recover-password" class="rcv">
          <div class="form-signup clearfix" style="margin-top:0px;">
            <div class="recover">
              <form accept-charset="UTF-8" action="{{ route('password.email') }}" id="recover_customer_password" method="post">
                @csrf
                <div class="form-signup aaaaaaaa erorr_page a-center" style="margin-top:0px;"></div>
                <div class="form_recover_" style="display:none;">
                  <div class="form-signup clearfix">
                    <fieldset class="form-group">
                      <input type="email" class="form-control form-control-lg" value="" name="email" id="email" placeholder="Nhập Email để lấy lại mật khẩu" required="">
                    </fieldset>
                  </div>
                  <div class="action_bottom">
                    <button type="summit" value="Lấy lại mật khẩu" class="btn button-50 width_100 btn-50-blue" style="margin-top:20px;">Lấy lại mật khẩu</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> --}}

<section id="breadcrumb-wrapper">
    <div class="breadcrumb-overlay"></div>
    <div class="breadcrumb-content">
        <div class="wrapper">
            <div class="inner text-center">
                <div class="breadcrumb-big">
                    <h2>
                        Tài khoản
                    </h2>
                </div>
                <div class="breadcrumb-small">
                    <a href="{{ url('/') }}" title="Quay trở về trang chủ">Trang chủ</a>
                    <span aria-hidden="true">/</span>
                    <span>Tài khoản</span>
                </div>
            </div>
        </div>
    </div>
</section>
<div id="PageContainer" class="is-moved-by-drawer">
    <main class="main-content" role="main">
        <section id="page-wrapper">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">
                        <div class="grid__item large--one-third push--large--one-third text-center">
                            <div class="note form-success" id="ResetSuccess" style="display:none;">
                                Chúng tôi đã gửi một email với đường dẫn để giúp bạn cập nhật mật khẩu.
                            </div>
                           
                            <div id="CustomerLoginForm" class="form-vertical">
                                <form accept-charset='UTF-8' action='{{ url('post-dang-nhap.html') }}' id='customer_login' method='post'>
                                    {{ csrf_field() }}
                                    <input name='form_type' type='hidden' value='customer_login'>
                                    <input name='utf8' type='hidden' value='✓'>
                                    <h1>Đăng nhập</h1>
                                    <div class="row">
                                        <div class="col-md-4"> </div>
                                        @if (Session::has('erro'))
                                        <div class="alert alert-danger col-md-4 text-center" style="margin-top: 1em;border-radius: 5px">
                                          <ul style="list-style: none">
                                            <li style="color: red"><i class="ace-icon fa fa-exclamation-triangle"></i> {{Session::get('erro')}} </li>
                                          </ul>
                                        </div>
                                        @endif
                                        {{-- @if (session('status'))
                                        <div class="alert alert-success col-md-4 text-center" style="margin-top: 1em;border-radius: 5px">
                                          <ul style="list-style: none">
                                            <li> {{ session('status') }} </li>
                                          </ul>
                                        </div>
                                        @endif --}}
                                        <div class="col-md-4"> </div>
                                    </div>


                                    <label for="CustomerEmail" class="hidden-label">Email</label>
                                    <input type="email" value="{{ old('name') }}" name="name" id="CustomerEmail" class="input-full" placeholder="Email" autocorrect="off" autocapitalize="off" autofocus>
                                    <label for="CustomerPassword" class="hidden-label">Mật khẩu</label>
                                    <input type="password" value="" name="password" id="CustomerPassword" class="input-full" placeholder="Mật khẩu">
                                    <p>
                                        <input type="submit" class="btn btn--full" value="Đăng nhập">
                                    </p>
                                    <p><a href="{{ url('/') }}">Trở về</a></p>
                                    <p><a href="{{ url('dang-ky.html') }}" id="customer_register_link">Đăng kí</a></p>
                                    <p><a href="#recover" id="RecoverPassword">Quên mật khẩu?</a></p>
                                </form>
                            </div>
                            <div id="RecoverPasswordForm" style="display: none;">
                                <h2>Cài đặt lại mật khẩu</h2>
                                <p>Mật khẩu mới sẽ được gửi về email của bạn.</p>
                                <div class="form-vertical">
                                    <form accept-charset='UTF-8' action='/account/recover' method='post'>
                                        <input name='form_type' type='hidden' value='recover_customer_password'>
                                        <input name='utf8' type='hidden' value='✓'>
                                        <label for="RecoverEmail" class="hidden-label">Email</label>
                                        <input type="email" value="" name="email" id="RecoverEmail" class="input-full" placeholder="Email" autocorrect="off" autocapitalize="off">
                                        <p>
                                            <input type="submit" class="btn btn--full" value="Gửi">
                                        </p>
                                        <button type="button" id="HideRecoverPasswordLink" class="text-link">Bỏ qua</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
</div>
@endsection