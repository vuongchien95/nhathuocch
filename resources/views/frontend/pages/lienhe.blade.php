@extends('frontend.layout.master')

@section('title','Liên hệ - Nhà Thuốc Chính Hãng')
@section('keywords',$cauhinh->TuKhoa)
@section('description',$cauhinh->MoTa)
@section('url',url('/gioi-thieu.html'))
@section('titleseo',$cauhinh->TuKhoa)
@section('type','giới thiệu')
@section('descriptionseo',$cauhinh->TuKhoa)
@section('image')

@section('content')
    <section id="breadcrumb-wrapper">
        <div class="breadcrumb-overlay"></div>
        <div class="breadcrumb-content">
            <div class="wrapper">
                <div class="inner text-center">
                    <div class="breadcrumb-big">
                        <h2>
                            Liên hệ
                        </h2>
                    </div>
                    <div class="breadcrumb-small">
                        <a href="{{ url('/') }}" title="Quay trở về trang chủ">Trang chủ</a>
                        <span aria-hidden="true">/</span>
                        <span>Liên hệ</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            <div id="page-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        <div class="grid">
                            <div class="grid__item large--one-whole">
                                <div class="page-contact-wrapper">
                                    <div class="page-head">
                                        <h1>Liên hệ</h1>
                                    </div>
                                    <div class="page-body">
                                        <div class="page-body-inner">
                                            <div class="grid">
                                                <div class="grid__item large--one-half medium--one-half small--one-whole">
                                                    <div class="contact-wrapper">
                                                        <div class="contact-title">
                                                            <h4>
                                                                Địa chỉ nhà thuốc:
                                                            </h4>
                                                        </div>
                                                        <div class="contact-info">
                                                            {{ $lienhe->DiaChi }}
                                                        </div>
                                                        <div class="contact-map">
                                                            {{-- <script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>
                                                            <div style='overflow:hidden;height:260px;width:100%;'>
                                                                <div id='gmap_canvas' style='height:260px;width:100%;'></div>
                                                                <div><small><a href="http://embedgooglemaps.com">Click here to generate your map!</a></small></div>
                                                                <div><small><a href="https://ilovebargain.ph/takatack.com-vouchers/">takatack ph voucher</a></small></div>
                                                                <style>
                                                                    #gmap_canvas img {
                                                                    max-width: none!important;
                                                                    background: none!important
                                                                    }
                                                                </style>
                                                            </div>
                                                            <script type='text/javascript'>
                                                                function init_map() {
                                                                var myOptions = {
                                                                zoom: 15,
                                                                center: new google.maps.LatLng(21.0209408, 105.82477300000005),
                                                                mapTypeId: google.maps.MapTypeId.ROADMAP
                                                                };
                                                                map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
                                                                marker = new google.maps.Marker({
                                                                map: map,
                                                                position: new google.maps.LatLng(21.0209408, 105.82477300000005)
                                                                });
                                                                infowindow = new google.maps.InfoWindow({
                                                                content: '<strong>Suplo</strong><br>29 Trần Đăng Ninh<br>'
                                                                });
                                                                google.maps.event.addListener(marker, 'click', function() {
                                                                infowindow.open(map, marker);
                                                                });
                                                                infowindow.open(map, marker);
                                                                }
                                                                google.maps.event.addDomListener(window, 'load', init_map);
                                                            </script> --}}
                                                            {!! $lienhe->Map !!}
                                                        </div>
                                                        <div class="contact-title">
                                                            <h4>
                                                                Số điện thoại:
                                                            </h4>
                                                        </div>
                                                        <div class="contact-info">
                                                            <a href="tel:{{ $lienhe->Sdt }}">{{ $lienhe->Sdt }}</a>
                                                        </div>
                                                        <div class="contact-title">
                                                            <h4>
                                                                Email:
                                                            </h4>
                                                        </div>
                                                        <div class="contact-info">
                                                            <a href="mailto:{{ $lienhe->Email }}">{{ $lienhe->Email }}</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="grid__item large--one-half medium--one-half small--one-whole">
                                                    <div class="form-vertical clearfix">
                                                        <form accept-charset='UTF-8' action="{{ url('send-mail') }}" class='contact-form' method='post'>
                                                            {{csrf_field()}}
                                                            
                                                            <input name='form_type' type='hidden' value='contact'>
                                                            <input name='utf8' type='hidden' value='✓'>
                                                            <label for="ContactFormName" class="hidden-label">Họ tên của bạn</label>
                                                            <input type="text" id="ContactFormName" class="input-full" name="hoten" placeholder="Họ tên của bạn" autocapitalize="words" value="" required="">
                                                            <label for="ContactFormEmail" class="hidden-label">Địa chỉ email của bạn</label>
                                                            <input type="email" id="ContactFormEmail" class="input-full" name="email" placeholder="Địa chỉ email của bạn" autocorrect="off" autocapitalize="off" value="" required="">
                                                            <label for="ContactFormPhone" class="hidden-label">Số điện thoại của bạn</label>
                                                            <input type="tel" id="ContactFormPhone" class="input-full" name="phone" placeholder="Số điện thoại của bạn" pattern="[0-9\-]*" value="" required="">
                                                            <label for="ContactFormMessage" class="hidden-label">Nội dung</label>
                                                            <textarea rows="10" id="ContactFormMessage" class="input-full" name="contnet" placeholder="Nội dung" required=""></textarea>
                                                            <input type="submit" class="btn right btnContactSubmit" value="Gửi">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
@endsection