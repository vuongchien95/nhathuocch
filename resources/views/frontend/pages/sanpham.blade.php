@extends('frontend.layout.master')

@section('title',$danhmuc->Name)
@section('keywords',$cauhinh->TuKhoa)
@section('description',$cauhinh->MoTa)
@section('url',url('/tin-tuc.html'))
@section('titleseo',$cauhinh->TuKhoa)
@section('type','news')
@section('descriptionseo',$cauhinh->TuKhoa)
@section('image')

@section('content')
    <section id="breadcrumb-wrapper">
        <div class="breadcrumb-overlay"></div>
        <div class="breadcrumb-content">
            <div class="wrapper">
                <div class="inner text-center">
                    <div class="breadcrumb-big">
                        <h2>
                            {!! $danhmuc->Name !!}
                        </h2>
                    </div>
                    <div class="breadcrumb-small">
                        <a href="/" title="Quay trở về trang chủ">Trang chủ</a>
                        <span aria-hidden="true">/</span>
                        <span>{!! $danhmuc->Name !!}</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            <section id="collection-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        <div class="grid">
                            <div class="grid__item large--three-quarters medium--one-whole small--one-whole float-right">
                                <div class="collection-content-wrapper">
                                    <div class="collection-head">
                                        <div class="grid">
                                            <div class="grid__item large--seven-twelfths medium--one-whole small--one-whole">
                                                <div class="collection-title">
                                                    <h1>{!! $danhmuc->Name !!}</h1>{{ Count($theloaisanpham)}} sản phẩm/trang
                                                </div>
                                            </div>
                                            <div class="grid__item large--five-twelfths medium--one-whole small--one-whole">
                                                <div class="collection-sorting-wrapper">
                                                    <!-- /snippets/collection-sorting.liquid -->
                                                    <div class="form-horizontal text-right">
                                                        <label for="SortBy">Sắp xếp</label>
                                                        <select name="SortBy" id="SortBy">
                                                            <option value="manual">Mặc định</option>
                                                            {{-- <option value="best-selling">Sản phẩm bán chạy</option>
                                                            <option value="title-ascending">Theo bảng chữ cái từ A-Z</option>
                                                            <option value="title-descending">Theo bảng chữ cái từ Z-A</option>
                                                            <option value="price-ascending">Giá từ thấp tới cao</option>
                                                            <option value="price-descending">Giá từ cao tới thấp</option>
                                                            <option value="created-descending">Mới nhất</option>
                                                            <option value="created-ascending">Cũ nhất</option> --}}
                                                        </select>
                                                    </div>
                                                    {{-- <script>
                                                        /*============================================================================
                                                            Inline JS because collection liquid object is only available
                                                            on collection pages and not external JS files
                                                          ==============================================================================*/
                                                        Haravan.queryParams = {};
                                                        if (location.search.length) {
                                                            for (var aKeyValue, i = 0, aCouples = location.search.substr(1).split('&'); i < aCouples.length; i++) {
                                                                aKeyValue = aCouples[i].split('=');
                                                                if (aKeyValue.length > 1) {
                                                                    Haravan.queryParams[decodeURIComponent(aKeyValue[0])] = decodeURIComponent(aKeyValue[1]);
                                                                }
                                                            }
                                                        }
                                                        
                                                        $(function() {
                                                            $('#SortBy')
                                                            .val('created-descending')
                                                            .bind('change', function() {
                                                                Haravan.queryParams.sort_by = jQuery(this).val();
                                                                location.search = jQuery.param(Haravan.queryParams);
                                                            }
                                                                     );
                                                        });
                                                    </script> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="collection-body">
                                        @if (count($theloaisanpham) > 0)

                                            <div class="grid-uniform product-list mg-left-15">
                                                @foreach ($theloaisanpham as $pro)
                                                    <div class="grid__item large--one-quarter medium--one-third small--one-half pd-left15">
                                                    <div class="product-item">
                                                        <div class="product-img">
                                                            <a href="{{ url('san-pham/'.$pro->Pro_id.'-'.$pro->Slug) }}.html">
                                                            <img id="1015092994" 
                                                                src="{{ asset('uploads/sanpham/'.$pro->Image) }}"
                                                                alt="{{ $pro->Name }}" />
                                                            </a>
                                                            <div class="product-actions text-center clearfix">
                                                                <div>
                                                                    <button type="button" class="btnBuyNow medium--hide small--hide">
                                                                        <span><a href="{{ url('mua-nhanh/'.$pro->Pro_id) }}" style="color: #fff"> Mua ngay</span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="product-item-info text-center">
                                                            <div class="product-title">
                                                                <a href="{{ url('san-pham/'.$pro->Slug) }}.html">{{ $pro->Pro_Name }}</a>
                                                            </div>
                                                            <div class="product-price clearfix">
                                                                <span class="current-price">{{ number_format($pro->Gia,0,',','.') }}₫</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @else
                                        <div>
                                            <h4>Sản phẩm đang được cập nhật. Vui lòng quay lại sau!</h4>
                                        </div>

                                        @endif

                                        
                                        <div class="pagination not-filter">
                                            {!! $theloaisanpham->links() !!}    
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="grid__item large--one-quarter medium--one-whole small--one-whole">
                                <div class="collection-sidebar-wrapper">
                                    <div class="grid">
                                        <div class="grid__item large--one-whole medium--one-half small--one-whole">
                                            <div class="collection-categories">
                                                <button class="accordion cs-title col-sb-trigger">
                                                <span>Danh mục</span>
                                                </button>
                                                <div class="panel">
                                                    @php
                                                        $menu = DB::table('danhmuc')->where('DanhMuc_Cha', 0)->get();
                                                    @endphp
                                                    @foreach ($menu as $items)
                                                        <ul class="no-bullets">
                                                            <li>
                                                                <a href="{{ url($items->id.'-'.$items->Slug) }}.html">{{ $items->Name }}</a>
                                                                @php
                                                                    $subMenu = DB::table('danhmuc')->where('DanhMuc_Cha',$items->id )->get();
                                                                @endphp
                                                                @if( count($subMenu) >0 )
                                                                <ul class="no-bullets">
                                                                    @foreach ($subMenu as $subm)
                                                                        <li>
                                                                            <a href="{{ url($subm->id.'-'.$subm->Slug) }}.html">- {{ $subm->Name }}</a>
                                                                        </li>
                                                                    @endforeach
                                                                   
                                                                </ul>
                                                                @endif
                                                            </li>
                                                        </ul>
                                                    @endforeach
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
@endsection
@section('plugins-js')
    <script>
            jQuery(document).ready(function() {
                var acc = document.getElementsByClassName("accordion");
                var i;
            
                for (i = 0; i < acc.length; i++) {
                    acc[i].onclick = function() {
                        this.classList.toggle("active");
                        var panel = this.nextElementSibling;
                        if (panel.style.maxHeight) {
                            panel.style.maxHeight = null;
                        } else {
                            panel.style.maxHeight = panel.scrollHeight + "px";
                        }
                    }
                }
            
                if ($(window).width() > 769) {
                    $('.accordion.col-sb-trigger').trigger('click');
                }
            });
        </script>
@endsection
