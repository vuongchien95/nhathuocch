@extends('frontend.layout.master')

@if (isset($cauhinh))
@section('title',$cauhinh->TieuDe)
@section('keywords',$cauhinh->TuKhoa)
@section('description',$cauhinh->MoTa)
@section('url',url(''))
@section('titleseo',$cauhinh->MoTa)
@section('type','product')
@section('descriptionseo')
@section('image')
@endif

@section('content')

    @include('frontend.layout.banner')

    <style type="text/css" media="screen">
        .link-buy{color: #fff;}
    </style>

    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            <div id="index-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        <div class="grid">
                            <div class="grid__item large--nine-twelfths medium--one-whole small--one-whole float-right">
                                <div id="home-policy">
                                    <div class="grid">
                                        <div class="grid__item wow fadeInUp large--one-half medium--one-half small--one-whole" data-wow-delay="0.2s" data-wow-duration="0.75s">
                                            <div class="hservice-item text-center">
                                                <div class="hservice-img">
                                                    <img src="{{ asset('img/hservice_icon1.png') }}" alt="Bảo quản thuốc đúng chuẩn">
                                                </div>
                                                <div class="hservice-title">
                                                    <a href="javascript:;">Bảo quản thuốc đúng chuẩn</a>
                                                </div>
                                                <div class="hservice-desc">
                                                    Nhà thuốc chính hãng cam kết nỗ lực hết mình nhằm cung cấp sản phẩm và dịch vụ đúng với những giá trị mà khách hàng mong đợi.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="grid__item wow fadeInUp large--one-half medium--one-half small--one-whole" data-wow-delay="0.4s" data-wow-duration="0.75s">
                                            <div class="hservice-item text-center">
                                                <div class="hservice-img">
                                                    <img src="{{ asset('img/giao-hang.png') }}" height="60px" width="60px" alt="Giao hàng tận nơi">
                                                </div>
                                                <div class="hservice-title">
                                                    <a href="javascript:;">Giao hàng tận nơi</a>
                                                </div>
                                                <div class="hservice-desc">
                                                    Nhà thuốc chính hãng cam kết nỗ lực hết mình nhằm cung cấp sản phẩm và dịch vụ đúng với những giá trị mà khách hàng mong đợi.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <section id="home-collection-tabs">
                                    <div class="section-title wow fadeInDown text-center">
                                        <h2>
                                            Sản phẩm nổi bật
                                        </h2>
                                        <span class="section-title-border"></span>
                                    </div>
                                    <div class="hmp-tab-wrapper1">
                                        <div class="tab text-center clearfix">
                                            @php
                                                $menu = DB::table('danhmuc')->where('DanhMuc_Cha', 0)->limit(4)->get();
                                            @endphp
                                            @php
                                                $i = 0;
                                            @endphp
                                            @foreach ($menu as $items)
                                            @php
                                                $i++;
                                              @endphp
                                                <button class="hmp-tablinks1 wow fadeInLeft" onclick="openHmpTab1(event, 'hmptab1_{{ $i }}')" id="defaultOpenHmpTab1" data-wow-delay="0.2s" data-wow-duration="0.75s">{{ $items->Name }}</button>
                                            @endforeach
                                            
                                        </div>
                                        <div id="hmptab1_1" class="hmp-tabcontent1">
                                            <div class="grid-uniform mg-left-15">
                                                @if (count($mypham) > 0)
                                                @foreach ($mypham as $items)
                                                    <div class="grid__item pd-left15 large--one-quarter medium--one-third small--one-half">
                                                        <form action="{{ url('mua-nhanh/'.$items->Pro_id) }}" enctype="multipart/form-data" id="" method="get" class="form-inline">
                                                        <div class="product-item">
                                                            <div class="product-img">
                                                                <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">
                                                                <img id="1014835198" class="lazyload" src="{{ asset('uploads/sanpham/'.$items->Image) }}" data-src="{{ asset('uploads/sanpham/'.$items->Image) }}" alt="{!! $items->Pro_Name !!}">
                                                                </a>
                                                                
                                                                <div class="product-actions text-center clearfix">
                                                                    <div>
                                                                        
                                                                        <button type="submit" class="btnBuyNow medium--hide small--hide">
                                                                            <span>Mua ngay</span>
                                                                        </button>
                                                                    
                                                                        
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="product-item-info text-center">
                                                                <div class="product-title">
                                                                    <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">{!! $items->Pro_Name !!}</a>
                                                                </div>
                                                                <div class="product-price clearfix">
                                                                    <span class="current-price">{!! number_format($items->Gia,0,',','.') !!}₫</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </form>
                                                    </div>
                                                @endforeach
                                                @else
                                                    <div class="col-lg-12 col-md-12 block_width"><span class="warning">Đang cập nhật sản phẩm</span></div>
                                                @endif
                                                
                                                <div class="grid__item pd-left15 large--one-whole text-center">
                                                    <a href="{{ url($menu[0]->id.'-'.$menu[0]->Slug) }}.html" class="hmp-viewmore">Xem tất cả {{ $menu[0]->Name }}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="hmptab1_2" class="hmp-tabcontent1">
                                            <div class="grid-uniform mg-left-15">
                                                @if (count($tanduoc) > 0)
                                                @foreach ($tanduoc as $items)
                                                    <div class="grid__item pd-left15 large--one-quarter medium--one-third small--one-half">
                                                        <form action="{{ url('mua-nhanh/'.$items->Pro_id) }}" enctype="multipart/form-data" id="" method="get" class="form-inline">
                                                        <div class="product-item">
                                                            <div class="product-img">
                                                                <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">
                                                                <img id="1014835198" class="lazyload" src="{{ asset('uploads/sanpham/'.$items->Image) }}" data-src="{{ asset('uploads/sanpham/'.$items->Image) }}" alt="{!! $items->Pro_Name !!}">
                                                                </a>
                                                                
                                                                <div class="product-actions text-center clearfix">
                                                                    <div>
                                                                        
                                                                        <button type="submit" class="btnBuyNow medium--hide small--hide">
                                                                            <span>Mua ngay</span>
                                                                        </button>
                                                                    
                                                                        
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="product-item-info text-center">
                                                                <div class="product-title">
                                                                    <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">{!! $items->Pro_Name !!}</a>
                                                                </div>
                                                                <div class="product-price clearfix">
                                                                    <span class="current-price">{!! number_format($items->Gia,0,',','.') !!}₫</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </form>
                                                    </div>
                                                @endforeach
                                                @else
                                                    <div class="col-lg-12 col-md-12 block_width"><span class="warning">Đang cập nhật sản phẩm</span></div>
                                                @endif
                                                
                                                <div class="grid__item pd-left15 large--one-whole text-center">
                                                    <a href="{{ url($menu[1]->id.'-'.$menu[1]->Slug) }}.html" class="hmp-viewmore">Xem tất cả {{ $menu[1]->Name }}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="hmptab1_3" class="hmp-tabcontent1">
                                            <div class="grid-uniform mg-left-15">
                                                @if (count($thaoduoc) > 0)
                                                @foreach ($thaoduoc as $items)
                                                    <div class="grid__item pd-left15 large--one-quarter medium--one-third small--one-half">
                                                        <form action="{{ url('mua-nhanh/'.$items->Pro_id) }}" enctype="multipart/form-data" id="" method="get" class="form-inline">
                                                        <div class="product-item">
                                                            <div class="product-img">
                                                                <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">
                                                                <img id="1014835198" class="lazyload" src="{{ asset('uploads/sanpham/'.$items->Image) }}" data-src="{{ asset('uploads/sanpham/'.$items->Image) }}" alt="{!! $items->Pro_Name !!}">
                                                                </a>
                                                                
                                                                <div class="product-actions text-center clearfix">
                                                                    <div>
                                                                        
                                                                        <button type="submit" class="btnBuyNow medium--hide small--hide">
                                                                            <span>Mua ngay</span>
                                                                        </button>
                                                                    
                                                                        
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="product-item-info text-center">
                                                                <div class="product-title">
                                                                    <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">{!! $items->Pro_Name !!}</a>
                                                                </div>
                                                                <div class="product-price clearfix">
                                                                    <span class="current-price">{!! number_format($items->Gia,0,',','.') !!}₫</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </form>
                                                    </div>
                                                @endforeach
                                                @else
                                                    <div class="col-lg-12 col-md-12 block_width"><span class="warning">Đang cập nhật sản phẩm</span></div>
                                                @endif
                                                
                                                <div class="grid__item pd-left15 large--one-whole text-center">
                                                    <a href="{{ url($menu[2]->id.'-'.$menu[2]->Slug) }}.html" class="hmp-viewmore">Xem tất cả {{ $menu[2]->Name }}</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="hmptab1_4" class="hmp-tabcontent1">
                                            <div class="grid-uniform mg-left-15">
                                                @if (count($tbyte) > 0)
                                                @foreach ($tbyte as $items)
                                                    <div class="grid__item pd-left15 large--one-quarter medium--one-third small--one-half">
                                                        <form action="{{ url('mua-nhanh/'.$items->Pro_id) }}" enctype="multipart/form-data" id="" method="get" class="form-inline">
                                                        <div class="product-item">
                                                            <div class="product-img">
                                                                <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html"">
                                                                <img id="1014835198" class="lazyload" src="{{ asset('uploads/sanpham/'.$items->Image) }}" data-src="{{ asset('uploads/sanpham/'.$items->Image) }}" alt="{!! $items->Pro_Name !!}">
                                                                </a>
                                                                
                                                                <div class="product-actions text-center clearfix">
                                                                    <div>
                                                                        
                                                                        <button type="submit" class="btnBuyNow medium--hide small--hide">
                                                                            <span>Mua ngay</span>
                                                                        </button>
                                                                    
                                                                        
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                            <div class="product-item-info text-center">
                                                                <div class="product-title">
                                                                    <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html"">{!! $items->Pro_Name !!}</a>
                                                                </div>
                                                                <div class="product-price clearfix">
                                                                    <span class="current-price">{!! number_format($items->Gia,0,',','.') !!}₫</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        </form>
                                                    </div>
                                                @endforeach
                                                @else
                                                    <div class="col-lg-12 col-md-12 block_width"><span class="warning">Đang cập nhật sản phẩm</span></div>
                                                @endif
                                                
                                                <div class="grid__item pd-left15 large--one-whole text-center">
                                                    <a href="{{ url($menu[3]->id.'-'.$menu[3]->Slug) }}.html" class="hmp-viewmore">Xem tất cả {{ $menu[3]->Name }}</a>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </section>
                                <div id="home-banner" class="wow fadeInDown" data-wow-delay="0.2s" data-wow-duration="0.75s">
                                    <a href="http://member.civi.vn/cpc/?sid=31536&bid=10046504" target="_blank"><img  src="http://member.civi.vn/imp/b/?sid=31536&bid=10046504" /></a>
                                </div>
                                <section id="home-articles">
                                    <div class="inner">
                                        <div class="section-title wow fadeInDown text-center">
                                            <h2>
                                                Tin tức nổi bật
                                            </h2>
                                            <span class="section-title-border"></span>
                                        </div>
                                        <div class="grid">
                                            <div id="owl-home-articles-slider" class="owl-carousel owl-theme">
                                                @foreach ($tintuc as $news)
                                                    <div class="item grid__item wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="0.75s">
                                                        <div class="article-item">
                                                            <div class="article-img">
                                                                <a href="{{ url('tin-tuc/'.$news->Slug) }}">
                                                                <img src="{{ asset('uploads/tintuc/'.$news->Image) }}" alt="{!! $news->TieuDe !!}">
                                                                </a>
                                                            </div>
                                                            <div class="article-info-wrapper">
                                                                <div class="article-title">
                                                                    <a href="{{ url('tin-tuc/'.$news->Slug) }}">{!! $news->TieuDe !!}</a>
                                                                </div>
                                                                <div class="article-desc">
                                                                    {!! strip_tags(str_limit($news->TomTat,200)) !!}
                                                                </div>
                                                                <div class="article-info">
                                                                    <div class="article-date">
                                                                        <i class="fas fa-calendar-alt"></i> {!! $news->created_at !!}
                                                                    </div>
                                                                    <div class="article-author">
                                                                        <i class="fas fa-user"></i> {{ $news->User->name }}
                                                                    </div>
                                                                    {{-- <div class="article-comment">
                                                                        <i class="fas fa-comments"></i> <span class="fb-comments-count" data-href="https://nha-thuoc-suplo.myharavan.com/blogs/news/moi-nguy-hiem-khi-thuong-xuyen-tieu-thu-do-an-nhanh"></span>
                                                                    </div> --}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="grid__item large--three-twelfths medium--one-whole small--one-whole">
                                <div id="index-sidebar">
                                    <div id="index-sidebar-categories" class="wow fadeInLeft">
                                        <h3 class="text-center">
                                            Danh mục sản phẩm
                                        </h3>
                                        <ul class="no-bullets">
                                            @foreach ($danhmuccha as $cate)
                                                <li>
                                                    <a href="{{ url($cate->id.'-'.$cate->Slug) }}.html">
                                                    <span class="isb-cate-icon"><i class="fas fa-caret-right"></i></span>
                                                    <span class="isb-cate-text">{{ $cate->Name }}</span>
                                                    </a>
                                                </li>
                                            @endforeach

                                        </ul>
                                    </div>
                                    <div id="index-sidebar-contact" class="wow fadeInLeft">
                                        <h3 class="text-center">
                                            Hỗ trợ trực tuyến
                                        </h3>
                                        <div class="isb-contact-form">
                                            <div class="form-vertical clearfix">
                                                <form accept-charset='UTF-8' action='{{ url('send-mail') }}' class='contact-form' method='post'>
                                                    {{ csrf_field() }}
                                                    <input name='form_type' type='hidden' value='contact'>
                                                    <input name='utf8' type='hidden' value='✓'>
                                                    <label for="ContactFormName" class="hidden-label">Họ tên của bạn</label>
                                                    <input type="text" id="ContactFormName" class="input-full" name="hoten" required="" placeholder="Họ tên của bạn" autocapitalize="words" value="">
                                                    <label for="ContactFormEmail" class="hidden-label">Địa chỉ email của bạn</label>
                                                    <input type="email" id="ContactFormEmail" class="input-full" name="email" required="" placeholder="Địa chỉ email của bạn" autocorrect="off" autocapitalize="off" value="">
                                                    <label for="ContactFormPhone" class="hidden-label">Số điện thoại của bạn</label>
                                                    <input type="tel" id="ContactFormPhone" class="input-full" name="phone" required="" placeholder="Số điện thoại của bạn" pattern="[0-9\-]*" value="">
                                                    <label for="ContactFormMessage" class="hidden-label">Nội dung</label>
                                                    <textarea rows="5" id="ContactFormMessage" class="input-full" name="content" required="" placeholder="Nội dung"></textarea>
                                                    <div class="text-center">
                                                        <input type="submit" class="btn btnContactSubmit" id="khlienHe" value="Gửi liên hệ">
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div id="index-sidebar-flashsale" class="wow fadeInLeft">
                                        <h3 class="text-center">
                                            Sản phẩm khuyến mãi
                                        </h3>
                                        <div class="isb-flashsale-wrapper mg-left-15 grid">
                                            <div id="owl-home-featured-products-slider" class="owl-carousel owl-theme">
                                                @foreach ($sanphamkhuyenmai as $items)
                                                    <div class="item grid__item pd-left15">
                                                        <div class="product-item">
                                                            <div class="product-img">
                                                                <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">
                                                                <img id="1014835198" class="lazyload" src="{{ asset('img/lazyload.jpg') }}" data-src="{{ asset('uploads/sanpham/'.$items->Image) }}" alt="{{ $items->Pro_Name }}">
                                                                </a>
                                                                <div class="product-actions text-center clearfix">
                                                                    <div>
                                                                        <button type="button" class="btnBuyNow medium--hide small--hide">
                                                                            <span><a href="{{ url('mua-nhanh/'.$items->Pro_id) }}" style="color: #fff"> Mua ngay</span>
                                                                        </button>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="product-item-info text-center">
                                                                <div class="product-title">
                                                                    <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">{{ $items->Pro_Name }}</a>
                                                                </div>
                                                                <div class="product-price clearfix">
                                                                    <span class="current-price">{!! number_format($items->Gia,0,',','.') !!}₫</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                
                                            </div>
                                        </div>
                                        {{-- <div class="home-collection-countdown text-center">
                                            <div class="countdown-days">
                                                <div id="days">
                                                    11
                                                </div>
                                                <div>
                                                    Ngày
                                                </div>
                                            </div>
                                            <div class="countdown-hrs">
                                                <div id="hrs">
                                                    11
                                                </div>
                                                <div>
                                                    Giờ
                                                </div>
                                            </div>
                                            <div class="countdown-mins">
                                                <div id="mins">
                                                    11
                                                </div>
                                                <div>
                                                    Phút
                                                </div>
                                            </div>
                                            <div class="countdown-secs">
                                                <div id="secs">
                                                    11
                                                </div>
                                                <div>
                                                    Giây
                                                </div>
                                            </div>
                                        </div> --}}
                                    </div>
                                    <div id="index-sidebar-subscribe" class="fadeInLeft wow">
                                        <h3 class="text-center">
                                            Địa chỉ
                                        </h3>
                                        <div class="isb-sub-desc">

                                            {!! $lienhe->Map !!}
                                            {{-- <p>
                                                Mỗi tháng chúng tôi đều có những đợt giảm giá dịch vụ và sản phẩm nhằm chi ân khách hàng. Để có thể cập nhật kịp thời những đợt giảm giá này, vui lòng nhập địa chỉ email của bạn vào ô dưới đây.
                                            </p>
                                            <div class="isb-sub-form">
                                                <form accept-charset='UTF-8' action='{{ url('send-mail') }}' class='contact-form' method='post'>
                                                    {{ csrf_field() }}
                                                    <input name='form_type' type='hidden' value='customer'>
                                                    <input name='utf8' type='hidden' value='✓'>
                                                    <div class="input-group">
                                                        <input type="email" value="" placeholder="Email của bạn..." name="email" id="Email" class="input-group-field" required="">
                                                        
                                                        <span class="input-group-btn">                        
                                                        <button type="submit" name="subscribe" id="subscribe" value="GỬI">Đăng ký</button>
                                                        </span>                   
                                                    </div>
                                                </form>
                                            </div> --}}
                                        </div>
                                    </div>
                                    <div class="fadeInLeft wow">
                                        <a href="http://member.civi.vn/cpc/?sid=31536&bid=10056605" target="_blank"><img  src="http://member.civi.vn/imp/b/?sid=31536&bid=10056605" /></a>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section id="home-small-collections">
                <div class="wrapper">
                    <div class="inner">
                        <div class="grid">
                            <div class="grid__item large--one-half medium--one-whole small--one-whole">
                                <div class="hsmallcol-wrapper">
                                    <div class="section-title wow fadeInDown">
                                        <h2>
                                            Cây thuốc quý
                                        </h2>
                                        <span class="section-title-border"></span>
                                    </div>
                                    <div class="grid-uniform md-mg-left-15">
                                        @if(count($thuocquy)>0)
                                        @foreach ($thuocquy as $items)
                                            <div class="grid__item md-pd-left15 large--one-half medium--one-half small--one-half">
                                                <div class="hsmallcol-item wow fadeInDown" data-wow-duration="0.75s" data-wow-delay="0.2s">
                                                    <div class="grid mg-left-5">
                                                        <div class="grid__item large--one-third medium--one-third small--one-third pd-left5">
                                                            <div class="hsmallcol-img">
                                                                <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">
                                                                <img class="lazyload" src="{{ asset('img/lazyload.jpg') }}" data-src="{{ asset('uploads/sanpham/'.$items->Image) }}" alt="{{ $items->Pro_Name }}">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="grid__item large--two-thirds medium--two-thirds small--two-thirds pd-left5">
                                                            <div class="hsmallcol-title">
                                                                <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">
                                                                {{ $items->Pro_Name }}
                                                                </a>
                                                            </div>
                                                            <div class="hsmallcol-price">
                                                                <span class="current-price">{!! number_format($items->Gia,0,',','.') !!}₫</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        
                                        @else
                                        <div class="grid__item md-pd-left15 large--one-half medium--one-half small--one-half">
                                            Sản phẩm đang cập nhật!
                                        </div>
                                        @endif
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="grid__item large--one-half medium--one-whole small--one-whole">
                                <div class="hsmallcol-wrapper">
                                    <div class="section-title wow fadeInDown">
                                        <h2>
                                            Bao cao su
                                        </h2>
                                        <span class="section-title-border"></span>
                                    </div>
                                    <div class="grid-uniform md-mg-left-15">
                                        @if(count($bcs)>0)
                                        @foreach ($bcs as $items)
                                            <div class="grid__item md-pd-left15 large--one-half medium--one-half small--one-half">
                                                <div class="hsmallcol-item wow fadeInDown" data-wow-duration="0.75s" data-wow-delay="0.2s">
                                                    <div class="grid mg-left-5">
                                                        <div class="grid__item large--one-third medium--one-third small--one-third pd-left5">
                                                            <div class="hsmallcol-img">
                                                                <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">
                                                                <img class="lazyload" src="{{ asset('img/lazyload.jpg') }}" data-src="{{ asset('uploads/sanpham/'.$items->Image) }}" alt="{{ $items->Pro_Name }}">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="grid__item large--two-thirds medium--two-thirds small--two-thirds pd-left5">
                                                            <div class="hsmallcol-title">
                                                                <a href="{{ url('san-pham/'.$items->Pro_id.'-'.$items->Slug) }}.html">
                                                                {{ $items->Pro_Name }}
                                                                </a>
                                                            </div>
                                                            <div class="hsmallcol-price">
                                                                <span class="current-price">{!! number_format($items->Gia,0,',','.') !!}₫</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        
                                        @else
                                        <div class="grid__item md-pd-left15 large--one-half medium--one-half small--one-half">
                                            Sản phẩm đang cập nhật!
                                        </div>
                                        @endif
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="home-brands">
                <div class="wrapper">
                    <div class="inner">
                        <div class="section-title wow fadeInDown text-center">
                            <h2>
                                Đối tác của chúng tôi
                            </h2>
                            <span class="section-title-border"></span>
                        </div>
                        <div class="grid">
                            <div id="owl-brands-slider" class="owl-carousel owl-theme">
                                <div class="item grid__item wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="0.75s">
                                    <a href="javascrip:;" class="text-center"><img src="https://theme.hstatic.net/1000282280/1000362500/14/brand_img1.png?v=1486" alt="Logo hãng 1"></a>
                                </div>
                                <div class="item grid__item wow fadeInUp" data-wow-delay="0.4s" data-wow-duration="0.75s">
                                    <a href="javascrip:;" class="text-center"><img src="https://theme.hstatic.net/1000282280/1000362500/14/brand_img2.png?v=1486" alt="Logo hãng 2"></a>
                                </div>
                                <div class="item grid__item wow fadeInUp" data-wow-delay="0.6s" data-wow-duration="0.75s">
                                    <a href="javascrip:;" class="text-center"><img src="https://theme.hstatic.net/1000282280/1000362500/14/brand_img3.png?v=1486" alt="Logo hãng 3"></a>
                                </div>
                                <div class="item grid__item wow fadeInUp" data-wow-delay="0.8s" data-wow-duration="0.75s">
                                    <a href="javascrip:;" class="text-center"><img src="https://theme.hstatic.net/1000282280/1000362500/14/brand_img4.png?v=1486" alt="Logo hãng 4"></a>
                                </div>
                                <div class="item grid__item wow fadeInUp" data-wow-delay="1s" data-wow-duration="0.75s">
                                    <a href="javascrip:;" class="text-center"><img src="https://theme.hstatic.net/1000282280/1000362500/14/brand_img5.png?v=1486" alt="Logo hãng 5"></a>
                                </div>
                                <div class="item grid__item wow fadeInUp" data-wow-delay="1.2s" data-wow-duration="0.75s">
                                    <a href="javascrip:;" class="text-center"><img src="https://theme.hstatic.net/1000282280/1000362500/14/brand_img6.png?v=1486" alt="Logo hãng 6"></a>
                                </div>
                                <div class="item grid__item wow fadeInUp" data-wow-delay="1.4s" data-wow-duration="0.75s">
                                    <a href="javascrip:;" class="text-center"><img src="https://theme.hstatic.net/1000282280/1000362500/14/brand_img7.png?v=1486" alt="Logo hãng 7"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
    
@endsection
@section('plugins-js')
@endsection