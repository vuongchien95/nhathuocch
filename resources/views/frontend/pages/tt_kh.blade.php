<div>
	<ul style="line-height: 20px;">
		<li>
			<span style="font-weight: 700">Tên khách hàng:</span> {{ $information['hoten'] }}
		</li>
		<li>
			<span style="font-weight: 700">Email:</span> {{ $information['email'] }}
		</li>
		<li>
			<span style="font-weight: 700">Số điện thoại:</span> {{ $information['phone'] }}
		</li>
		<li>
			<span style="font-weight: 700">Nội dung:</span> {{ $information['content'] }}
		</li>
	</ul>
</div>
