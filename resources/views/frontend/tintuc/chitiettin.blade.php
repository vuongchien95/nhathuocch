@extends('frontend.layout.master')

@section('title',$chitiettin->TieuDe)
@section('keywords',$chitiettin->TuKhoaSeo)
@section('description',$chitiettin->MoTaSeo)
@section('url',url('/tin-tuc/'.$chitiettin->Slug))
@section('titleseo',$chitiettin->TieuDeSeo)
@section('type','news')
@section('descriptionseo',$chitiettin->MoTaSeo)
@section('image')

@section('content')
    <section id="breadcrumb-wrapper">
        <div class="breadcrumb-overlay"></div>
        <div class="breadcrumb-content">
            <div class="wrapper">
                <div class="inner text-center">
                    <div class="breadcrumb-big">
                        <h2>
                            {{ $chitiettin->Tieude }}
                        </h2>
                    </div>
                    <div class="breadcrumb-small">
                        <a href="{{ url('/') }}" title="Quay trở về trang chủ">Trang chủ</a>
                        <span aria-hidden="true">/</span>
                        <a href="{{ url('tin-tuc.html') }}" title="">Tin tức</a>
                        <span aria-hidden="true">/</span>
                        <span>{{ $chitiettin->TieuDe }}</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            <section id="blog-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        <div class="grid">
                            <article class="float-right grid__item large--nine-twelfths medium--one-whole small--one-whole" itemscope itemtype="http://schema.org/Article">
                                <div class="article-content">
                                    <div class="article-head">
                                        <h1>{{ $chitiettin->TieuDe }}</h1>
                                        <div class="grid mg-left-15">
                                            <div class="grid__item large--one-half medium--one-half small--one-half pd-left15">
                                                <div class="article-date-comment">
                                                    <div class="date"><i class="fas fa-calendar-alt"></i> {!! $chitiettin->created_at !!}</div>
                                                    <div class="comment"><i class="fas fa-comment-alt"></i> <span class="fb-comments-count" data-href="https://nha-thuoc-suplo.myharavan.com/blogs/news/moi-nguy-hiem-khi-thuong-xuyen-tieu-thu-do-an-nhanh"></span></div>
                                                    <div class="author">
                                                        <i class="fas fa-user"></i> {{ $chitiettin->User->name }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="grid__item large--one-half medium--one-half small--one-half pd-left15">
                                                <div class="social-network-actions text-right">
                                                    <div class="fb-like" data-href="https://nha-thuoc-suplo.myharavan.com/blogs/news/moi-nguy-hiem-khi-thuong-xuyen-tieu-thu-do-an-nhanh" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="article-tldr clearfix">
                                        <p>{!! strip_tags($chitiettin->TomTat) !!}</p>
                                    </div>
                                    <div class="article-body">
                                        {!! $chitiettin->MoTa !!}
                                    </div>
                                    
                                </div>
                                <div class="social-network-actions-outside text-right">
                                    <div class="fb-send" data-href="https://nha-thuoc-suplo.myharavan.com/blogs/news/moi-nguy-hiem-khi-thuong-xuyen-tieu-thu-do-an-nhanh"></div>
                                    <div class="fb-like" data-href="https://nha-thuoc-suplo.myharavan.com/blogs/news/moi-nguy-hiem-khi-thuong-xuyen-tieu-thu-do-an-nhanh" data-layout="button" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                                </div>
                                <div id="section-fbcomment" class="fb-comments-wrapper">
                                    <div class="fb-comments" data-href="https://nha-thuoc-suplo.myharavan.com/blogs/news/moi-nguy-hiem-khi-thuong-xuyen-tieu-thu-do-an-nhanh" data-numposts="5"></div>
                                </div>
                                <div class="related-articles">
                                    <div class="related-articles-head">
                                        <h3>
                                            Các bài viết liên quan
                                        </h3>
                                    </div>
                                    <div class="related-articles-body">
                                        <ul class="no-bullets">
                                            @foreach ($tintuckhac as $items)
                                                <li>
                                                    <a href="{{ url('tin-tuc/'.$items->Slug) }}">{!! $items->TieuDe !!}</a>
                                                </li>
                                            @endforeach
                                            
                                            
                                        </ul>
                                    </div>
                                </div>
                            </article>

                            @include('frontend.layout.sidebarTt')
                            
                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
@endsection