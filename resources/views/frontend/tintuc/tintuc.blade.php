@extends('frontend.layout.master')

@section('title','Tin Tức - Nhà Thuốc Chính Hãng')
@section('keywords',$cauhinh->TuKhoa)
@section('description',$cauhinh->MoTa)
@section('url',url('/tin-tuc.html'))
@section('titleseo',$cauhinh->TuKhoa)
@section('type','news')
@section('descriptionseo',$cauhinh->TuKhoa)
@section('image')

@section('content')
    <section id="breadcrumb-wrapper">
        <div class="breadcrumb-overlay"></div>
        <div class="breadcrumb-content">
            <div class="wrapper">
                <div class="inner text-center">
                    <div class="breadcrumb-big">
                        <h2>
                            Tin tức
                        </h2>
                    </div>
                    <div class="breadcrumb-small">
                        <a href="/" title="Quay trở về trang chủ">Trang chủ</a>
                        <span aria-hidden="true">/</span>
                        <span>Tin tức</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="PageContainer" class="is-moved-by-drawer">
        <main class="main-content" role="main">
            <section id="blog-wrapper">
                <div class="wrapper">
                    <div class="inner">
                        <div class="grid">
                            <div class="grid__item large--nine-twelfths medium--one-whole small--one-whole float-right">
                                <div class="blog-content">
                                    <div class="blog-content-wrapper">
                                        <div class="blog-head">
                                            <div class="blog-title">
                                                <h1>Tin tức</h1>
                                            </div>
                                        </div>
                                        <div class="blog-body">
                                            <div class="grid-uniform">
                                                @foreach ($tintuc as $items)
                                                    <div class="grid__item large--one-half medium--one-half small--one-whole">
                                                        <div class="article-item">
                                                            <div class="article-img">
                                                                <a href="{{ url('tin-tuc/'.$items->Slug) }}">
                                                                <img src="{{ asset('uploads/tintuc/'.$items->Image) }}" alt="{!! $items->TieuDe !!}" />
                                                                </a>
                                                            </div>
                                                            <div class="article-info-wrapper">
                                                                <div class="article-title">
                                                                    <a href="{{ url('tin-tuc/'.$items->Slug) }}">{!! $items->TieuDe !!}</a>
                                                                </div>
                                                                <div class="article-desc">
                                                                    {!! strip_tags(str_limit($items->TomTat,200)) !!}

                                                                </div>
                                                                <div class="article-info">
                                                                    <div class="article-date">
                                                                        <i class="fas fa-calendar-alt"></i> {!! $items->created_at !!}
                                                                    </div>
                                                                    <div class="article-author">
                                                                        <i class="fas fa-user"></i> {{ $items->User->name }}
                                                                    </div>
                                                                    {{-- <div class="article-comment">
                                                                        <i class="fas fa-comments"></i> <span class="fb-comments-count" data-href="https://nha-thuoc-suplo.myharavan.com/blogs/news/moi-nguy-hiem-khi-thuong-xuyen-tieu-thu-do-an-nhanh"></span>
                                                                    </div> --}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                
                                                
                                            </div>
                                            
                                        </div>

                                        <div class="pagination not-filter">
                                            {!! $tintuc->links() !!}    
                                        </div>

                                    </div>
                                </div>
                            </div>
                            
                            @include('frontend.layout.sidebarTt')

                        </div>
                    </div>
                </div>
            </section>
        </main>
    </div>
@endsection