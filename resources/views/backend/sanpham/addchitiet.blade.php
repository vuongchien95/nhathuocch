@extends('backend.layout.master')
@section('content')
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="{{ url('admin/pages') }}">Home</a>
        </li>

        <li>
          <a href="{{ url('admin/sanpham') }}"> Sản Phẩm </a>
        </li>
        <li class="active">Thêm chi tiết sản phẩm</li>
      </ul><!-- /.breadcrumb -->
      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
    <div class="page-content">
      <div class="row">
        <div class="col-xs-12">
          <div class="widget-box">
            <div class="widget-header widget-header-blue widget-header-flat">
              <h4 class="widget-title lighter">Thêm chi tiết sản phẩm: {{ $sanpham->Pro_Name }}</h4>
            </div>
            <div class="widget-body">
              @include('backend.block.err')
              <div class="widget-main">
                <div id="fuelux-wizard-container">
                  <div class="step-content pos-rel">
                    <div class="step-pane active" data-step="1">
                      <form action="{{ route('admin.sanpham.getDetail',$sanpham->Pro_id) }}" class="form-horizontal" method="POST" id="validation-form" enctype="multipart/form-data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                          <div class="pull-text col-sm-3" ></div>
                          <div class="pull-text col-sm-7">
                            @include('backend.block.alert')
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Thêm Ảnh Cho Sản Phẩm:</label>
                          <div class="col-xs-12 col-sm-6">
                            <div class="clearfix">
                              <input multiple="" name="avatar[]" type="file" id="id-input-file-3" />
                            </div>
                          </div>
                        </div>      
                        <div class="row form-group" style="margin-bottom: 2em">
                          @if(count($sp_img) > 0)
                          @foreach($sp_img as $items)
                          <div class="col-md-2" style="margin-bottom: 2em">
                            <img src="{{ asset('uploads/images/sanphamimg/'.$items->Image) }}" style="width: 100%;height: 100%;padding: 1em;border: 1px solid #c8bdbd; border-radius: 5px;">
                            &nbsp; &nbsp; &nbsp;
                            <a href="{{ url('admin/sanpham/delimg',$items->id) }}" class="btn btn-danger" style="margin:1em 0 0 2em;border-radius:5px;padding: 4px;">
                              <i class="ace-icon glyphicon glyphicon-remove"></i>
                              Xóa 
                            </a>
                          </div>
                          @endforeach
                          @endif
                        </div>
                        
                        

                        <!--<hr/>-->
                        <!--<div class="form-group">-->
                        <!--  <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Mã Giảm Giá</label>-->
                        <!--  <div class="col-xs-12 col-sm-9">-->
                        <!--    <div class="clearfix">-->
                        <!--      <input type="text" id="name" name="magiamgia" class="col-xs-12 col-sm-5" value="{!! old('magiamgia',isset($query) ? $query->MaGiamGia : null) !!}"  placeholder="Nhập mã giảm giá nếu có..." />-->
                        <!--    </div>-->
                        <!--  </div>-->
                        <!--</div>-->

                        <!--<div class="form-group">-->
                        <!--  <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Giảm Giá Còn:</label>-->
                        <!--  <div class="col-xs-12 col-sm-9">-->
                        <!--    <div class="clearfix">-->
                        <!--      <input type="text" id="name" name="gia" class="col-xs-12 col-sm-5" value="{!! old('gia',isset($query) ? $query->Gia : null) !!}" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"/>-->
                        <!--    </div>-->
                        <!--  </div>-->
                        <!--</div>-->
                        <hr />
                        <div class="wizard-actions">
                          <a href="{{ url('admin/sanpham') }}" class="btn btn-info"><i class="fa fa-reply"></i> Về Danh Sách</a>
                          <button class="btn" type="reset"><i class="ace-icon fa fa-undo bigger-110"></i> Nhập Lại </button>
                          <button type="submit" id="trigger-save" class="btn btn-success submit-form" name="add"><i class="fa fa-save"></i> Thêm Mới </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div>
        </div>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
  jQuery(function($) {

    $('[data-rel=tooltip]').tooltip();

    $('.select2').css('width','200px').select2({allowClear:true})
    .on('change', function(){
      $(this).closest('form').validate().element($(this));
    }); 
    $(document).one('ajaxloadstart.page', function(e) {
      $('[class*=select2]').remove();
    });
    $('#id-disable-check').on('click', function() {
      var inp = $('#form-input-readonly').get(0);
      if(inp.hasAttribute('disabled')) {
        inp.setAttribute('readonly' , 'true');
        inp.removeAttribute('disabled');
        inp.value="This text field is readonly!";
      }
      else {
        inp.setAttribute('disabled' , 'disabled');
        inp.removeAttribute('readonly');
        inp.value="This text field is disabled!";
      }
    });
    if(!ace.vars['touch']) {
      $('.chosen-select').chosen({allow_single_deselect:true}); 

      $(window)
      .off('resize.chosen')
      .on('resize.chosen', function() {
        $('.chosen-select').each(function() {
          var $this = $(this);
          $this.next().css({'width': $this.parent().width()});
        })
      }).trigger('resize.chosen');
          //resize chosen on sidebar collapse/expand
          $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
            if(event_name != 'sidebar_collapsed') return;
            $('.chosen-select').each(function() {
              var $this = $(this);
              $this.next().css({'width': $this.parent().width()});
            })
          });


          $('#chosen-multiple-style .btn').on('click', function(e){
            var target = $(this).find('input[type=radio]');
            var which = parseInt(target.val());
            if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
            else $('#form-field-select-4').removeClass('tag-input-style');
          });
        }


        $('[data-rel=tooltip]').tooltip({container:'body'});
        $('[data-rel=popover]').popover({container:'body'});

        autosize($('textarea[class*=autosize]'));
        
        $('textarea.limited').inputlimiter({
          remText: '%n character%s remaining...',
          limitText: 'max allowed : %n.'
        });

        $.mask.definitions['~']='[+-]';
        $('.input-mask-date').mask('99/99/9999');
        $('.input-mask-phone').mask('(999) 999-9999');
        $('.input-mask-eyescript').mask('~9.99 ~9.99 999');
        $(".input-mask-product").mask("a*-999-a999",{placeholder:" ",completed:function(){alert("You typed the following: "+this.val());}});



        $( "#input-size-slider" ).css('width','200px').slider({
          value:1,
          range: "min",
          min: 1,
          max: 8,
          step: 1,
          slide: function( event, ui ) {
            var sizing = ['', 'input-sm', 'input-lg', 'input-mini', 'input-small', 'input-medium', 'input-large', 'input-xlarge', 'input-xxlarge'];
            var val = parseInt(ui.value);
            $('#form-field-4').attr('class', sizing[val]).attr('placeholder', '.'+sizing[val]);
          }
        });

        $( "#input-span-slider" ).slider({
          value:1,
          range: "min",
          min: 1,
          max: 12,
          step: 1,
          slide: function( event, ui ) {
            var val = parseInt(ui.value);
            $('#form-field-5').attr('class', 'col-xs-'+val).val('.col-xs-'+val);
          }
        });


        
        //"jQuery UI Slider"
        //range slider tooltip example
        $( "#slider-range" ).css('height','200px').slider({
          orientation: "vertical",
          range: true,
          min: 0,
          max: 100,
          values: [ 17, 67 ],
          slide: function( event, ui ) {
            var val = ui.values[$(ui.handle).index()-1] + "";

            if( !ui.handle.firstChild ) {
              $("<div class='tooltip right in' style='display:none;left:16px;top:-6px;'><div class='tooltip-arrow'></div><div class='tooltip-inner'></div></div>")
              .prependTo(ui.handle);
            }
            $(ui.handle.firstChild).show().children().eq(1).text(val);
          }
        }).find('span.ui-slider-handle').on('blur', function(){
          $(this.firstChild).hide();
        });
        
        
        $( "#slider-range-max" ).slider({
          range: "max",
          min: 1,
          max: 10,
          value: 100
        });
        
        $( "#slider-eq > span" ).css({width:'90%', 'float':'left', margin:'15px'}).each(function() {
          // read initial values from markup and remove that
          var value = parseInt( $( this ).text(), 10 );
          $( this ).empty().slider({
            value: value,
            range: "min",
            animate: true
            
          });
        });
        
        $("#slider-eq > span.ui-slider-purple").slider('disable');//disable third item

        
        $('#id-input-file-1 , #id-input-file-2').ace_file_input({
          no_file:'No File ...',
          btn_choose:'Choose',
          btn_change:'Change',
          droppable:false,
          onchange:null,
          thumbnail:false //| true | large
          //whitelist:'gif|png|jpg|jpeg'
          //blacklist:'exe|php'
          //onchange:''
          //
        });
        //pre-show a file name, for example a previously selected file
        //$('#id-input-file-1').ace_file_input('show_file_list', ['myfile.txt'])


        $('#id-input-file-3').ace_file_input({
          style: 'well',
          btn_choose: 'Bấm Ctrl Để Chọn Nhiều Ảnh',
          btn_change: null,
          no_icon: 'ace-icon fa fa-cloud-upload',
          droppable: true,
          thumbnail: 'small'//large | fit
          //,icon_remove:null//set null, to hide remove/reset button
          /**,before_change:function(files, dropped) {
            //Check an example below
            //or examples/file-upload.html
            return true;
          }*/
          /**,before_remove : function() {
            return true;
          }*/
          ,
          preview_error : function(filename, error_code) {
            //name of the file that failed
            //error_code values
            //1 = 'FILE_LOAD_FAILED',
            //2 = 'IMAGE_LOAD_FAILED',
            //3 = 'THUMBNAIL_FAILED'
            //alert(error_code);
          }

        }).on('change', function(){
          //console.log($(this).data('ace_input_files'));
          //console.log($(this).data('ace_input_method'));
        });
        
        
        //$('#id-input-file-3')
        //.ace_file_input('show_file_list', [
          //{type: 'image', name: 'name of image', path: 'http://path/to/image/for/preview'},
          //{type: 'file', name: 'hello.txt'}
        //]);


        //dynamically change allowed formats by changing allowExt && allowMime function
        $('#id-file-format').removeAttr('checked').on('change', function() {
          var whitelist_ext, whitelist_mime;
          var btn_choose
          var no_icon
          if(this.checked) {
            btn_choose = "Bấm Ctrl Để Chọn Nhiều Ảnh";
            no_icon = "ace-icon fa fa-picture-o";

            whitelist_ext = ["jpeg", "jpg", "png", "gif" , "bmp"];
            whitelist_mime = ["image/jpg", "image/jpeg", "image/png", "image/gif", "image/bmp"];
          }
          else {
            btn_choose = "Bấm Ctrl Để Chọn Nhiều Ảnh";
            no_icon = "ace-icon fa fa-cloud-upload";
            
            whitelist_ext = null;//all extensions are acceptable
            whitelist_mime = null;//all mimes are acceptable
          }
          var file_input = $('#id-input-file-3');
          file_input
          .ace_file_input('update_settings',
          {
            'btn_choose': btn_choose,
            'no_icon': no_icon,
            'allowExt': whitelist_ext,
            'allowMime': whitelist_mime
          })
          file_input.ace_file_input('reset_input');
          
          file_input
          .off('file.error.ace')
          .on('file.error.ace', function(e, info) {
            //console.log(info.file_count);//number of selected files
            //console.log(info.invalid_count);//number of invalid files
            //console.log(info.error_list);//a list of errors in the following format
            
            //info.error_count['ext']
            //info.error_count['mime']
            //info.error_count['size']
            
            //info.error_list['ext']  = [list of file names with invalid extension]
            //info.error_list['mime'] = [list of file names with invalid mimetype]
            //info.error_list['size'] = [list of file names with invalid size]
            
            
            /**
            if( !info.dropped ) {
              //perhapse reset file field if files have been selected, and there are invalid files among them
              //when files are dropped, only valid files will be added to our file array
              e.preventDefault();//it will rest input
            }
            */
            
            
            //if files have been selected (not dropped), you can choose to reset input
            //because browser keeps all selected files anyway and this cannot be changed
            //we can only reset file field to become empty again
            //on any case you still should check files with your server side script
            //because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
          });
          
          
          /**
          file_input
          .off('file.preview.ace')
          .on('file.preview.ace', function(e, info) {
            console.log(info.file.width);
            console.log(info.file.height);
            e.preventDefault();//to prevent preview
          });
          */

        });

        $('#spinner1').ace_spinner({value:0,min:0,max:200,step:10, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
        .closest('.ace-spinner')
        .on('changed.fu.spinbox', function(){
          //console.log($('#spinner1').val())
        }); 
        $('#spinner2').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner3').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});


        $('#spinner4').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner5').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner6').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner7').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner8').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner9').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner10').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner11').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner12').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner13').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner14').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner15').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner16').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner17').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner18').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner19').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner20').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner21').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner22').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner23').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner24').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner25').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner26').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner27').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner28').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner29').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner30').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner31').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner32').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner33').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner34').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner35').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner36').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner37').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner38').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner39').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner40').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner41').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner42').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner43').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner44').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner45').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner46').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner47').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner48').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner49').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner50').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner51').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner52').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner53').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner54').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner55').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner56').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner57').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});

        $('#spinner58').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner59').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner60').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner61').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner62').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner63').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner64').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner65').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner66').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner67').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner68').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner69').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner70').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner71').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});

        $('#spinner72').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner73').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner74').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner75').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner76').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner77').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner78').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner79').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner80').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner81').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner82').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
        $('#spinner83').ace_spinner({value:0,min:0,max:10000,step:10, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});


        //$('#spinner1').ace_spinner('disable').ace_spinner('value', 11);
        //or
        //$('#spinner1').closest('.ace-spinner').spinner('disable').spinner('enable').spinner('value', 11);//disable, enable or change value
        //$('#spinner1').closest('.ace-spinner').spinner('value', 0);//reset to 0


        //datepicker plugin
        //link
        $('.date-picker').datepicker({
          autoclose: true,
          todayHighlight: true
        })
        //show datepicker when clicking on the icon
        .next().on(ace.click_event, function(){
          $(this).prev().focus();
        });

        //or change it into a date range picker
        $('.input-daterange').datepicker({autoclose:true});


        //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
        $('input[name=date-range-picker]').daterangepicker({
          'applyClass' : 'btn-sm btn-success',
          'cancelClass' : 'btn-sm btn-default',
          locale: {
            applyLabel: 'Apply',
            cancelLabel: 'Cancel',
          }
        })
        .prev().on(ace.click_event, function(){
          $(this).next().focus();
        });


        $('#timepicker1').timepicker({
          minuteStep: 1,
          showSeconds: true,
          showMeridian: false,
          disableFocus: true,
          icons: {
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down'
          }
        }).on('focus', function() {
          $('#timepicker1').timepicker('showWidget');
        }).next().on(ace.click_event, function(){
          $(this).prev().focus();
        });
        
        

        
        if(!ace.vars['old_ie']) $('#date-timepicker1').datetimepicker({
         //format: 'MM/DD/YYYY h:mm:ss A',//use this option to display seconds
         icons: {
          time: 'fa fa-clock-o',
          date: 'fa fa-calendar',
          up: 'fa fa-chevron-up',
          down: 'fa fa-chevron-down',
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-arrows ',
          clear: 'fa fa-trash',
          close: 'fa fa-times'
        }
      }).next().on(ace.click_event, function(){
        $(this).prev().focus();
      });


      $('#colorpicker1').colorpicker();
        //$('.colorpicker').last().css('z-index', 2000);//if colorpicker is inside a modal, its z-index should be higher than modal'safe

        $('#simple-colorpicker-1').ace_colorpicker();
        $('#simple-colorpicker-2').ace_colorpicker();
        $('#simple-colorpicker-3').ace_colorpicker();
        $('#simple-colorpicker-4').ace_colorpicker();
        $('#simple-colorpicker-5').ace_colorpicker();
        $('#simple-colorpicker-6').ace_colorpicker();

        $('#simple-colorpicker-7').ace_colorpicker();
        $('#simple-colorpicker-8').ace_colorpicker();
        $('#simple-colorpicker-9').ace_colorpicker();
        $('#simple-colorpicker-10').ace_colorpicker();
        $('#simple-colorpicker-11').ace_colorpicker();
        $('#simple-colorpicker-12').ace_colorpicker();
        $('#simple-colorpicker-13').ace_colorpicker();
        $('#simple-colorpicker-14').ace_colorpicker();
        $('#simple-colorpicker-15').ace_colorpicker();
        $('#simple-colorpicker-16').ace_colorpicker();
        $('#simple-colorpicker-17').ace_colorpicker();
        $('#simple-colorpicker-18').ace_colorpicker();
        $('#simple-colorpicker-19').ace_colorpicker();
        $('#simple-colorpicker-20').ace_colorpicker();
        $('#simple-colorpicker-21').ace_colorpicker();
        $('#simple-colorpicker-22').ace_colorpicker();
        $('#simple-colorpicker-23').ace_colorpicker();
        $('#simple-colorpicker-24').ace_colorpicker();
        $('#simple-colorpicker-25').ace_colorpicker();
        $('#simple-colorpicker-26').ace_colorpicker();
        $('#simple-colorpicker-27').ace_colorpicker();
        $('#simple-colorpicker-28').ace_colorpicker();
        $('#simple-colorpicker-29').ace_colorpicker();
        $('#simple-colorpicker-30').ace_colorpicker();
        $('#simple-colorpicker-31').ace_colorpicker();
        $('#simple-colorpicker-32').ace_colorpicker();
        $('#simple-colorpicker-33').ace_colorpicker();
        $('#simple-colorpicker-34').ace_colorpicker();
        $('#simple-colorpicker-35').ace_colorpicker();
        $('#simple-colorpicker-36').ace_colorpicker();
        $('#simple-colorpicker-37').ace_colorpicker();
        $('#simple-colorpicker-38').ace_colorpicker();
        $('#simple-colorpicker-39').ace_colorpicker();
        $('#simple-colorpicker-40').ace_colorpicker();
        $('#simple-colorpicker-41').ace_colorpicker();
        $('#simple-colorpicker-42').ace_colorpicker();
        $('#simple-colorpicker-43').ace_colorpicker();
        $('#simple-colorpicker-44').ace_colorpicker();

        $('#simple-colorpicker-45').ace_colorpicker();
        $('#simple-colorpicker-46').ace_colorpicker();
        $('#simple-colorpicker-47').ace_colorpicker();
        $('#simple-colorpicker-48').ace_colorpicker();
        $('#simple-colorpicker-49').ace_colorpicker();
        $('#simple-colorpicker-50').ace_colorpicker();
        $('#simple-colorpicker-51').ace_colorpicker();
        $('#simple-colorpicker-52').ace_colorpicker();
        $('#simple-colorpicker-53').ace_colorpicker();
        $('#simple-colorpicker-54').ace_colorpicker();
        $('#simple-colorpicker-55').ace_colorpicker();
        $('#simple-colorpicker-56').ace_colorpicker();
        $('#simple-colorpicker-57').ace_colorpicker();
        $('#simple-colorpicker-58').ace_colorpicker();
        $('#simple-colorpicker-59').ace_colorpicker();
        $('#simple-colorpicker-60').ace_colorpicker();
        $('#simple-colorpicker-61').ace_colorpicker();
        $('#simple-colorpicker-62').ace_colorpicker();
        $('#simple-colorpicker-63').ace_colorpicker();
        $('#simple-colorpicker-64').ace_colorpicker();
        $('#simple-colorpicker-65').ace_colorpicker();

        $('#simple-colorpicker-66').ace_colorpicker();
        $('#simple-colorpicker-67').ace_colorpicker();
        $('#simple-colorpicker-68').ace_colorpicker();
        $('#simple-colorpicker-69').ace_colorpicker();
        $('#simple-colorpicker-70').ace_colorpicker();
        $('#simple-colorpicker-71').ace_colorpicker();
        $('#simple-colorpicker-72').ace_colorpicker();
        $('#simple-colorpicker-73').ace_colorpicker();
        $('#simple-colorpicker-74').ace_colorpicker();
        $('#simple-colorpicker-75').ace_colorpicker();
        $('#simple-colorpicker-76').ace_colorpicker();
        $('#simple-colorpicker-77').ace_colorpicker();
        $('#simple-colorpicker-78').ace_colorpicker();
        $('#simple-colorpicker-79').ace_colorpicker();
        $('#simple-colorpicker-80').ace_colorpicker();
        $('#simple-colorpicker-81').ace_colorpicker();
        $('#simple-colorpicker-82').ace_colorpicker();
        $('#simple-colorpicker-83').ace_colorpicker();
        $('#simple-colorpicker-84').ace_colorpicker();
        $('#simple-colorpicker-85').ace_colorpicker();
        $('#simple-colorpicker-86').ace_colorpicker();




        // $('#simple-colorpicker-1').ace_colorpicker();
        // $('#simple-colorpicker-1').ace_colorpicker();
        // $('#simple-colorpicker-1').ace_colorpicker();
        // $('#simple-colorpicker-1').ace_colorpicker();

        //$('#simple-colorpicker-1').ace_colorpicker('pick', 2);//select 2nd color
        //$('#simple-colorpicker-1').ace_colorpicker('pick', '#fbe983');//select #fbe983 color
        //var picker = $('#simple-colorpicker-1').data('ace_colorpicker')
        //picker.pick('red', true);//insert the color if it doesn't exist


        $(".knob").knob();
        
        
        var tag_input = $('#form-field-tags');
        try{
          tag_input.tag(
          {
            placeholder:tag_input.attr('placeholder'),
            //enable typeahead by specifying the source array
            source: ace.vars['US_STATES'],//defined in ace.js >> ace.enable_search_ahead
            /**
            //or fetch data from database, fetch those that match "query"
            source: function(query, process) {
              $.ajax({url: 'remote_source.php?q='+encodeURIComponent(query)})
              .done(function(result_items){
              process(result_items);
              });
            }
            */
          }
          )

          //programmatically add/remove a tag
          var $tag_obj = $('#form-field-tags').data('tag');
          $tag_obj.add('Programmatically Added');
          
          var index = $tag_obj.inValues('some tag');
          $tag_obj.remove(index);
        }
        catch(e) {
          //display a textarea for old IE, because it doesn't support this plugin or another one I tried!
          tag_input.after('<textarea id="'+tag_input.attr('id')+'" name="'+tag_input.attr('name')+'" rows="3">'+tag_input.val()+'</textarea>').remove();
          //autosize($('#form-field-tags'));
        }
        
        
        /////////
        $('#modal-form input[type=file]').ace_file_input({
          style:'well',
          btn_choose:'Drop files here or click to choose',
          btn_change:null,
          no_icon:'ace-icon fa fa-cloud-upload',
          droppable:true,
          thumbnail:'large'
        })
        
        //chosen plugin inside a modal will have a zero width because the select element is originally hidden
        //and its width cannot be determined.
        //so we set the width after modal is show
        $('#modal-form').on('shown.bs.modal', function () {
          if(!ace.vars['touch']) {
            $(this).find('.chosen-container').each(function(){
              $(this).find('a:first-child').css('width' , '210px');
              $(this).find('.chosen-drop').css('width' , '210px');
              $(this).find('.chosen-search input').css('width' , '200px');
            });
          }
        })
        /**
        //or you can activate the chosen plugin after modal is shown
        //this way select element becomes visible with dimensions and chosen works as expected
        $('#modal-form').on('shown', function () {
          $(this).find('.modal-chosen').chosen();
        })
        */

        
        
        $(document).one('ajaxloadstart.page', function(e) {
          autosize.destroy('textarea[class*=autosize]')
          
          $('.limiterBox,.autosizejs').remove();
          $('.daterangepicker.dropdown-menu,.colorpicker.dropdown-menu,.bootstrap-datetimepicker-widget.dropdown-menu').remove();
        });
      })
    </script>
@endsection

