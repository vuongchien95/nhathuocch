@extends('backend.layout.master')
@section('content')
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="{{ url('admin/pages') }}">Home</a>
        </li>

        <li>
          <a href="javascript:voi"> Sản Phẩm </a>
        </li>
        <li class="active"> Danh Sách Sản Phẩm</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>

    <div class="page-content">
      <div class="row">
        <div class="col-xs-12">
          <div class="row">
            <div class="col-xs-12">
              <div class="clearfix">
                <a href="{{ url('admin/sanpham/add') }}">
                  <div class="pull-text col-xs-3" style="margin-bottom: 5px">
                    <button class="btn btn-success"><i class="ace-icon glyphicon glyphicon-plus"></i> Thêm Mới </button>
                  </div>
                </a>
                <div class="pull-text col-xs-6">
                  @include('backend.block.alert')
                </div>
                <div class="pull-right tableTools-container col-xs-2"></div>
              </div>
              <div class="table-header">
                Danh Sách Sản Phẩm
              </div>

              <div>
                <table id="dynamic-table" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <th>Mã Sản Phẩm</th>
                      <th>Hình ảnh</th>
                      <th>Tên Sản Phẩm</th>
                      <th>Thuộc Danh Mục</th>
                      <th>Người Đăng</th>
                      <th>Nhà cung cấp</th>
                      <th class="hidden-480">Hiển Thị</th>
                      <th class="hidden-480">Trạng Thái</th>
                      <!--<th>Thêm Thông Tin</th>-->
                      <th>Chức Năng</th>
                    </tr>
                  </thead>

                  <tbody>
                    @foreach ($list as $items)
                    <tr>
                      <td>{{ $items->Pro_id }}</td>
                      <td><img style="width: 90px;height:90px" src="{{ asset('uploads/sanpham/'.$items->Image) }}"></td>
                      <td>{!! $items->Pro_Name !!}</td>
                      <td>
                        @php
                          $danhmuc = DB::table('danhmuc')->where('id',$items->idDanhMuc)->first();
                        @endphp
                        {{ $danhmuc->Name }}
                      </td>
                        
                      <td>
                        {{ $items->User->name }}
                      </td>

                      <td>
                        @php
                          $ncc = json_decode($items->NhaCungCap);
                        @endphp
                        @if($ncc !=null)
                          @foreach ($ncc as $it_ncc)
                            @php  
                              $nhacc = DB::table('nhacungcap')->where('id',$it_ncc)->first(); 
                            @endphp
                          <span class="label label-info">{!! $nhacc->name !!}</span>
                          @endforeach
                        @endif
                      </td>

                      <td class="hidden-480">
                        @if ( $items->AnHien == 1)
                        <span class="label label-success arrowed-in arrowed-in-right"> Hiện </span>
                        @else
                        <span class="label label-danger arrowed" style="padding-top: 5px;"> Ẩn </span>
                        @endif
                      </td>

                      <td class="hidden-480">
                        @if ( $items->TrangThai == 1)
                        <span class="label label-success arrowed-in arrowed-in-right"> Còn Hàng </span>
                        @else
                        <span class="label label-danger arrowed" style="padding-top: 5px;"> Hết Hàng </span>
                        @endif
                      </td>
                     <!-- <td>-->
                     <!--   <a href="{{ url('admin/sanpham/addDetail',$items->Pro_id) }}">-->
                     <!--     <button class="btn btn-white btn-default btn-round" style="padding: 4px 6px 0px;">-->
                     <!--      Thêm Chi Tiết-->
                     <!--    </button>-->
                     <!--  </a>-->
                     <!--</td>-->

                     <td>
                      <div class="hidden-sm hidden-xs action-buttons">
                        <a class="blue" href="{{ route('admin.sanpham.detail',$items->Pro_id) }}">
                          <i class="ace-icon fa fa-search-plus bigger-130"></i>
                          Chi Tiết
                        </a>

                        <a class="green" href="{{ route('admin.sanpham.getedit',$items->Pro_id) }}" style="margin-right: 5px" >
                          <i class="ace-icon fa fa-pencil-square-o bigger-130"></i>
                          Sửa 
                        </a>

                        <a class="red" href="{{ route('admin.sanpham.del',$items->Pro_id) }}" onclick="return confirm('Bạn có chắc là xóa Sản phẩm {{ $items->Pro_Name }} ')">
                          <i class="ace-icon fa fa-trash-o bigger-130"></i>
                          Xóa
                        </a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <div>{!! $list->links() !!}</div>
            </div>
          </div>
        </div>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->
@endsection
