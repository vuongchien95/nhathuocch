<div id="navbar" class="navbar navbar-default ace-save-state">
	<div class="navbar-container ace-save-state" id="navbar-container">
		<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
			<span class="sr-only">Toggle sidebar</span>

			<span class="icon-bar"></span>

			<span class="icon-bar"></span>

			<span class="icon-bar"></span>
		</button>

		<div class="navbar-header pull-left">
			<a href="{{ url('admin/pages') }}" class="navbar-brand">
				<small>
					<i class="fa fa-leaf"></i>
					Nhà Thuốc Chính Hãng
				</small>
			</a>
		</div>
		<div class="navbar-buttons navbar-header pull-right" role="navigation">
			<ul class="nav ace-nav">
        <li class="grey dropdown-modal" style="margin-right: 1em">
          <button type="button" class="btn btn-white btn-success no-border">
            <i class="ace-icon fa fa-reply icon-only"></i>&nbsp;
            <a href="{{ url('/') }}">Xem website</a>
          </button>
        </li>

        
        <li class="light-blue dropdown-modal">
         <a data-toggle="dropdown" href="#" class="dropdown-toggle">
          <img class="nav-user-photo" src="{{ asset('backend/assets/images/avatars/avatar4.png') }}" alt="" />
          <span class="user-info">
           <small>Xin Chào,</small>
           @if (Auth::check())
           <span style="text-transform: capitalize;">{{ Auth::user()->name }}</span>
           @endif
         </span>
         <i class="ace-icon fa fa-caret-down"></i>
       </a>
       <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
        <li>
         <a href="{{ url('admin/pages/chitietuser/') }}">
          <i class="ace-icon fa fa-user"></i>
          Thông Tin
        </a>
      </li>

      <li>
       <a href="{{ url('admin/pages/doi-mk/') }}">
        <i class="ace-icon fa fa-key"></i>
        Đổi Mật Khẩu
      </a>
    </li>

    <li class="divider"></li>

    <li>
     <a href="{{ route('getlogout') }}" onclick="return confirm('Bạn có muốn đăng xuất?')">
      <i class="ace-icon fa fa-power-off"></i>
      Đăng Xuất
    </a>
  </li>
</ul>
</li>
</ul>
</div>
</div><!-- /.navbar-container -->
</div>