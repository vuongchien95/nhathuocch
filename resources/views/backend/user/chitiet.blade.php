@extends('backend.layout.master')
@section('content')
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="{{ url('admin/pages') }}">Home</a>
				</li>

				<li>
					<a href="javascript:voi"> Thành Viên </a>
				</li>
				<li class="active"> Chi Tiết Thành Viên</li>
			</ul><!-- /.breadcrumb -->

			<div class="nav-search" id="nav-search">
				<form class="form-search">
					<span class="input-icon">
						<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
						<i class="ace-icon fa fa-search nav-search-icon"></i>
					</span>
				</form>
			</div><!-- /.nav-search -->
		</div>

		<div class="page-content">
			<div class="row">
				<div class="col-xs-12">
					<div class="row">
						<div class="col-xs-12">
							<div class="clearfix">
								<a href="{{ url('admin/user') }}">
									<div class="pull-text col-xs-3" style="margin-bottom: 5px">
										<button class="btn btn-success"><i class="ace-icon glyphicon glyphicon-list"></i> Danh Sách </button>
									</div>
								</a>
								<div class="pull-text col-xs-6">
									@include('backend.block.alert')
								</div>
								<div class="pull-right tableTools-container col-xs-2"></div>
							</div>
							<div class="table-header">
								Chi Tiết Tin: {{ $chitiet->name }}
							</div>

							<div>
								<table id="dynamic-table" class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Tên Thành Viên</th>
											<th>Email</th>
											<th>Phone</th>
											<th>Địa Chỉ</th>
										</tr>
									</thead>

									<tbody>
										<tr>
											<td>{{ $chitiet->name }}</td>
											<td>{!! $chitiet->email !!}</td>
											<td>{!! $chitiet->phone !!}</td>
											<td>{!! $chitiet->address !!}</td>
											
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
@endsection

