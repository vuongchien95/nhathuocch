@extends('backend.layout.master')
@section('content')
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="{{ url('admin/pages') }}">Home</a>
        </li>

        <li>
          <a href="javascript:void(0)">Đổi Mật Khẩu</a>
        </li>
      </ul><!-- /.breadcrumb -->
      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
    <div class="page-content">
      <div class="row">
        <div class="col-xs-12">
          <div class="widget-box">
            <div class="widget-header widget-header-blue widget-header-flat">
              <h4 class="widget-title lighter">Đổi Mật Khẩu Thành Viên: {{ Auth::user()->name }}</h4>
            </div>
            <div class="widget-body">
              @include('backend.block.err')
              <div class="widget-main">
                <div id="fuelux-wizard-container">
                  <div class="step-content pos-rel">
                    <div class="step-pane active" data-step="1">
                      <form action="{{ route('admin.pages.getdoimk') }}" class="form-horizontal" method="post" id="validation-form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                          <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Email:</label>
                          <div class="col-xs-12 col-sm-9">
                            <div class="clearfix">
                              <input type="email" id="name" name="email" class="col-xs-12 col-sm-5" value="{{ Auth::user()->email }}" required="required"/>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Mật Khẩu:</label>
                          <div class="col-xs-12 col-sm-9">
                            <div class="clearfix">
                              <input type="password" id="name" name="password" class="col-xs-12 col-sm-5" value="" required="required"/>
                            </div>
                          </div>
                        </div>

                        <div class="form-group">
                          <label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">Xác Nhận Mật Khẩu:</label>
                          <div class="col-xs-12 col-sm-9">
                            <div class="clearfix">
                              <input type="password" id="name" name="re_password" class="col-xs-12 col-sm-5" value="" required="required"/>
                            </div>
                          </div>
                        </div>

                        <hr />
                        <div class="wizard-actions">
                          <a href="{{ url('admin/pages') }}" class="btn btn-info"><i class="fa fa-reply"></i> Hủy Bỏ</a>
                          <button class="btn" type="reset"><i class="ace-icon fa fa-undo bigger-110"></i> Nhập Lại </button>
                          <button type="submit" id="trigger-save" class="btn btn-success submit-form" name="addUser"><i class="fa fa-save"></i> Thay Đổi</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div><!-- /.widget-main -->
            </div><!-- /.widget-body -->
          </div>
        </div>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.page-content -->
</div>
</div>
@endsection

