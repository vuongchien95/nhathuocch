@extends('backend.layout.master')
@section('content')
<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="{{ route('admin.pages.index') }}">Home</a>
        </li>
        <li>
          <a href="javascript:void(0)">Chi Tiết Thành Viên</a>
        </li>
      </ul>
      <!-- /.breadcrumb -->
      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
          <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
          <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div>
      <!-- /.nav-search -->
    </div>
    <div class="page-content">
      <div class="row">
        <div class="col-xs-12">
          <div>
            <div id="user-profile-1" class="user-profile row">
              <div class="col-xs-12 col-sm-3 center">
                <div>
                  <span class="profile-picture">
                  <img id="avatar" class="editable img-responsive" alt="Alex's Avatar" src="backend/assets/images/avatars/profile-pic.jpg" />
                  </span>
                  <div class="space-4"></div>
                  <div class="width-80 label label-info label-xlg arrowed-in arrowed-in-right">
                    <div class="inline position-relative">
                      <a href="#" class="user-title-label dropdown-toggle" data-toggle="dropdown">
                      <i class="ace-icon fa fa-circle light-green"></i>
                      &nbsp;
                      <span class="white">{{ Auth::user()->name }}</span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-9">
                <div class="space-12"></div>
                <div class="profile-user-info profile-user-info-striped">
                  <div class="profile-info-row">
                    <div class="profile-info-name"> Tên Thành Viên </div>
                    <div class="profile-info-value">
                      <span class="editable" id="username">{{ Auth::user()->name }}</span>
                    </div>
                  </div>
                  <div class="profile-info-row">
                    <div class="profile-info-name"> Địa Chỉ </div>
                    <div class="profile-info-value">
                      <i class="fa fa-map-marker light-orange bigger-110"></i>
                      <span class="editable" id="country">{{ Auth::user()->address }}</span>
                    </div>
                  </div>
                  <div class="profile-info-row">
                    <div class="profile-info-name"> Điện Thoại </div>
                    <div class="profile-info-value">
                      <span class="editable" id="age">{{ Auth::user()->phone }}</span>
                    </div>
                  </div>
                  <div class="profile-info-row">
                    <div class="profile-info-name"> Ngày Đăng Ký </div>
                    <div class="profile-info-value">
                      <span class="editable" id="signup">{{ Auth::user()->ngaydk }}</span>
                    </div>
                  </div>
                </div>
                <div class="space-20"></div>
                <div class="hr hr2 hr-double"></div>
                
              </div>
            </div>
          </div>
          <!-- PAGE CONTENT ENDS -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </div>
    <!-- /.page-content -->
  </div>
</div>
@endsection