@extends('backend.layout.master')
@section('content')
<div class="main-content">
	<div class="main-content-inner">
		<div class="breadcrumbs ace-save-state" id="breadcrumbs">
			<ul class="breadcrumb">
				<li>
					<i class="ace-icon fa fa-home home-icon"></i>
					<a href="{{ route('admin.pages.index') }}">Home</a>
				</li>
				<li class="active">Trang Chủ</li>
			</ul><!-- /.breadcrumb -->

			<div class="nav-search" id="nav-search">
				<form class="form-search">
					<span class="input-icon">
						<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
						<i class="ace-icon fa fa-search nav-search-icon"></i>
					</span>
				</form>
			</div><!-- /.nav-search -->
		</div>

		<div class="page-content">

			{{-- cài đặt  --}}
			@include('backend.layout.setting')
			{{-- end:cài đặt --}}

			<div class="row">
				<div class="col-xs-12">

					<div class="row">
						<div class="space-6"></div>

						<div class="col-sm-12 infobox-container">
							<div class="infobox infobox-green">
								<div class="infobox-icon">
									<i class="ace-icon fa fa-comments"></i>
								</div>

								<div class="infobox-data">
									<span class="infobox-data-number">{{ $tintuc }}</span>
									<div class="infobox-content">Tin Tức Mới</div>
								</div>

								<div class="stat stat-success">8%</div>
							</div>

							<div class="infobox infobox-blue">
								<div class="infobox-icon">
									<i class="ace-icon fa fa-twitter"></i>
								</div>

								<div class="infobox-data">
									<span class="infobox-data-number">{{ $sanpham }}</span>
									<div class="infobox-content">Sản Phẩm Mới</div>
								</div>

								<div class="badge badge-success">
									+32%
									<i class="ace-icon fa fa-arrow-up"></i>
								</div>
							</div>

							<div class="infobox infobox-pink">
								<div class="infobox-icon">
									<i class="ace-icon fa fa-shopping-cart"></i>
								</div>

								<div class="infobox-data">
									<span class="infobox-data-number">{{ $donhang }}</span>
									<div class="infobox-content">Đơn Hàng Mới</div>
								</div>
								<div class="stat stat-success">8%</div>
							</div>

							<div class="infobox infobox-red">
								<div class="infobox-icon">
									<i class="ace-icon fa fa-flask"></i>
								</div>

								<div class="infobox-data">
									<span class="infobox-data-number">{{ $dichvu }}</span>
									<div class="infobox-content">Dịch Vụ Mới</div>
								</div>
                <div class="badge badge-success">
                  +32%
                  <i class="ace-icon fa fa-arrow-up"></i>
                </div>
							</div>

							<div class="infobox infobox-blue2">
								<div class="infobox-progress">
									<div class="easy-pie-chart percentage" data-percent="79" data-size="46">
										<span class="percent">79</span>%
									</div>
								</div>

								<div class="infobox-data">
									<span class="infobox-text">79%</span>

									<div class="infobox-content">
										<span class="bigger-110">~</span>
										Lượt Truy Cập
									</div>
								</div>
							</div>
						</div>
					</div><!-- /.row -->

					<div class="hr hr32 hr-dotted"></div>

					<div class="row">
						<div class="col-sm-12 text-center">
							<h2 style="color: #74a0ba;font-size: 38px;">Chào Mừng Bạn Đến Với Trang Quản Trị</h2>
              {{-- <h6>lượt truy cập: {{isYesterday()}}</h6> --}}
						</div><!-- /.col -->
					</div><!-- /.row -->

          <div class="row">
            <div class="col-xs-3"> </div>
            <div class="pull-text col-xs-6">
              @include('backend.block.alert')
            </div>
          </div>

          

					<div class="hr hr32 hr-dotted"></div>

					<!-- PAGE CONTENT ENDS -->
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.page-content -->
	</div>
</div><!-- /.main-content -->
@endsection