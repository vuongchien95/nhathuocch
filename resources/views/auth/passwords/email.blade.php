<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta charset="utf-8" />
  <title>Reset Password - Ace Admin</title>

  <meta name="description" content="User login page" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

  <!-- bootstrap & fontawesome -->
  <link rel="stylesheet" href="{{ asset('backend/assets/css/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('backend/assets/font-awesome/4.5.0/css/font-awesome.min.css') }}" />

  <!-- text fonts -->
  <link rel="stylesheet" href="{{ asset('backend/assets/css/fonts.googleapis.com.css') }}assets/css/fonts.googleapis.com.css" />

  <!-- ace styles -->
  <link rel="stylesheet" href="{{ asset('backend/assets/css/ace.min.css') }}" />

    <!--[if lte IE 9]>
      <link rel="stylesheet" href="assets/css/ace-part2.min.css" />
    <![endif]-->
    <link rel="stylesheet" href="{{ asset('backend/assets/css/ace-rtl.min.css') }}" />

    <!--[if lte IE 9]>
      <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
    <![endif]-->

    <!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

    <!--[if lte IE 8]>
    <script src="assets/js/html5shiv.min.js"></script>
    <script src="assets/js/respond.min.js"></script>
  <![endif]-->
</head>

<body class="login-layout">
  <div class="main-container">
    <div class="main-content">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <div class="login-container">
            <div class="center">
              <h1>
                <i class="ace-icon fa fa-leaf green"></i>
                <span class="red">Admin Pro </span>
              </h1>
            </div>

            <div class="space-6"></div>

            <div class="position-relative">
              <div id="forgot-box" class="forgot-box no-border" style="margin: 3px 0;border: 1px solid #CCC;">
                <div class="widget-body">
                  @if (session('status'))
                  <div class="alert alert-success">
                    {{ session('status') }}
                  </div>
                  @endif
                  <div class="widget-main">
                    <h4 class="header red lighter bigger">
                      <i class="ace-icon fa fa-key"></i>
                      Khôi Phục Mật Khẩu
                    </h4>

                    <div class="space-6"></div>
                    <p>
                      Nhập địa chỉ Email đã đăng ký tài khoản
                    </p>

                    <form method="POST" action="{{ route('password.email') }}">
                      @csrf
                      <fieldset>
                        <label class="block clearfix">
                          <span class="block input-icon input-icon-right">
                            <input id="email" name="email" type="email" class="form-control" placeholder="Email" value="{{ old('email') }}" required/>
                            <i class="ace-icon fa fa-envelope"></i>
                          </span>
                        </label>

                        <div class="clearfix">
                          <button type="submit" class="width-35 pull-right btn btn-sm btn-danger">
                            <span class="bigger-110">Send</span>
                          </button>
                        </div>
                      </fieldset>
                    </form>
                  </div><!-- /.widget-main -->

                  <div class="toolbar center">
                    <a href="{{ url('/login-admin') }}"  class="back-to-login-link">
                      Quay về trang đăng nhập
                      <i class="ace-icon fa fa-arrow-right"></i>
                    </a>
                  </div>
                </div><!-- /.widget-body -->
              </div><!-- /.forgot-box -->


            </div>
            <!-- /.position-relative -->

            <div class="navbar-fixed-top align-right">
              <br />
              &nbsp;
              <a id="btn-login-dark" href="#">Dark</a>
              &nbsp;
              <span class="blue">/</span>
              &nbsp;
              <a id="btn-login-blur" href="#">Blur</a>
              &nbsp;
              <span class="blue">/</span>
              &nbsp;
              <a id="btn-login-light" href="#">Light</a>
              &nbsp; &nbsp; &nbsp;
            </div>
          </div>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.main-content -->
  </div><!-- /.main-container -->

  <!-- basic scripts -->

  <!--[if !IE]> -->
  <script src="{{ asset('backend/assets/js/jquery-2.1.4.min.js') }}"></script>

  <!-- <![endif]-->

    <!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
  if('ontouchstart' in document.documentElement) document.write("<script src='backend/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>

<!-- inline scripts related to this page -->
<script type="text/javascript">
  jQuery(function($) {
    $(document).on('click', '.toolbar a[data-target]', function(e) {
      e.preventDefault();
      var target = $(this).data('target');
        $('.widget-box.visible').removeClass('visible');//hide others
        $(target).addClass('visible');//show target
      });
  });



      //you don't need this, just used for changing background
      jQuery(function($) {
        $('#btn-login-dark').on('click', function(e) {
          $('body').attr('class', 'login-layout');
          $('#id-text2').attr('class', 'white');
          $('#id-company-text').attr('class', 'blue');

          e.preventDefault();
        });
        $('#btn-login-light').on('click', function(e) {
          $('body').attr('class', 'login-layout light-login');
          $('#id-text2').attr('class', 'grey');
          $('#id-company-text').attr('class', 'blue');

          e.preventDefault();
        });
        $('#btn-login-blur').on('click', function(e) {
          $('body').attr('class', 'login-layout blur-login');
          $('#id-text2').attr('class', 'white');
          $('#id-company-text').attr('class', 'light-blue');

          e.preventDefault();
        });

      });
    </script>
  </body>
  </html>
