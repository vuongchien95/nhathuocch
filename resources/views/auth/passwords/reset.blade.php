<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta charset="utf-8" />
  <title>Reset Password - Ace Admin</title>

  <meta name="description" content="User login page" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
  <link rel="stylesheet" href="{{ asset('backend/assets/css/bootstrap.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('backend/assets/font-awesome/4.5.0/css/font-awesome.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('backend/assets/css/fonts.googleapis.com.css') }}assets/css/fonts.googleapis.com.css" />
  <link rel="stylesheet" href="{{ asset('backend/assets/css/ace.min.css') }}" />
  <link rel="stylesheet" href="{{ asset('backend/assets/css/ace-rtl.min.css') }}" />
</head>

<body class="login-layout">
  <div class="main-container">
    <div class="main-content">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <div class="login-container">
            {{-- <div class="center">
              <h1>
                <i class="ace-icon fa fa-leaf green"></i>
                <span class="red">Admin Pro </span>
              </h1>
            </div> --}}
            <div class="space-6"></div>
            <div class="position-relative">
              <div id="forgot-box" class="forgot-box no-border" style="margin: 3px 0;border: 1px solid #CCC;">
                <div class="widget-body">
                  <div class="widget-main">
                    <h4 class="header red lighter bigger">
                      <i class="ace-icon fa fa-key"></i>
                      Thay Đổi Mật Khẩu
                    </h4>

                    <div class="space-6"></div>

                    <form method="POST" action="{{ route('password.request') }}">
                      @csrf
                      <input type="hidden" name="token" value="{{ $token }}">
                      <label class="block clearfix">
                        <span class="block input-icon input-icon-right">
                          <input id="email" type="email" class="form-control" placeholder="Nhập Email"  name="email" value="{{ $email ?? old('email') }}" required autofocus/>
                          @if ($errors->has('email'))
                          <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                          </span>
                          @endif
                          <i class="ace-icon fa fa-envelope"></i>
                        </span>
                      </label>

                      <label class="block clearfix">
                        <span class="block input-icon input-icon-right">
                          <input name="password" required id="password" type="password" placeholder="mật khẩu"  class="form-control"/>

                          @if ($errors->has('password'))
                          <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                          </span>
                          @endif
                        </span>
                      </label>

                      <label class="block clearfix">
                        <span class="block input-icon input-icon-right">
                          <input name="password_confirmation" required id="password-confirm" type="password" placeholder="nhập lại mật khẩu" class="form-control"/>
                        </span>
                      </label>

                      <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4">
                          <button type="submit" class="btn btn-primary">
                            Thay đổi
                          </button>
                        </div>
                      </div>
                    </form>
                  </div><!-- /.widget-main -->


                </div><!-- /.widget-body -->
              </div><!-- /.forgot-box -->


            </div>
            <!-- /.position-relative -->

            <div class="navbar-fixed-top align-right">
              <br />
              &nbsp;
              <a id="btn-login-dark" href="#">Dark</a>
              &nbsp;
              <span class="blue">/</span>
              &nbsp;
              <a id="btn-login-blur" href="#">Blur</a>
              &nbsp;
              <span class="blue">/</span>
              &nbsp;
              <a id="btn-login-light" href="#">Light</a>
              &nbsp; &nbsp; &nbsp;
            </div>
          </div>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.main-content -->
  </div><!-- /.main-container -->
  <script src="{{ asset('backend/assets/js/jquery-2.1.4.min.js') }}"></script>
  <script type="text/javascript">
    if('ontouchstart' in document.documentElement) document.write("<script src='backend/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
  </script>
  <script type="text/javascript">
    jQuery(function($) {
      $(document).on('click', '.toolbar a[data-target]', function(e) {
        e.preventDefault();
        var target = $(this).data('target');
        $('.widget-box.visible').removeClass('visible');//hide others
        $(target).addClass('visible');//show target
      });
    });
    jQuery(function($) {
      $('#btn-login-dark').on('click', function(e) {
        $('body').attr('class', 'login-layout');
        $('#id-text2').attr('class', 'white');
        $('#id-company-text').attr('class', 'blue');

        e.preventDefault();
      });
      $('#btn-login-light').on('click', function(e) {
        $('body').attr('class', 'login-layout light-login');
        $('#id-text2').attr('class', 'grey');
        $('#id-company-text').attr('class', 'blue');

        e.preventDefault();
      });
      $('#btn-login-blur').on('click', function(e) {
        $('body').attr('class', 'login-layout blur-login');
        $('#id-text2').attr('class', 'white');
        $('#id-company-text').attr('class', 'light-blue');

        e.preventDefault();
      });

    });
  </script>
</body>
</html>

