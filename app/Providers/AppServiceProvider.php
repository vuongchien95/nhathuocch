<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;
use App\Models\CauHinh;
use App\Models\LienHe;
use App\Models\Logo;
use App\Models\DanhMuc;
use App\Models\SanPham;
use App\Models\TinTuc;
use App\Models\DonHang;
use Cart,View,DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $data['cauhinh']    = CauHinh::first();
        $data['lienhe']     = LienHe::first();
        $data['logo']       = Logo::first();
        $data['danhmuccha'] = DanhMuc::where('DanhMuc_Cha',0)->get();
        $data['video']      = DB::table('video')->first();
       

        $data['donhang'] = DonHang::where('TrangThai',2)->count();

        View::share($data);


      Schema::defaultStringLength(191);
      view()->composer('frontend.layout.master',function($view){
        $data['cart']       = Cart::content();
        $data['total']      = Cart::subtotal(0);
        // Cart::destroy();
        $view->with($data);
      });
      
      view()->composer('frontend.layout.toolbar',function($view){
        $data['cart']       = Cart::content();
        $data['total']      = Cart::subtotal(0);
        $view->with($data);
      });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
  }
