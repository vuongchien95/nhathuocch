<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TinhThanh extends Model
{
    protected $table = 'tinhthanh';
    protected $guarded =[];//
    public $timestamps = false;

    public function QuanHuyen(){
      return $this->hasMany('App\Models\QuanHuyen','idTinhThanh','id');
      // 1 tỉnh thành có nhiều quận huyện
    }
}
