<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SamPhamImage extends Model
{
    protected $table = 'sanpham_image';
    protected $guarded =[];


    public function SanPham(){
    	return $this->belongsTo('App\Models\SanPham','idSanPham','id');
    	// 1 ảnh chỉ thuộc về 1 sản phẩm
    }
}
