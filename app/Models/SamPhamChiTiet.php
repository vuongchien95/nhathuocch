<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SamPhamChiTiet extends Model
{
    protected $table = 'sanpham_chitiet';
    protected $guarded =[];


    public function SanPham(){
    	return $this->belongsTo('App\Models\SanPham','idSanPham','id');
    	// 1 thuộc tính chỉ thuộc về 1 sản phẩm
    }
}
