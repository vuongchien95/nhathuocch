<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DonHang extends Model
{
    protected $table = 'donhang';
    protected $guarded =[];
    // public $timestamps = false;

    public function CTDonHang(){
    	return $this->hasMany('App\Models\CTDonHang','idDonHang','id');
    	// 1 đơn hàng thì có nhiều chi tiết đơn hàng
    }

    public function User(){
    	return $this->belongsTo('App\User','idUser','id');
    	// 1 đơn hàng thì chỉ thuộc về 1 người dùng
    }
}
