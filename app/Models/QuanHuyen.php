<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuanHuyen extends Model
{
    protected $table = 'quanhuyen';
    protected $guarded =[];//
    public $timestamps = false;

    public function XaPhuong(){
      return $this->hasMany('App\Models\XaPhuong','idQuanHuyen','id');
      // 1 quận huyện có nhiều xã phường
    }
    public function TinhThanh(){
      return $this->belongsTo('App\Models\TinhThanh','idTinhThanh','id');
      // 1 quận huyện chỉ thuộc về 1 tỉnh thành
    }
}
