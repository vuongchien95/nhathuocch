<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BinhLuan extends Model
{
    protected $table = 'binhluan';
    protected $guarded =[];


    public function SanPham(){
    	return $this->hasOne('App\Models\SanPham','idSanPham','id');
    }

    public function User(){
    	return $this->belongsTo('App\User','idUser','id');
    }

    public function DichVu(){
    	return $this->hasOne('App\Models\DichVu','idDichVu','id');
    }

    public function User(){
    	return $this->belongsTo('App\Models\User','idUser','id');
    }
}
