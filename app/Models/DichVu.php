<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DichVu extends Model
{
    protected $table = 'dichvu';
    protected $guarded =[];
    // protected $fillable = ['idTheLoai','Name','MaSp','ThuongHieu','Price','PriceSale','SoLanXem','Description','Status','DateOrder','AnHien'];

    // public function DanhMuc(){
    // 	return $this->belongsTo('App\Models\DanhMuc','idDanhMuc','id');
    // 	// 1 dich vu thuộc về 1 danh mục
    // }

    public function User(){
    	return $this->belongsTo('App\User','idUser','id');
    	// 1 dich vu thuoc ve 1 ng đăng
    }
}
