<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DKMail extends Model
{
  protected $table = 'dkmail';
  protected $guarded =[];
}
