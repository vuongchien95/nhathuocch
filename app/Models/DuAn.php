<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DuAn extends Model
{
  protected $table = 'duan';
  protected $guarded =[];

  public function User(){
    return $this->belongsTo('App\User','idUser','id');
      // 1 du an thuoc ve 1 ng đăng
  }
}
