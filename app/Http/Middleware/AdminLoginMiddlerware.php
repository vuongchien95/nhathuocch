<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminLoginMiddlerware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next){
      if(Auth::check()){
        $user = Auth::user();
        if($user->level == 1 && $user->trangthai == 1){
          return $next($request);
        }else if($user->trangthai != 1 && $user->level == 1){
          // là admin nhưng bị khóa
          return redirect()->route('getlogin')->with('thog_bao_lg','Tài khoản bạn đang bị khóa.Vui lòng liên hệ Admin để được mở khóa');
        }else if($user->trangthai != 1 && $user->level != 1){
          // là thành viên bị khóa
          return redirect()->route('getlogin')->with('thog_bao_lg','Tài khoản bạn đã bị khóa.');
        }else if($user->level != 1){
          // là thành viên không có quyền truy cập admin
          return redirect()->route('getlogin')->with('er','Bạn không được cấp quyền vào trang quản trị');
        }else{
          return redirect()->route('getlogin');
        }
      }else{
        return redirect()->route('getlogin');
      }
    }
}
