<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\TinTuc;
use App\Models\DanhMuc;
use App\Http\Requests\AdminTTRequest;
use App\Http\Requests\AdminEditTTRequest;
use Auth;

class TinTucController extends Controller
{
	public function List(){
		$list = TinTuc::select()->orderBy('id','DESC')->get();
		// dd($list);
		return view('backend.tintuc.list',compact('list'));
	}
	public function getAdd(){
		$data = DanhMuc::select()->get();
		return view('backend.tintuc.add',compact('data'));
	}
	public function postAdd(AdminTTRequest $req){
		$tintuc            = new Tintuc;
		// $tintuc->idDanhMuc = $req->danhmuc;
		
		$tintuc->idUser    = Auth::user()->id;
		$tintuc->TieuDe    = title_case($req->name);
		$tintuc->Slug      = str_slug($req->name);
		$tintuc->TomTat    = $req->tomtat;
		$tintuc->TuKhoaSeo = $req->tukhoaseo;
    $tintuc->TieuDeSeo = $req->tieudeseo;
    $tintuc->MoTaSeo   = $req->motaseo;
		$tintuc->MoTa      = $req->chitiet;
		// $tintuc->NgayDang  = date('Y-m-d H:i:s');
		$tintuc->AnHien    = $req->status;


		$file = $req->file('image')->getClientOriginalName();
		$filename = rand().'_'.$file;
		$tintuc->Image = $filename;     
		$req->file('image')->move('uploads/tintuc/',$filename);

		$tintuc->save();
		return redirect()->route('admin.tintuc.list')->with(['thog_bao'=>'Thêm mới Tin Tức thành công !!']);
	}

	public function getEdit($id){
		$edit    = TinTuc::findOrFail($id);
		// dd($edit);
		$data = DanhMuc::select()->get();
		return view('backend.tintuc.edit',compact('edit','data'));
	}

	public function postEdit(AdminEditTTRequest $req,$id){
		$tintuc            = TinTuc::find($id);
    
		// $tintuc->idDanhMuc = $req->danhmuc;
		$tintuc->idUser    = Auth::user()->id;
		$tintuc->TieuDe    = title_case($req->name);
		$tintuc->Slug      = str_slug($req->name);
		$tintuc->TomTat    = $req->tomtat;
		$tintuc->TuKhoaSeo = $req->tukhoaseo;
    $tintuc->TieuDeSeo = $req->tieudeseo;
    $tintuc->MoTaSeo   = $req->motaseo;
		$tintuc->MoTa      = $req->chitiet;
		// $tintuc->NgayDang  = date('Y-m-d H:i:s');
		$tintuc->AnHien    = $req->status;

		$file_path = public_path('uploads/tintuc/').$tintuc->Image;
    	if ($req->hasFile('image')) {
    		if (file_exists($file_path))
    		{
    			unlink($file_path);
    		}

    		$f = $req->file('image')->getClientOriginalName();
    		$filename = rand().'_'.$f;
    		$tintuc->Image = $filename;       
    		$req->file('image')->move('uploads/tintuc/',$filename);
    	}

		$tintuc->save();
		return redirect()->route('admin.tintuc.list')->with(['thog_bao'=>'Tin Tức đã được sửa thành công !!']);
	}
	public function Del($id){
		$del = TinTuc::find($id);
		$del->delete();
		return redirect()->back()->with(['thog_bao'=>'Xóa Tin thành công!!']);
	}

	public function Detail($id){
		$chitiet = TinTuc::find($id);
		return view('backend.tintuc.chitiet',compact('chitiet'));
	}
}
