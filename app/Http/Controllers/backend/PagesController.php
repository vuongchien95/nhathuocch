<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth,Session,Input;
use App\Models\TinTuc;
use App\Models\SanPham;
use App\Models\DonHang;
use App\Models\DichVu;
use App\User;
use Hash;
use App\Http\Requests\DoiMKRequest;
class PagesController extends Controller
{
	public function AdminIndex(){
    $data['tintuc'] = TinTuc::count();
    $data['sanpham'] = SanPham::count();
    $data['donhang'] = DonHang::where('TrangThai',2)->count();
    $data['dichvu'] = DichVu::count();
		return view('backend.pages.index',$data);
	}

	public function getLogin(){
		return view('backend.pages.login');
	}

	public function postLogin(Request $request){
		$this->validate($request,
			[
				'password'=>'min:5|max:30'
			],
			[
				'password.min'=>'Mật khẩu ít nhất phải có 5 kí tự',
				'password.max'=>'Mật khẩu không được quá 30 kí tự'
			]
		);
		$loginadmin = array(
			'email'    => $request->email,
			'password'=> $request->password
		);
		if (Auth::attempt($loginadmin)) {
			return redirect('admin/pages/');
		}else{
			return redirect()->back()->with('thog_bao_lg','Tên tài khoản hoặc mật khẩu không đúng !!');
		}
	}

	public function getLogoutAdmin(){
		Auth::logout();
        Session::flush();
		return redirect()->route('getlogin');
	}

	public function Lich()
    {
		return view('backend.pages.lich');
	}
	public function ChiTietUser()
    {
        return view('backend.pages.chitietuser');
    }

    public function InboxMail(){
        return view('backend.pages.mail');
	}

    public function getDoiMk(){
        return view('backend.pages.doimk');
    }

  public function postDoiMk(DoiMKRequest $req){
    $doimk = User::find(Auth::user()->id);

    if ($req->password) 
      {
        $doimk->password =Hash::make($req->password);
      }
    $doimk->email = $req->email;
    $doimk->remember_token =$req->_token;
    $doimk->save();

    return redirect('admin/pages')->with('thog_bao','Thay đổi mật khẩu thành công !!');
  }
  
}
