<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Hash;
use Auth;
use App\Http\Requests\AdminURequest;
use App\Http\Requests\AdminEditURequest;
class UserController extends Controller
{
	public function List(){
		$list = User::select()->orderBy('id','DESC')->get();
		return view('backend.user.list',compact('list'));
	}

	public function getAdd(){
		return view('backend.user.add');
	}
	public function postAdd(AdminURequest $req){
		$user            = new User;
		
		$user->name      = trim($req->name);
		$user->password  = Hash::make($req->password);
		$user->email     = trim($req->email);
		$user->phone     = trim($req->phone);
		$user->address   = trim($req->address);
		$user->ngaydk    = date('Y-m-d H:i:s');
		$user->level     = trim($req->level);
		$user->trangthai = $req->status;
		$user->remember_token = $req->_token;


		$user->save();
		return redirect()->route('admin.user.list')->with(['thog_bao'=>'Thêm mới Thành Viên thành công !!']);
	}

	public function getEdit($id){
		$edit    = User::findOrFail($id);
		if ((Auth::user()->id != 1) && ($id == 1 || $edit->level == 1 && Auth::user()->id != $id)) {
			return redirect()->back()->with(['thog_bao_loi'=>'Bạn không có quyền truy cập vào tài khoản này này !!']);
		}
		else{
			return view('backend.user.edit',compact('edit'));
		}
		return view('backend.user.edit',compact('edit'));
	}

	public function postEdit(AdminEditURequest $req,$id){
		$user            = User::find($id);
		
		$user->name      = trim($req->name);
    if ($req->password) {
      $user->password  = Hash::make($req->password);
    }
		$user->email     = trim($req->email);
    $user->phone     = trim($req->phone);
    $user->address   = trim($req->address);
		$user->remember_token = $req->_token;
		$user->ngaydk    = date('Y-m-d H:i:s');
		if ($id == 1) {
			$user->level     = 1;
			$user->trangthai = 1;
		}else{
			$user->level     = trim($req->level);
			$user->trangthai = $req->status;
		}
		

		$user->save();
		return redirect()->route('admin.user.list')->with(['thog_bao'=>'Thông tin Thành Viên đã được sửa thành công !!']);
	}
	public function Del($id){
		$del = User::find($id);
		if ($id == 1 ) {
			return redirect()->route('admin.user.list')->with(['thog_bao_loi'=>'Bạn không thể xóa Người Quản Trị !!']);
		}
		// elseif(Auth::user()->level == 1){
		// 	return redirect()->route('admin.user.list')->with(['thog_bao'=>'Bạn không thể xóa Thanh Viên này !!']);
		// }
		elseif(Auth::user()->id == $del->id){
			return redirect()->route('admin.user.list')->with(['thog_bao_loi'=>'Bạn không thể xóa Chính Bạn !!']);
		}
		else{
			$del->delete();
			return redirect()->back()->with(['thog_bao'=>'Xóa Thành Viên thành công !!']);
		}
	}

	public function Detail($id){
		$chitiet = User::find($id);
		return view('backend.user.chitiet',compact('chitiet'));
	}
}
