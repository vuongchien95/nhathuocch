<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DanhMuc;
use App\Http\Requests\AdminDmRequest;
use App\Http\Requests\AdminEditDMRequest;
use Auth,DB;

class DanhMucController extends Controller
{
  public function List(){
   $data = DanhMuc::select()->orderBy('id','DESC')->get();
   return view('backend.danhmuc.list',compact('data'));
 }

 public function getAdd(){
  $data = DanhMuc::all();
  return view('backend.danhmuc.add',compact('data'));
}
public function postAdd(AdminDmRequest $req){
 $danhmuc              = new DanhMuc;
 $danhmuc->idUser      = Auth::user()->id;
 $danhmuc->Name        = title_case($req->name);
 $danhmuc->Slug        = str_slug($req->name);
 $danhmuc->DanhMuc_Cha = $req->danhmuc;
 // $danhmuc->AnHien      = $req->status;

 $danhmuc->save();
 return redirect()->route('admin.danhmuc.list')->with(['thog_bao'=>'Danh mục đã được thêm mới thành công !!']);
}

public function getEdit($id){
 $edit = DanhMuc::findOrFail($id);
 $data = DanhMuc::all();
 return view('backend.danhmuc.edit',compact('data','edit'));
}

public function postEdit(AdminEditDMRequest $req,$id){
 $danhmuc              = DanhMuc::find($id);
 $danhmuc->idUser      = Auth::user()->id;
 $danhmuc->Name        = title_case($req->name);
 $danhmuc->Slug        = str_slug($req->name);
 $danhmuc->DanhMuc_Cha = $req->danhmuc;
 // $danhmuc->AnHien      = $req->status;
 $danhmuc->save();
 return redirect()->route('admin.danhmuc.list')->with(['thog_bao'=>'Danh mục đã được sửa thành công !!']);
}

public function Del($id){
  $parent = DanhMuc::where('DanhMuc_Cha',$id)->count();
  if($parent == 0 ){
    $del = DanhMuc::find($id);
    $del->delete();
    return redirect()->back()->with(['thog_bao'=>'Xóa Danh Mục thành công!!']);
  }else{
    return redirect()->route('admin.danhmuc.list')->with(['thog_bao_loi'=>'Danh Mục đang gồm nhiều danh mục con. Bạn không thể xóa !!']);
  }

}



}
