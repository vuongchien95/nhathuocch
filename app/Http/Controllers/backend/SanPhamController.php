<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\SanPham;
use App\Models\DanhMuc;
use App\Models\SamPhamImage;
use App\Models\SamPhamChiTiet;
use Auth,DB;
use App\Http\Requests\AdminSpRequest;
use App\Http\Requests\AdminEditSpRequest;

class SanPhamController extends Controller
{
    public function List()
    {
        $list = SanPham::select()->orderBy('Pro_id','DESC')->paginate(15);
        return view('backend.sanpham.list',compact('list','ncc'));
    }

    public function getAdd()
    {
        $data = DanhMuc::select()->get();
        return view('backend.sanpham.add',compact('data'));
    }

    public function postAdd(AdminSpRequest $req)
    {
    
        $sp             = new SanPham;
        $sp->NhaCungCap = json_encode($req->nhacungcap);
        $sp->Pro_Name   = title_case($req->name);
        $sp->Slug       = str_slug($req->name);
        $sp->idDanhMuc  = $req->danhmuc;
        $sp->idUser     = Auth::user()->id;
        $sp->Kieu       = $req->kieu;
        $sp->ThuongHieu = $req->thuonghieu;
        $sp->Gia        = $req->gia;
        
        $sp->DonVi      = $req->donvi;
        $sp->ThanhPhan  = $req->thanhphan;
        $sp->SoLanXem   = 1;
        $sp->TuKhoaSeo  = $req->tukhoaseo;
        $sp->TieuDeSeo  = $req->tieudeseo;
        $sp->MoTaSeo    = $req->motaseo;
        $sp->MoTa       = $req->mota;
        $sp->TrangThai  = $req->trangthai;
        $sp->AnHien     = $req->status;
    // $sp->NgayDang  = date('Y-m-d H:i:s');

        $file = $req->file('image')->getClientOriginalName();
        $filename = rand().'_'.$file;
        $sp->Image = $filename;     
        $req->file('image')->move('uploads/sanpham/',$filename);

        $sp->save();
        return redirect()->route('admin.sanpham.list')->with(['thog_bao'=>'Thêm mới Sản Phẩm thành công !!']);
    }

    public function getEdit($id)
    {
        $edit     = SanPham::findOrFail($id);
        $data     = DanhMuc::select()->get();
        return view('backend.sanpham.edit',compact('edit','data'));
    }

    public function postEdit(AdminEditSpRequest $req,$id)
    {
        $sp            = SanPham::find($id);
    
        $sp->Pro_Name      = title_case($req->name);
        $sp->Slug      = str_slug($req->name);
        $sp->idDanhMuc = $req->danhmuc;
        $sp->idUser    = Auth::user()->id;
        $sp->Kieu      = $req->kieu;
        $sp->ThuongHieu= $req->thuonghieu;
        $sp->Gia       = $req->gia;
        $sp->DonVi     = $req->donvi;
        $sp->ThanhPhan = $req->thanhphan;
        $sp->NhaCungCap = json_encode($req->nhacungcap);
        // dd($sp->NhaCungCap);
        $sp->SoLanXem  = 1;
        $sp->TuKhoaSeo = $req->tukhoaseo;
        $sp->TieuDeSeo = $req->tieudeseo;
        $sp->MoTaSeo   = $req->motaseo;
        $sp->MoTa      = $req->mota;
        $sp->TrangThai = $req->trangthai;
        $sp->AnHien    = $req->status;
    // $sp->NgayDang  = date('Y-m-d H:i:s');

        $file_path = public_path('uploads/sanpham/').$sp->Image;
        if ($req->hasFile('image')) 
        {
            if (file_exists($file_path))
            {
                unlink($file_path);
            }

            $f = $req->file('image')->getClientOriginalName();
            $filename = rand().'_'.$f;
            $sp->Image = $filename;       
            $req->file('image')->move('uploads/sanpham/',$filename);
        }
    
        $sp->save();
        return redirect()->route('admin.sanpham.list')->with(['thog_bao'=>'Thông tin Sản Phẩm đã được sửa thành công !!']);
    }

    public function Del($id)
    {
        $del = SanPham::find($id)->delete();
    // $delImg = SamPhamImage::whereIn('idSanPham',[$del->id])->delete();
    // File::delete('resources/upload/'.$product->image);
        return redirect()->back()->with(['thog_bao'=>'Xóa Sản Phẩm thành công!!']);
    }

    public function DelImg($id)
    {
        $del_img = SamPhamImage::find($id)->delete();
        return redirect()->back()->with(['thog_bao'=>'Xóa Ảnh Sản Phẩm thành công!!']);
    }

    public function GetAddDetail($id)
    {
        $sanpham    = SanPham::find($id);
        $sp_img     = SamPhamImage::whereIn('idSanPham',[$sanpham->Pro_id])->get();
        $query = SamPhamChiTiet::where('idSanPham',$id)->first();
        if($query){
            $sp_chitiet = json_decode($query->SizeMauSL);
        }
        return view('backend.sanpham.addchitiet',compact('sanpham','sp_img','sp_chitiet','query'));
    }
  
    public function PostAddDetail(Request $req,$id)
    {
        if($files = $req->file('avatar')){
            foreach($files as $file){
            $name = rand().$file->getClientOriginalName();
            $spImg = new SamPhamImage;
            $spImg->idSanPham = $id;
            $spImg->AnHien    = 1;
            $spImg->Image     = $name;
            $spImg->save();
            if ($spImg) {
                $file->move('uploads/images/sanphamimg',$name);
            }
        }
    }

        $sanpham = SanPham::find($id);

        $spchitiet = new SamPhamChiTiet();
        $spchitiet->idSanPham = $id;
        $spchitiet->MaGiamGia = $req->magiamgia;
        $spchitiet->Gia       = $req->gia;

        SamPhamChiTiet::where('idSanPham',$id)->delete();

        $spchitiet->save();
        return redirect()->back()->with(['thog_bao'=>'Thêm thông tin thành công!!']);
    }

    public function Detail($id)
    {

        $chitiet = SanPham::find($id);
        return view('backend.sanpham.chitiet',compact('chitiet'));
    }
}






