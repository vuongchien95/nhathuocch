<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\NhaCungCap;

class NCCController extends Controller
{
    public function List()
    {
		$list = NhaCungCap::select()->orderBy('id','DESC')->get();
		return view('backend.nhacungcap.listNCC',compact('list'));
	}

	public function getAddNCC()
	{
		return view('backend.nhacungcap.addNCC');
	}

	public function postAddNCC(Request $rq)
	{
		$ncc 	= new NhaCungCap();

		$ncc->name 	= $rq->name;
		$ncc->email = $rq->email;
		$ncc->phone = $rq->phone;
		$ncc->address = $rq->address;

		$ncc->save();
		return redirect('/admin/nhacungcap')->with('thog_bao','Thêm nhà cung cấp thành công!!');
	}

	public function getEditNCC($id)
	{
    	$edit = NhaCungCap::find($id);
    	return view('backend.nhacungcap.editNCC',compact('edit'));
  	}

  	public function postEditNCC(Request $rq,$id)
  	{
    	$editncc             = NhaCungCap::find($id);
    	$editncc->name 		 = $rq->name;
    	$editncc->email      = $rq->email;
    	$editncc->phone      = $rq->phone;
    	$editncc->address    = $rq->address;
    	$editncc->save();
    	return redirect('admin/nhacungcap')->with('thog_bao','Thay đổi nhà cung cấp thành công !!');
  	}

	public function getDel($id)
	{
		$delncc = NhaCungCap::find($id);
		$delncc->delete();
		return redirect('/admin/nhacungcap')->with('thog_bao','Xóa thành công nhà cung cấp!!');
	}

}
