<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DonHang;
use App\Models\CTDonHang;
use App\Models\SanPham;
use App\User;
class DonHangController extends Controller
{
  public function List(){
    $list = DonHang::select()->orderBy('id','DESC')->get();
      // dd($list);
    return view('backend.donhang.list',compact('list'));
  }
  public function Detail($id){
    $donhang = DonHang::find($id);
    $chitiet = CTDonHang::where('idDonHang',$id)->get();
    // dd($chitiet);
    // foreach ($chitiet as $key => $items) {
    //   $sanpham[] = SanPham::where('id',$items->idSanPham)->get();
    // }
    // dd($sanpham);

    $user =  User::where('id',$donhang->idUser)->first();
    return view('backend.donhang.chitiet',compact('chitiet','user','donhang','sanpham'));
  }
  public function Del($id){
    $del = DonHang::find($id);
    if ($del->TrangThai == 1) {
      return redirect()->back()->with(['thog_bao'=>'Không thể xóa Đơn Hàng vì đã được xác nhận!']);
    }
    else{
      $del->delete();
      return redirect()->back()->with(['thog_bao'=>'Xóa Đơn Hàng thành công!!']);
    }
    
  }

  public function XacNhan($id){
    $xacnhan = DonHang::find($id);
    $xacnhan->TrangThai = 1;

    $xacnhan->save();
    return redirect()->route('admin.donhang.list')->with(['thog_bao'=>'Xác nhận Đơn Hàng thành công!!']);
  }

  public function HuyBo($id){
    $huybo = DonHang::find($id);
    $huybo->TrangThai = 3;

    $huybo->save();
    return redirect()->route('admin.donhang.list')->with(['thog_bao'=>'Hủy Đơn Hàng thành công!!']);
  }
  public function DelSp($dh,$sp){
    // dd($sp);
    $delsp = CTDonHang::where(['idDonHang'=>$dh,'idSanPham'=>$sp])->first();
    // dd($delsp);
    $delsp->delete();
    return redirect()->back()->with(['thog_bao'=>'Xóa Sản Phẩm thành công!!']);
  }
}
