<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GioiThieu;
use App\Models\LienHe;
use App\Models\SanPham;
use App\User;
use App\Http\Requests\FronURequest;
use Hash,Auth,Session;
use App\Models\TinhThanh;
use App\Models\QuanHuyen;
use App\Models\XaPhuong;

class UserController extends Controller
{
  public function GetDangKy(){
    $tinhThanh = TinhThanh::all();
    return view('frontend.pages.dangky',compact('tinhThanh'));
  }
  
  public function PostDangKy(FronURequest $req){
    $tinhThanh = TinhThanh::where('id',$req->tinhthanh)->first();
    $quanHuyen = QuanHuyen::where('id',$req->quanhuyen)->first();
    $xaPhuong  = XaPhuong::where('id',$req->xaphuong)->first();

    $user                 = new User;
    $user->name           = trim($req->name);
    $user->password       = Hash::make($req->password);
    $user->email          = trim($req->email);
    $user->phone          = trim($req->phone);
    $user->address        = trim($req->address.' - '.$xaPhuong->Name.' - '.$quanHuyen->Name.' - '.$tinhThanh->Name);
    $user->ngaydk         = date('Y-m-d H:i:s');
    $user->level          = 2;
    $user->trangthai      = 1;
    $user->remember_token = $req->_token;
    $user->save();

    if (Auth::attempt(['name'=>$req->name,'password'=>$req->password])) {
      return redirect('/');
    }
  }
  public function GetDangNhap(){
    return view('frontend.pages.dangnhap');
  }

  public function PostDangNhap(Request $req){
    $dangNhap = array('email'=>$req->name,'password'=>$req->password);
    if (Auth::attempt($dangNhap)) {
      echo "true"; 
    }else{
      echo "false"; 
    }
  }
  public function Login(Request $req){
    $dangNhap = array('email'=>$req->name,'password'=>$req->password);
    if (Auth::attempt($dangNhap)) {
      return redirect('/');
    }else{
      return redirect()->back()->with('erro','Tên tài khoản hoặc mật khẩu không đúng');
    }
  }

  public function DangXuat(){
    Auth::logout();
    return redirect()->back();
  }

}
