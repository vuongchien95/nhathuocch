<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\GioiThieu;
use App\Models\LienHe;
use App\Models\SanPham;
use App\User;
use App\Http\Requests\FronURequest;
use Hash,Auth,Session;
use App\Models\TinhThanh;
use App\Models\QuanHuyen;
use App\Models\XaPhuong;
use App\Models\YeuThich;
use Mail;

class PagesController extends Controller
{
    public function LienHe(){
        $lienhe = LienHe::first();
        return view('frontend.pages.lienhe',compact('lienhe'));
    }

    public function GioiThieu(){
        $gioithieu = GioiThieu::first();
        return view('frontend.pages.gioithieu',compact('gioithieu'));
    }

    public function SanPham(){
        $sanpham = SanPham::where('AnHien',1)->inRandomOrder()->paginate(12);
        return view('frontend.pages.sanpham',compact('sanpham'));
    }
  
    public function WhereQuanHuyen($id){
        $quanHuyen = QuanHuyen::where('idTinhThanh',$id)->get();
        foreach ($quanHuyen as $items) 
        {
        echo "<option value='".$items->id."'>".$items->Name."</option>";
        }
    }

    public function WhereXaPhuong($id)
    {
        $xaPhuong = XaPhuong::where('idQuanHuyen',$id)->get();
        foreach ($xaPhuong as $items) 
        {
          echo "<option value='".$items->id."'>".$items->Name."</option>";
        }
    }

    public function XemNhanh($id)
    {
        $xemNhanh = SanPham::find($id);
        $json = json_encode($xemNhanh);
        echo $json;
    }

    public function TimKiem(Request $req)
    {
        $search = $req->search;
        $sanPham = SanPham::where('Pro_Name','like','%'.$req->search.'%')
                ->orWhere('Gia',$req->search)
                ->paginate(25);
        $viewsp = SanPham::inRandomOrder()->limit(10)->get();

        return view('frontend.pages.timkiem',compact('sanPham','search','viewsp'));
    }

    public function searchAjax($key)
    {

        if ($key == null) 
        {
            $html = '';
        }
        else
        {
            $pro = SanPham::where('Pro_Name','like','%'.$key.'%')
                ->orWhere('Gia',$key)
                ->get();
            $html = '';
            foreach($pro as $items){
                $html .= "<a class='thumbs' href='/san-pham/".$items->Pro_id."-".$items->Slug.".html'>";
                $html .= "<img src='/uploads/sanpham/".$items->Image."'>";
                $html .= "</a>";
                $html .= "<a href='/san-pham/".$items->Pro_id."-".$items->Slug.".html'>".$items->Pro_Name."<span class='price-search'>".number_format($items->Gia,0,',','.')."đ</span></a>";
            }
            $html .= '';
        }
       
        return response()->json([ 'html' => $html ],200);
    }

    public function sendMail(Request $request)
    {
        $hoten    = $request->hoten;
        $email    = $request->email; 
        $phone    = $request->phone;
        $content  = $request->content;

        $data['information'] = [
            'hoten'     => $hoten,
            'email'     => $email,
            'phone'     => $phone,
            'content'   => $content,
        ];

        $emailNHan  = 'nhathuocchinhhang@gmail.com';
        Mail::send('frontend.pages.tt_kh', $data, function($messager) use ($emailNHan) {
            $messager->to( $emailNHan , 'Nhà thuốc chính hãng');
            $messager->subject('Thông tin khách hàng');
        });
        return "<script type='text/javascript'>alert('Cám ơn bạn! Chúng tôi sẽ liên hệ lại bạn sớm nhất');window.location.href='/';</script>";
    }

}
