<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CauHinh;
use App\Models\Banner;
use App\Models\ChinhSach;
use App\Models\SanPham;
use App\Models\DanhMuc;
use App\Models\TinTuc;
use Session;

class TrangChuController extends Controller
{
  public function TrangChu(){
    $data['banner']             = Banner::where('AnHien',1)->get();
    $data['chinhsach']          = ChinhSach::all();
    // $data['sanphamkhuyenmai']   = SanPham::where([['GiaSale','>',0],['AnHien','=',1]])->get();
    $data['sanphamkhuyenmai']   = SanPham::inRandomOrder()->limit(9)->get();
    $data['mypham']             = SanPham::where(['Kieu'=> 1, 'AnHien' => 1])->orderBy('Pro_id','DESC')->limit(16)->get();
    $data['tanduoc']            = SanPham::where(['Kieu'=> 2, 'AnHien' => 1])->orderBy('Pro_id','ASC')->limit(16)->get();
    $data['thaoduoc']           = SanPham::where(['Kieu'=> 3, 'AnHien' => 1])->orderBy('Pro_id','DESC')->limit(16)->get();
    $data['tbyte']              = SanPham::where(['Kieu'=> 4, 'AnHien' => 1])->orderBy('Pro_id','DESC')->limit(16)->get();
    $data['thuocquy']           = SanPham::where(['Kieu'=> 5, 'AnHien' => 1])->orderBy('Pro_id','DESC')->limit(6)->get();
    $data['bcs']                = SanPham::where(['Kieu'=> 6, 'AnHien' => 1])->orderBy('Pro_id','DESC')->limit(6)->get();
    $data['tintuc']             = TinTuc::where('AnHien',1)->limit(5)->orderBy('id','DESC')->get();
    return view('frontend.pages.trangchu',$data);
  }
  
}
