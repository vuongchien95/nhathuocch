<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminEditDVRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'max:255',
            'status' => 'required'
        ];
    }

    public function messages(){
        return 
        [
            'name.max'    => 'Tiêu Đề bạn không được nhập quá 255 ký tự',
            'status.required' => 'Trạng Thái không được đê trống'
        ];
    }
}
