<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cauhinh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cauhinh', function (Blueprint $table) {
            $table->increments('id');
            $table->string('TieuDe', 255)->nullable();
            $table->string('TuKhoa', 255)->nullable();
            $table->text('MoTa')->nullable();
            $table->string('Favicon', 255)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cauhinh');
    }
}
