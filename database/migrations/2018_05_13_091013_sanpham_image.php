<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SanphamImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('sanpham_image', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('idSanPham')->unsigned();
        $table->foreign('idSanPham')->references('id')->on('sanpham')->onDelete('cascade');
        $table->string('Image', 100)->nullable();
        $table->tinyInteger('AnHien')->nullable();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('sanpham_image');
    }
  }
