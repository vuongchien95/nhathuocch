<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Duan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('duan', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('idUser')->unsigned()->nullable();
        $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade');
        $table->string('TenDuAn', 255);
        $table->string('Slug', 255);
        $table->string('Image', 100)->nullable();
        $table->string('TomTat', 255)->nullable();
        $table->text('MoTa')->nullable();
        $table->text('TuKhoaSeo')->nullable();
        $table->text('TieuDeSeo')->nullable();
        $table->text('MoTaSeo')->nullable();

        $table->date('NgayDang')->nullable();
        $table->tinyInteger('NoiBat')->nullable();
        $table->tinyInteger('AnHien')->nullable();
        

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('duan');
    }
  }
