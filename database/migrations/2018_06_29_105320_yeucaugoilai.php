<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Yeucaugoilai extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('ycgoilai', function (Blueprint $table) {
        $table->increments('id');
        $table->string('Phone', 255)->nullable();
        $table->string('UrlSp', 255)->nullable();
        $table->date('NgayDK')->nullable();

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('ycgoilai');
    }
}
