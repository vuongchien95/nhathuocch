<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Binhluan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('binhluan', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('idUser')->unsigned();
        $table->foreign('idUser')->references('id')->on('users')->onDelete('cascade');
        $table->integer('idSanPham')->unsigned()->nullable();
        $table->foreign('idSanPham')->references('id')->on('sanpham')->onDelete('cascade');
        $table->integer('idDichVu')->unsigned()->nullable();
        $table->foreign('idDichVu')->references('id')->on('dichvu')->onDelete('cascade');
        $table->integer('idTinTuc')->unsigned()->nullable();
        $table->foreign('idTinTuc')->references('id')->on('tintuc')->onDelete('cascade');
        $table->text('noidung')->nullable();
        
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('binhluan');
    }
  }
