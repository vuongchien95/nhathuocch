<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Chinhsach extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('chinhsach', function (Blueprint $table) {
        $table->increments('id');
        $table->string('TenChinhSach', 255)->nullable();
        $table->string('Image', 100)->nullable();
        $table->string('MoTa', 255)->nullable();

        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('chinhsach');
    }
}
