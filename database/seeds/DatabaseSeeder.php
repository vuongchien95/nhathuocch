<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(Users::class);
    	DB::table('users')->insert([
    		'name' => 'Vương Chiến',
            'password' => bcrypt('24121995'),
    		'email' => 'vuongchien95@gmail.com',
    		'phone' => '01648485882',
    		'address' => 'Hà Nội',
    		'trangthai' => 1,
    		'ngaydk' => date('Y-m-d H:i:s'),
    		'level'	=> 1
    	]);

    }
}
