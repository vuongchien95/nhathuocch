let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');
// let mix = require('laravel-mix');
// npm install
// npm run dev

mix.styles([
 'public/backend/assets/css/bootstrap.min.css',
 'public/backend/assets/font-awesome/4.5.0/css/font-awesome.min.css',
 'public/backend/assets/css/fonts.googleapis.com.css',
 'public/backend/assets/css/ace.min.css',
 'public/backend/assets/css/select2.min.css',
 'public/backend/assets/css/style.css',
 'public/backend/assets/css/jquery-ui.custom.min.css',
 'public/backend/assets/css/fullcalendar.min.css',
 'public/backend/assets/css/dropzone.min.css',
 'public/backend/assets/css/ace-skins.min.css',
 'public/backend/assets/css/ace-rtl.min.css',
], 'public/css/app.css');

mix.scripts([
 'public/backend/assets/js/ace-extra.min.js',
 'public/backend/assets/js/jquery-2.1.4.min.js',
 'public/backend/assets/js/bootstrap.min.js',
 'public/backend/assets/js/jquery-ui.custom.min.js',
 'public/backend/assets/js/jquery.ui.touch-punch.min.js',
 'public/backend/assets/js/jquery.easypiechart.min.js',
 'public/backend/assets/js/jquery.sparkline.index.min.js',
 'public/backend/assets/js/jquery.flot.min.js',
 'public/backend/assets/js/jquery.flot.pie.min.js',
 'public/backend/assets/js/jquery.flot.resize.min.js',
 'public/backend/assets/js/wizard.min.js',
 'public/backend/assets/js/jquery.validate.min.js',
 'public/backend/assets/js/jquery-additional-methods.min.js',
 'public/backend/assets/js/bootstrap-wysiwyg.min.js',
 'public/backend/assets/js/bootbox.js',
 'public/backend/assets/js/jquery.maskedinput.min.js',
 'public/backend/assets/js/select2.min.js',
 'public/backend/assets/js/dropzone.min.js',
 'public/backend/assets/js/jquery.dataTables.min.js',
 'public/backend/assets/js/dataTables.select.min.js',
 'public/backend/assets/js/jquery.dataTables.bootstrap.min.js',
 'public/backend/assets/js/dataTables.buttons.min.js',
 'public/backend/assets/js/buttons.flash.min.js',
 'public/backend/assets/js/buttons.html5.min.js',
 'public/backend/assets/js/buttons.print.min.js',
 'public/backend/assets/js/buttons.colVis.min.js',
 'public/backend/assets/js/ace-elements.min.js',
 'public/backend/assets/js/ace.min.js',
 'public/backend/assets/js/chosen.jquery.min.js',
 'public/backend/assets/js/spinbox.min.js',
 'public/backend/assets/js/bootstrap-datepicker.min.js',
 'public/backend/assets/js/bootstrap-timepicker.min.js',
 'public/backend/assets/js/moment.min.js',
 'public/backend/assets/js/daterangepicker.min.js',
 'public/backend/assets/js/bootstrap-datetimepicker.min.js',
 'public/backend/assets/js/bootstrap-colorpicker.min.js',
 'public/backend/assets/js/jquery.knob.min.js',
 'public/backend/assets/js/autosize.min.js',
 'public/backend/assets/js/jquery.inputlimiter.min.js',
 'public/backend/assets/js/bootstrap-tag.min.js',
 'public/backend/assets/js/fullcalendar.min.js',
], 'public/js/app.js');

// mix.copyDirectory('public/fontend/fonts', 'public/fonts'); 
// //coppy file

// const ImageminPlugin = require('imagemin-webpack-plugin').default;
// const CopyWebpackPlugin = require('copy-webpack-plugin');
// const imageminMozjpeg = require('imagemin-mozjpeg');

// mix.webpackConfig({
//     plugins: [
//         new CopyWebpackPlugin([{
//             from: 'public/images',
//             to: 'images', // Thư mục chứa public/images
//         }]),
//         new ImageminPlugin({
//             test: /\.(jpe?g|png|gif|svg)$/i,
//             plugins: [
//                 imageminMozjpeg({
//                     quality: 50, // Chất lượng nén
//                 })
//             ]
//         })
//     ]
// });
